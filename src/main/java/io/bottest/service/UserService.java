package io.bottest.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.bottest.dto.SignupDTO;
import io.bottest.dto.UserDTO;
import io.bottest.jpa.entity.User;
import io.bottest.jpa.respository.UserRepository;

@Service
@Transactional
public class UserService {

	private final Logger LOGGER = LogManager.getLogger(UserService.class);
	
	@Value("${security.jwt.token.secret-key:Bottest@Paracel}")
	private String secretKey = "Bottest@Paracel";

//	@Autowired
//	SendMailService sendMailService;

	
	

	@Autowired
	private UserRepository userRepository;
	

	/**
	 * @param username
	 * @return
	 * @throws NoSuchElementException
	 */
	public boolean checkEmailExist(String email) throws NoSuchElementException {
		LOGGER.info("Begin Service UserService Function checkEmailExist");
		LOGGER.info("Service UserService Function checkEmailExist PARAMS username: " + email);
		
		LOGGER.info("End Service UserService Function checkEmailExist");
		return (userRepository.getUserByEmail(email) == null) ? true : false;
	}

	/**
	 * @param dto
	 */
	public void saveUserRole(SignupDTO dto) {

		LOGGER.info("Begin Service UserService Function saveUserRole");
		LOGGER.info("Service UserService Function checkEmailExist PARAMS dto: " + new Gson().toJson(dto, new TypeToken<SignupDTO>() {}.getType()));
		User user = new User();

		user.setUsername(dto.getEmail());
		user.setEmail(dto.getEmail());
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		user.setPassword(encoder.encode(dto.getPassword()));
		
		user.setEnable(false);
		user.setRole("ROLE_USER");
		//user.setCompany(dto.getCompany());
		//user.setCountry(dto.getCountry());
		//user.setFirstname(dto.getFirstname());
		//user.setLastname(dto.getLastname());
		//user.setPosition(dto.getPosition());
		//user.setLanguage(dto.getLanguage());	
		//user.setGitUser(dto.getGitUsername());
		//user.setGitPass(dto.getGitPassword());
		userRepository.save(user);
		
		LOGGER.info("End Service UserService Function saveUserRole");
	}

	
	
	public List<User> getAllUsers(){
		LOGGER.info("Begin Service UserService Function getAllUsers");
		
		LOGGER.info("End Service UserService Function getAllUsers");
		return userRepository.findByAllUsers();
	}
	
	public User getBottestUser(String username) {

		return userRepository.getBottestUserByUsername(username);
	}	
	
	

	public UserDTO getUserByUserName(String username) { 
		LOGGER.info("Begin Service UserService Function getUserByUserName");
		LOGGER.info("Service UserService Function getUserByUserName PARAM username: " + username);
		 
		
		UserDTO userDTO = new UserDTO();
		
		LOGGER.info("End Service UserService Function getUserByUserName");
		return userDTO;	
	}

	
	public UserDTO updateProfile(UserDTO userDTO, String username) {
		LOGGER.info("Begin Service UserService Function updateProfile");
		LOGGER.info("Service UserService Function updateProfile PARAM userDTO: " + userDTO);
		
		UserDTO result = new UserDTO();
		if(StringUtils.isEmpty(username)) {
			return result;
		}
		
		
		
		result = getUserByUserName(username);
		LOGGER.info("End Service UserService Function updateProfile");
		return result;
	}

	

}

