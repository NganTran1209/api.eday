package io.bottest.service;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.bottest.configure.security.jwt.JwtTokenProvider;
import io.bottest.dto.FilterProductDTO;
import io.bottest.dto.ProductDTO;
import io.bottest.jpa.entity.Product;
import io.bottest.jpa.respository.ProductRepository;
import io.bottest.jpa.respository.ProductRepositoryCustom;

@Service
@Transactional
public class ProductService {

	@PersistenceContext
	private EntityManager entityManager;
	
	private final Logger LOGGER = LogManager.getLogger(ProductService.class);
	
	@Autowired
	 JwtTokenProvider jwtTokenProvider;
	
	@Autowired
	ProductRepository productRepo;
	
	@Autowired
	ProductRepositoryCustom proCustom;

	

	public List<Product> getAllProduct() {
		LOGGER.info("Begin Service ProductService Function getAllProduct");
		List<Product> listProduct = new ArrayList<Product>();
		
		// Call Db get all user project active
		listProduct = productRepo.getAllProduct();
		
		LOGGER.info("End Service ProductService Function getAllProduct");
		return listProduct;
	}

	public Product getProductById(Long productId) {
		return productRepo.findById(productId).get();
	}

	public List<Map<String, Object>> filterProduct(FilterProductDTO filterProduct) {
		LOGGER.info("Begin Service ProductService Function filterProduct");
		
		return proCustom.filterProduct(filterProduct);
	}
	
}
