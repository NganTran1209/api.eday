package io.bottest.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.bottest.config.ExportExcel;
import io.bottest.jpa.entity.PayRoll;
import io.bottest.jpa.entity.PayRollIdentity;
import io.bottest.jpa.entity.Worker;
import io.bottest.jpa.respository.PayRollRepository;

@Transactional
@Service
public class PayRollService {

	@Autowired
	private PayRollRepository payRollRepository;
	
	@Value("${exportFile.path}")
	private String exportFile;
	
	@Autowired
	private WorkerService workerService;
	
	public void createNewPayMonth(String month, Worker worker) {
		PayRollIdentity payRollIdentity = new PayRollIdentity();
		PayRoll payRoll = new PayRoll();
		payRollIdentity.setMonth(month);
		System.out.println("-------" + worker.getId());
		payRollIdentity.setWorkerId(worker.getId());
		payRoll.setPayRollIdentity(payRollIdentity);
		payRoll.setContractSalary(worker.getUnit_salary());
		payRoll.setTotalSalary(worker.getUnit_salary());
		payRoll.setStatus(false);
		payRollRepository.save(payRoll);
	}

	public String createFileExcelPayRoll(List<PayRoll> listPayRoll, String month) {
		HSSFWorkbook workbook = new HSSFWorkbook();
		//for(String testsuiteId: listTestsuites) {
			HSSFSheet sheet = workbook.createSheet(month);
			sheet.setColumnWidth(0, 1000);
			sheet.setColumnWidth(1, 5000);
			sheet.setColumnWidth(2, 10000);
			sheet.setColumnWidth(3, 10000);
			//sheet.setColumnWidth(4, 10000);
			sheet.setColumnWidth(4, 3000);
			sheet.setColumnWidth(5, 6000);
//			sheet.setColumnWidth(6, 7000);
			int rownum = 0;
			Cell cell;
	        Row row;
	        
	        ExportExcel ex = new ExportExcel();
	        HSSFCellStyle style = ex.createStyleForTitle(workbook);
	        HSSFCellStyle cellStyle = workbook.createCellStyle();
	        cellStyle.setWrapText(true);
	        
	        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	        cellStyle.setAlignment(HorizontalAlignment.LEFT);
	        
	        cellStyle.setBorderLeft(BorderStyle.THIN);
	        cellStyle.setBorderRight(BorderStyle.DOTTED);
	        cellStyle.setBorderBottom(BorderStyle.DOTTED);
	        cellStyle.setBorderTop(BorderStyle.DOTTED);
	        
	        HSSFCellStyle cellStyleRight = workbook.createCellStyle();
	        cellStyleRight.setWrapText(true);
	        
	        cellStyleRight.setVerticalAlignment(VerticalAlignment.CENTER);
	        cellStyleRight.setAlignment(HorizontalAlignment.LEFT);
	        
	        cellStyleRight.setBorderLeft(BorderStyle.THIN);
	        cellStyleRight.setBorderRight(BorderStyle.THIN);
	        cellStyleRight.setBorderBottom(BorderStyle.DOTTED);
	        cellStyleRight.setBorderTop(BorderStyle.DOTTED);
	        
	        HSSFCellStyle cellStyleBottom = workbook.createCellStyle();
	        cellStyleBottom.setWrapText(true);
	        
	        cellStyleBottom.setVerticalAlignment(VerticalAlignment.CENTER);
	        cellStyleBottom.setAlignment(HorizontalAlignment.LEFT);
	        
	        cellStyleBottom.setBorderLeft(BorderStyle.THIN);
	        cellStyleBottom.setBorderRight(BorderStyle.THIN);
	        cellStyleBottom.setBorderBottom(BorderStyle.THIN);
	        cellStyleBottom.setBorderTop(BorderStyle.DOTTED);
	        
	        HSSFCellStyle styleHeader = ex.createStyleForTitle(workbook);
	        styleHeader.setWrapText(true);
	        styleHeader.setVerticalAlignment(VerticalAlignment.CENTER);
	        styleHeader.setAlignment(HorizontalAlignment.CENTER);
	        
	        HSSFCellStyle cellBackgroundColor = workbook.createCellStyle();
	        cellBackgroundColor.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
	        cellBackgroundColor.setFillBackgroundColor(IndexedColors.SKY_BLUE.getIndex());
	        cellBackgroundColor.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	        cellBackgroundColor.setWrapText(true);
	        cellBackgroundColor.setVerticalAlignment(VerticalAlignment.CENTER);
	        cellBackgroundColor.setAlignment(HorizontalAlignment.CENTER);
	        
	        cellBackgroundColor.setBorderLeft(BorderStyle.THIN);
	        cellBackgroundColor.setBorderRight(BorderStyle.THIN);
	        cellBackgroundColor.setBorderBottom(BorderStyle.THIN);
	        cellBackgroundColor.setBorderTop(BorderStyle.THIN);
	        
	        
	        
	        HSSFFont boldFont = workbook.createFont();
	        boldFont.setBold(true);
	        boldFont.setFontHeightInPoints((short) 11);
	        boldFont.setColor(IndexedColors.WHITE.getIndex());
	        
	        //cellBackgroundColor.setFont(font);
	        cellBackgroundColor.setFont(boldFont);
	        
	        row = sheet.createRow(rownum);
	 
	        cell = row.createCell(0, CellType.STRING);
	        cell.setCellValue("#");
	        cell.setCellStyle(style);
	        cell.setCellStyle(styleHeader);
	        cell.setCellStyle(cellBackgroundColor);
	        
	        cell = row.createCell(1, CellType.STRING);
	        cell.setCellValue("Worker");
	        cell.setCellStyle(style);
	        cell.setCellStyle(styleHeader);
	        cell.setCellStyle(cellBackgroundColor);
	        
	        cell = row.createCell(2, CellType.STRING);
	        cell.setCellValue("Contact Salary");
	        cell.setCellStyle(style);
	        cell.setCellStyle(styleHeader);
	        cell.setCellStyle(cellBackgroundColor);
	        
	        cell = row.createCell(3, CellType.STRING);
	        cell.setCellValue("Actual Work Day ");
	        cell.setCellStyle(style);
	        cell.setCellStyle(styleHeader);
	        cell.setCellStyle(cellBackgroundColor);
	        
			/*
			 * cell = row.createCell(4, CellType.STRING); cell.setCellValue("Last Run");
			 * cell.setCellStyle(style); cell.setCellStyle(styleHeader);
			 * cell.setCellStyle(cellBackgroundColor);
			 */
	        
	        cell = row.createCell(4, CellType.STRING);
	        cell.setCellValue("Total Salary");
	        cell.setCellStyle(style);
	        cell.setCellStyle(styleHeader);
	        cell.setCellStyle(cellBackgroundColor);
	        
	        cell = row.createCell(5, CellType.STRING);
	        cell.setCellValue("Status");
	        cell.setCellStyle(style);
	        cell.setCellStyle(styleHeader);
	        cell.setCellStyle(cellBackgroundColor);
	        
//	        cell = row.createCell(6, CellType.STRING);
//	        cell.setCellValue("Executed User");
//	        cell.setCellStyle(style);
//	        cell.setCellStyle(styleHeader);
//	        cell.setCellStyle(cellBackgroundColor);
	        
	        writeDataToExcel(workbook, listPayRoll, sheet, rownum, row, cell, cellStyle, cellStyleRight, cellStyleBottom);
		//}
		
        File file = new File(this.exportFile + "/" + "Payroll_" + month +"_" +"Export.xls");
        file.getParentFile().mkdirs();
        
        FileOutputStream outFile;
		try {
			outFile = new FileOutputStream(file);
			workbook.write(outFile);
	        
		} catch (IOException e) {
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
		
		return file.getAbsolutePath().toString();
		
	}

	private void writeDataToExcel(HSSFWorkbook workbook, List<PayRoll> listPayRoll, HSSFSheet sheet, int rownum,
			Row row, Cell cell, HSSFCellStyle cellStyle, HSSFCellStyle cellStyleRight, HSSFCellStyle cellStyleBottom) {
		LocalDate localDate = LocalDate.now();
		int dayOfMonth = localDate.getMonth().length(true);
		for(PayRoll e: listPayRoll) {
//			listTestcase.add(e);
	        rownum++;
	        
            row = sheet.createRow(rownum);
         
            cell = row.createCell(0, CellType.NUMERIC);
            cell.setCellValue(rownum);
            cell.setCellStyle(cellStyle);
            
            cell = row.createCell(1, CellType.STRING);
            cell.setCellValue(e.getWorker().getUser().getUsername());
            cell.setCellStyle(cellStyle);
            
            cell = row.createCell(2, CellType.STRING);
           
            cell.setCellValue(e.getContractSalary());
            cell.setCellStyle(cellStyle);
            
            cell = row.createCell(3, CellType.STRING);
            String arrayDayOff[] = null;
    		if (e.getDayOff() != null)
    			arrayDayOff = workerService.convertDateStringToArray(e.getDayOff());
    		
    		int actual = dayOfMonth - ((arrayDayOff!= null) ? arrayDayOff.length : 0);
            cell.setCellValue(actual);
            cell.setCellStyle(cellStyle);
            
			/*
			 * cell = row.createCell(4, CellType.STRING); cell.setCellValue(e.getLastRun());
			 * cell.setCellStyle(cellStyle);
			 */
            
           
            cell = row.createCell(4, CellType.STRING);
            cell.setCellValue(e.getTotalSalary()); 
            cell.setCellStyle(cellStyle);
            
            cell = row.createCell(5, CellType.STRING);
            String status = (e.isStatus() == false) ? "Unpaid" : "Paid";
            cell.setCellValue(status); 
            cell.setCellStyle(cellStyle);
            
            
		}
		
		
	}

}
