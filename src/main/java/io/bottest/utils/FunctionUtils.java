package io.bottest.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class FunctionUtils {
	public final static DateFormat ISO_DATE = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	public final static DateFormat PYTHON_DATE = new SimpleDateFormat("yyyyMMdd'_'HHmmss");

	public static String  convertISO2PythonDate(Date isoDate) {

			//date = ISO_DATE.parse(isoDate);
			return PYTHON_DATE.format(isoDate);

	}
	
	public static String joinInteger(List<Integer> list) {
		StringBuffer result = new StringBuffer();
		if (list.isEmpty()) {
			result.append("*");
		}else {
			for (Integer i : list) {
				result.append(i);
				result.append(",");
			}
			result.deleteCharAt(result.length()-1);
		}
		return result.toString();
	}
	
	public static String randomString() {
		int leftLimit = 48; // numeral '0'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = 10;
	    Random random = new Random();

	    String generatedString = random.ints(leftLimit, rightLimit + 1)
	      .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
	      .limit(targetStringLength)
	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	      .toString();

	    System.out.println(generatedString);
	    
	    return generatedString;
	}

}
