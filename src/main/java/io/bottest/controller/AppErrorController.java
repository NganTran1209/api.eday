package io.bottest.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AppErrorController implements ErrorController {
	
	private final Logger LOGGER = LogManager.getLogger(AppErrorController.class);
	
	private final static String ERROR_PATH = "/error";
	
	@Autowired
	private ErrorAttributes errorAttributes;

	@Override
	public String getErrorPath() {
		// TODO Auto-generated method stub
		return ERROR_PATH;
	}
	
	@RequestMapping(value = { "/error/403" }, method = RequestMethod.GET)
	public ModelAndView accessDeny() {
		
		LOGGER.info("Start GET /error/403 ");
		
		ModelAndView mav = new ModelAndView("login");
		
		LOGGER.info("End GET /error/403 ");
		return mav;
	}
	
    /**
     * Supports the HTML Error View
     * @param request
     * @return
     */
    @RequestMapping(value = ERROR_PATH, produces = "text/html")
    public ModelAndView errorHtml(HttpServletRequest request) {
    	LOGGER.info("Start GET /error ");
    	
    	LOGGER.info("End GET /error ");
        return new ModelAndView("login", getErrorAttributes(request, false));
    }
    
    private Map<String, Object> getErrorAttributes(HttpServletRequest request,
        boolean includeStackTrace) {
    	
        LOGGER.info("Start FUNCTION getErrorAttributes ");
    	
        LOGGER.info("POST FUNCTION getErrorAttributes PARAMS includeStackTrace:" + includeStackTrace);
        
    	ServletWebRequest servletWebRequest = new ServletWebRequest(request);
    	
    	LOGGER.info("End FUNCTION getErrorAttributes ");
    	return this.errorAttributes.getErrorAttributes(servletWebRequest,
    					includeStackTrace);
    }

}
