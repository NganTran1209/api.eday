package io.bottest.controller;

import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import io.bottest.dto.APIResult;
import io.bottest.dto.OrderDTO;
import io.bottest.dto.OrderUserDTO;
import io.bottest.jpa.entity.Order;
import io.bottest.jpa.entity.Payment;
import io.bottest.jpa.entity.Product;
import io.bottest.jpa.entity.ProductWareHouse;
import io.bottest.jpa.entity.User;
import io.bottest.jpa.respository.OrderPoductRepository;
import io.bottest.jpa.respository.PaymentRepository;
import io.bottest.jpa.respository.ProductRepository;
import io.bottest.jpa.respository.ProductWareHouseResponsitory;
import io.bottest.jpa.respository.UserRepository;

@Controller
@RestController
@RequestMapping("/order")
public class OrderController {
	private static final String LOG_STRING = null;

	private final Logger LOGGER = LogManager.getLogger(OrderController.class);
	
	@Autowired
	OrderPoductRepository orderRepo;
	
	@Autowired
	ProductRepository productRepo;
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	ProductWareHouseResponsitory productWareRepo;
	
	@Autowired
	PaymentRepository paymentRepo;
	
	@RequestMapping(value = { "/addCart" }, method = RequestMethod.POST)
	public APIResult addCart(@RequestBody Order order, @AuthenticationPrincipal UserDetails userDetails ) {

		final String LOG_STRING = "POST /order/addCart";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		if(userDetails != null) {
			if(order.getProId() > 0) {
				Product product = productRepo.findByProductId(order.getProId());
				ProductWareHouse productWareHouse = productWareRepo.findByProductId(product.getId());
				if(order.getAmount() > 0) {
					if(order.getAmount() <= productWareHouse.getStockQuantity()) {
						order.setPrice(product.getPrice_sell()*order.getAmount());
						order.setState(0);
						User owner = userRepo.findByUserNameAndPassword(userDetails.getUsername(), userDetails.getPassword());
						order.setUser_id(owner.getId());
						order.setCreateDay(new Date());
						orderRepo.save(order);
						result.setData(order);
						LOGGER.info("End POST /order/addCart");
					}else {
						result.setErrormessage("The number of products exceeds the available quantity ");
					}
					
				}else {
					result.setErrormessage("Invalid product quantity ");
				}
				
			}else {
				result.setErrormessage("Invalid productId  ");
			}
		}else {
				result.setErrormessage("You need login !");
		}
		
		return result;

	}
	
	@RequestMapping(value = { "/{orderId}/submit" }, method = RequestMethod.POST)
	public APIResult submitOrder(@PathVariable("orderId") int orderId, HttpServletRequest request, @AuthenticationPrincipal UserDetails userDetails ) {

		final String LOG_STRING = "POST /order/addCart";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		if(userDetails != null) {
			if(orderId > 0) {
				Order order = orderRepo.findByOrderId(orderId);
				if(order != null) {
					order.setState(2);
					orderRepo.save(order);
					
					//TANG SO LUONG DA BAN
					Product product = productRepo.findByProductId(order.getProId());
					product.setAmount_sell(product.getAmount_sell() + order.getAmount());
					productRepo.save(product);
					
					//CAP NHAT KHO
					ProductWareHouse productWareHouse = productWareRepo.findByProductId(product.getId());
					long total_sale = productWareHouse.getSalerableQuantity()  + order.getAmount();
					productWareHouse.setSalerableQuantity(total_sale);
					productWareHouse.setStockQuantity(productWareHouse.getAmount() - total_sale);
					productWareHouse.setTotalMoneySell(total_sale * product.getPrice_sell());
					productWareHouse.setProfit(total_sale * (product.getPrice_sell() - product.getPrice_buy()));
					productWareRepo.save(productWareHouse);
					
					result.setData(order);
	
					LOGGER.info("End POST /order/addCart");
				}else {
					result.setErrormessage("Please check order");
				}
				
			}else {
				result.setErrormessage("Invalid orderId ");
			}
		}else {
			result.setErrormessage("You need login !");
		}
		
		return result;

	}
	
	@RequestMapping(value = { "/getCartByUser" }, method = RequestMethod.GET)
	public APIResult listOrderOnCartByUser(@AuthenticationPrincipal UserDetails userDetails ) {

		final String LOG_STRING = "POST /order/getAll";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		if(userDetails != null) {
			User user = userRepo.findByUserNameAndPassword(userDetails.getUsername(), userDetails.getPassword());
			List<Order> orders = orderRepo.findCartByUserId(user.getId());
			List<OrderUserDTO> listOrderDTO = new ArrayList<OrderUserDTO>();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Map<String, Object> variables = new HashMap<String, Object>();
			if(orders != null) {
				for (Order order : orders) {
					OrderUserDTO orderDTO = new OrderUserDTO();
					orderDTO.setId(order.getId());
					Product product = productRepo.findByProductId(order.getProId());
					orderDTO.setProduct(product);
					orderDTO.setAmount(order.getAmount());
					orderDTO.setPrice(order.getPrice());
					if(order.getCreateDay() != null) {
						orderDTO.setAddDay(df.format(order.getCreateDay()));
					}
					if(order.getState() == 0) {
						orderDTO.setStatus("Add cart");
					}else {
						if(order.getState() == 1) {
							orderDTO.setStatus("Done payment");
						}else {
							if(order.getState() == 2) {
								orderDTO.setStatus("Done buy");
							}
						}
					}
					listOrderDTO.add(orderDTO);
				}
				variables.put("total",listOrderDTO.size() );
				variables.put("listOrder", listOrderDTO);
				result.setData(variables);
			}else {
				variables.put("total", 0);
				variables.put("listOrder", orders);
				result.setData(orders);
			}
			
		}else {
			result.setErrormessage("You need login !");
		}
		LOGGER.info("End POST /order/getAll");
		return result;

	}
	
	@RequestMapping(value = { "/{orderId}/detail" }, method = RequestMethod.GET)
	public APIResult detailOrder(@PathVariable("orderId") int orderId,@AuthenticationPrincipal UserDetails userDetails ) {

		final String LOG_STRING = "POST /order/" + orderId + "/detailOrder";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		if(userDetails != null) {
			if(orderId > 0) {
				Order order = orderRepo.findByOrderId(orderId);
				if(order != null) {
					OrderDTO orderDTO = new OrderDTO();
					orderDTO.setId(order.getId());
					User user = userRepo.findById(order.getUser_id());
					orderDTO.setUser(user);
					Product product = productRepo.findByProductId(order.getProId());
					orderDTO.setProduct(product);
					Payment payment = paymentRepo.getPaymentByOrderId((long) orderId);
					orderDTO.setPayment(payment);
					orderDTO.setAmount(order.getAmount());
					orderDTO.setPrice(order.getPrice());
					if(order.getState() == 0) {
						orderDTO.setStatus("Add cart");
					}else {
						if(order.getState() == 1) {
							orderDTO.setStatus("Done payment");
						}else {
							if(order.getState() == 2) {
								orderDTO.setStatus("Done buy");
							}
						}
					}
					result.setData(orderDTO);
				}else {
					result.setData(order);
					result.setErrormessage("No orders exist!");
				}
				
				LOGGER.info("End GET" + LOG_STRING );
			}else {
				result.setErrormessage("Invalid orderId !");
			}
		}else {
			result.setErrormessage("You need login !");
		}
		return result;

	}
	
	@RequestMapping(value = { "/{orderId}/deleted" }, method = RequestMethod.DELETE)
	public APIResult deleteOrder(Principal principal,@PathVariable(name = "orderId") String orderId, @AuthenticationPrincipal UserDetails userDetails) {
		
		final String LOG_STRING = "POST /order/"+ orderId + "/deleted";
		LOGGER.info("Start " + LOG_STRING);
		APIResult result = new APIResult();
		if(userDetails != null) {
			if(orderId != null && Integer.parseInt(orderId) > 0) {
				Order order = orderRepo.findByOrderId(Integer.parseInt(orderId));
				if(order != null) {
					User user = userRepo.findByUserNameAndPassword(userDetails.getUsername(), userDetails.getPassword());
					if(user.getId() == order.getUser_id()) {
						if(order.getState() == 0) {
							orderRepo.delete(order);
							
							List<Order> orders = orderRepo.findCartByUserId(user.getId());
							result.setData(orders);
						}else {
							result.setErrormessage("Order don't remove !");
						}
					}else {
						result.setErrormessage("Don't permission remove !");
					}
					
					
				}else {
					result.setErrormessage("An error occurred, please check the parameter again!");
				}
			}else {
				result.setErrormessage("Invalid orderId !");
			}
			
		}else {
			result.setErrormessage("You need login !");
		}
		
		LOGGER.info("End " + LOG_STRING);
		return result;

	}
	
	@RequestMapping(value = { "/getCart/total" }, method = RequestMethod.GET)
	public APIResult totalCartByUser(@AuthenticationPrincipal UserDetails userDetails ) {

		final String LOG_STRING = "POST /order/getAll";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		if(userDetails != null) {
			User user = userRepo.findByUserNameAndPassword(userDetails.getUsername(), userDetails.getPassword());
			List<Order> orders = orderRepo.findCartByUserId(user.getId());
			if(orders != null) {
				result.setData(orders.size());
			}else {
				result.setData(0);
			}
			
		}else {
			result.setErrormessage("You need login !");
		}
		LOGGER.info("End POST /order/getAll");
		return result;

	}
}
