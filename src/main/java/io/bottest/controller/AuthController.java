package io.bottest.controller;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.ResponseEntity.ok;

import java.awt.List;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.bottest.configure.security.jwt.JwtTokenProvider;
import io.bottest.dto.APIResult;
import io.bottest.dto.AuthenticationRequest;
import io.bottest.dto.UserDTO;
import io.bottest.jpa.entity.User;
import io.bottest.jpa.respository.UserRepository;
import io.bottest.service.CustomUserDetailsService;
import io.bottest.service.UserService;

@Controller
@RestController
@RequestMapping("/auth")
public class AuthController {

	private final Logger LOGGER = LogManager.getLogger(AuthController.class);
	
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    CustomUserDetailsService users;

    @Autowired
    UserService userService;

    @Autowired
	UserRepository userRepository;
    /**
     * @param data
     * @return
     */
    @RequestMapping(value="/signin", method=RequestMethod.POST) 
    public APIResult signin(@RequestBody AuthenticationRequest data, HttpServletRequest request) {
    	//System.out.println(data);
    	
    	LOGGER.info("Start POST /auth/signin ");
    	APIResult result = new APIResult();
            String email = data.getEmail();
           try {
        	   UsernamePasswordAuthenticationToken userpassToken=new UsernamePasswordAuthenticationToken(email, data.getPassword());
               User detail = this.users.loadUserByUsername(email);
               if(detail != null) {
   	            String username = detail.getUsername();
   	            userpassToken.setDetails(detail);
   	            
   	            LOGGER.info("POST /auth/signin PARAM email:" + email);
   	            
   	            Authentication auth =  authenticationManager.authenticate(userpassToken);
   	            String token = jwtTokenProvider.createToken(username, detail.getRole());
   	            SecurityContextHolder.getContext().setAuthentication(auth);
   	            System.out.println("token:" + token);
   	            
   	            LOGGER.info("POST /auth/signin PARAM token:" + token);
   	            
   	            Map<Object, Object> model = new HashMap<>();
   	            model.put("email", email);
   	            model.put("token", token);
   	            model.put("role", detail.getRole());
   	            LOGGER.info("POST /auth/signin PARAM role:" + detail.getRole());
   	            
   	            result.setData(model);
   	            
               }else {
   	            	result.setErrormessage("Email don't exist!");
               }
               
               LOGGER.info("END POST /auth/signin ");
               
              
           }catch(AuthenticationException e) {
        	   result.setErrormessage("Invalid password supplied !");
           }
           
           return result;
    }
    
    /**
     * @param userDetails
     * @return
     */
    @RequestMapping(value="/me", method=RequestMethod.GET)
    public APIResult currentUser(@AuthenticationPrincipal UserDetails userDetails){
    	APIResult result = new APIResult();
    	if(userDetails != null) {
    		LOGGER.info("Start GET /auth/me ");
        	
        	LOGGER.info("GET /auth/me PARAMS userDetails:" + new Gson().toJson(userDetails, new TypeToken<UserDetails>() {}.getType()));
        	
            Map<Object, Object> model = new HashMap<>();
            model.put("username", userDetails.getUsername());
            model.put("role", userDetails.getAuthorities()
                .stream()
                .map(a -> ((GrantedAuthority) a).getAuthority())
                .collect(toList())
            );
            User me = userRepository.getUserAuthen(userDetails.getUsername());
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            UserDTO userReturn = new UserDTO();
			userReturn.setId(me.getId());
			userReturn.setAddr(me.getAddr());
			if(me.getBirthDay() != null) {
				userReturn.setBirthDay(df.format(me.getBirthDay()));
			}
			userReturn.setEmail(me.getEmail());
			userReturn.setFirst_name(me.getFirst_name());
			userReturn.setLast_name(me.getLast_name());
			userReturn.setPassword(me.getPassword());
			userReturn.setPhone(me.getPhone());
			userReturn.setRole(me.getRole());
			userReturn.setSex(me.isSex());
			userReturn.setUsername(me.getUsername());
            result.setData(userReturn);
    	}else {
    		result.setErrormessage("403 Authencation!!");
    	}
    	
        
        LOGGER.info("End GET /auth/me ");
        
        return result;        
    }
    
    //EDAY
  
}
