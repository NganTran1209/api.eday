package io.bottest.dto;

import java.util.List;

public class WorkerDTO {
	
	private String ownerId;
	private String id;
	private String name;
	private String phone;
	private String address;
	private String workTime;
	private String dateStart;
	private String dateEnd;
	private String contractSalary;
	private String unitSalary;
	private String[] schedule;
	private String avatar;
	private List<PayRollDTO> listPayRoll;
	
	
	public String getId() {
		return id;
	}
	
	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getWorkTime() {
		return workTime;
	}
	
	public void setWorkTime(String workTime) {
		this.workTime = workTime;
	}
	
	public String getDateStart() {
		return dateStart;
	}
	
	public void setDateStart(String dateStart) {
		String arrayDateStart[] = dateStart.split("-");
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < arrayDateStart.length; i++) {
			sb.append(arrayDateStart[i]);
		}
		this.dateStart = sb.toString();
	}
	
	public String getDateEnd() {
		return dateEnd;
	}
	
	public void setDateEnd(String dateEnd) {
		String arrayDateEnd[] = dateEnd.split("-");
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < arrayDateEnd.length; i++) {
			sb.append(arrayDateEnd[i]);
		}
		this.dateEnd = sb.toString();
	}
	
	public String getContractSalary() {
		return contractSalary;
	}
	
	public void setContractSalary(String contractSalary) {
		this.contractSalary = contractSalary;
	}
	
	public String getUnitSalary() {
		return unitSalary;
	}
	
	public void setUnitSalary(String unitSalary) {
		this.unitSalary = unitSalary;
	}
	
	public String[] getSchedule() {
		return schedule;
	}
	
	public void setSchedule(String[] schedule) {
		this.schedule = schedule;
	}
	
	public String getAvatar() {
		return avatar;
	}
	
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	public List<PayRollDTO> getListPayRoll() {
		return listPayRoll;
	}
	
	public void setListPayRoll(List<PayRollDTO> listPayRoll) {
		this.listPayRoll = listPayRoll;
	}
	
	
	
}
