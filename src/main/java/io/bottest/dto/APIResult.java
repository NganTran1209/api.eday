package io.bottest.dto;

public class APIResult {
	String errormessage;
	String errorCode;
	Object data;
	/**
	 * @return the errormessage
	 */
	public String getErrormessage() {
		return errormessage;
	}
	/**
	 * @param errormessage the errormessage to set
	 */
	public void setErrormessage(String errormessage) {
		this.errormessage = errormessage;
	}
	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}
	
}
