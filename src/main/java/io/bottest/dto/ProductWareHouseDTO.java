package io.bottest.dto;

import java.util.Date;

import io.bottest.jpa.entity.Product;
import io.bottest.jpa.entity.User;

public class ProductWareHouseDTO {

	private Long id;
	
	private Product product;

	private Long amount;
	
	private Date updateDay;
	
	private String created_date;
	
	private Long stockQuantity;
	
	private Long salerableQuantity;
	
	private Long totalMoneyBuy;
	
	private Long totalMoneySell;
	
	private Long profit;
	
	private User Imple_staff;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Date getUpdateDay() {
		return updateDay;
	}

	public void setUpdateDay(Date updateDay) {
		this.updateDay = updateDay;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public Long getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(Long stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public Long getSalerableQuantity() {
		return salerableQuantity;
	}

	public void setSalerableQuantity(Long salerableQuantity) {
		this.salerableQuantity = salerableQuantity;
	}

	public Long getTotalMoneyBuy() {
		return totalMoneyBuy;
	}

	public void setTotalMoneyBuy(Long totalMoneyBuy) {
		this.totalMoneyBuy = totalMoneyBuy;
	}

	public Long getTotalMoneySell() {
		return totalMoneySell;
	}

	public void setTotalMoneySell(Long totalMoneySell) {
		this.totalMoneySell = totalMoneySell;
	}

	public Long getProfit() {
		return profit;
	}

	public void setProfit(Long profit) {
		this.profit = profit;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public User getImple_staff() {
		return Imple_staff;
	}

	public void setImple_staff(User imple_staff) {
		Imple_staff = imple_staff;
	}
	
}
