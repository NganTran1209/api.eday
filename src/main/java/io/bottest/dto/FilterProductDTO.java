package io.bottest.dto;

import java.util.List;

public class FilterProductDTO {

	private String name;
	private int catId;
	private List<String> size;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCatId() {
		return catId;
	}
	public void setCatId(int catId) {
		this.catId = catId;
	}
	public List<String> getSize() {
		return size;
	}
	public void setSize(List<String> size) {
		this.size = size;
	}
	
	
	
}
