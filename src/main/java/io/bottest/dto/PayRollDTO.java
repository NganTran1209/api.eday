package io.bottest.dto;

public class PayRollDTO {

	private String month;
	private String unitSalary;
	private String hourOfDay;
	private String salaryToMonth;
	private String contractSalary;
	private String[] dayOff;
	private String[] dayWork;
	private String status;
	private Long bonus;
	private String totalSalary;
	private String comment;
	
	public String getUnitSalary() {
		return unitSalary;
	}
	
	public void setUnitSalary(String unitSalary) {
		this.unitSalary = unitSalary;
	}
	
	public String getHourOfDay() {
		return hourOfDay;
	}
	
	public void setHourOfDay(String hourOfDay) {
		this.hourOfDay = hourOfDay;
	}
	
	public String getSalaryToMonth() {
		return salaryToMonth;
	}
	
	public void setSalaryToMonth(String salaryToMonth) {
		this.salaryToMonth = salaryToMonth;
	}
	
	public String getContractSalary() {
		return contractSalary;
	}
	
	public void setContractSalary(String contractSalary) {
		this.contractSalary = contractSalary;
	}
	
	public String getMonth() {
		return month;
	}
	
	public void setMonth(String month) {
		this.month = month;
	}
	
	public String[] getDayOff() {
		return dayOff;
	}
	
	public void setDayOff(String[] dayOff) {
		this.dayOff = dayOff;
	}
	
	public String[] getDayWork() {
		return dayWork;
	}
	
	public void setDayWork(String[] dayWork) {
		this.dayWork = dayWork;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getBonus() {
		return bonus;
	}

	public void setBonus(Long bonus) {
		this.bonus = bonus;
	}

	public String getTotalSalary() {
		return totalSalary;
	}

	public void setTotalSalary(String totalSalary) {
		this.totalSalary = totalSalary;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
}
