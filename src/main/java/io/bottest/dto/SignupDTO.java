package io.bottest.dto;

public class SignupDTO {
	private static final long serialVersionUID = 1L;

	private String username;
	
	private String email;
	
	private String password;
	
	private String phone;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "SignupDTO [username=" + username + ", email=" + email + ", password=" + password + ", phone=" + phone
				+ "]";
	}

}
