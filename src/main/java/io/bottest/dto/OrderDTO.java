package io.bottest.dto;

import java.io.Serializable;

import io.bottest.jpa.entity.Payment;
import io.bottest.jpa.entity.Product;
import io.bottest.jpa.entity.User;

public class OrderDTO {
	
	private Long id;
	private User user;
	private Payment payment;
	private Product product;
	private Long amount;
	private Long price;
	private String status;
	public Long getId() {
		return id;
	}
	public void setId(Long long1) {
		this.id = long1;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
