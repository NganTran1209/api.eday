package io.bottest.configure.security.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

@Component
public class JwtTokenFilter extends GenericFilterBean {
	
	final public static String URL_PROJECT ="/project/";

	@Autowired
    private JwtTokenProvider jwtTokenProvider;
    

//    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider,ProjectService projectService) {
//        this.jwtTokenProvider = jwtTokenProvider;
//        this.projectService = projectService;
//    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
        throws IOException, ServletException {

        String token = jwtTokenProvider.resolveToken((HttpServletRequest) req);
        if (token != null && jwtTokenProvider.validateToken(token)) {
            Authentication auth = jwtTokenProvider.getAuthentication(token);

            if (auth != null) {
                SecurityContextHolder.getContext().setAuthentication(auth);
                
            }
            //checkProjectPermission( req,  res);
        }
        filterChain.doFilter(req, res);
    }
//
//	private boolean checkProjectPermission(ServletRequest req, ServletResponse res) {
//		HttpServletRequest httpRequest = (HttpServletRequest) req;
//		StringBuffer url = httpRequest.getRequestURL();
//
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		if (auth == null) {
//			return true;
//		}
//		UserDetails user = (UserDetails) auth.getPrincipal();
//		System.out.println("request user: "+ user.toString());
//		List<UserProject> userProjects = projectService.getProjectByUserName(user.getUsername());
//		
//		int indexStart = url.indexOf(URL_PROJECT);
//		if (indexStart > 0) {
//			int indexEnd = url.indexOf("/", indexStart+ URL_PROJECT.length());
//			if (indexEnd <= 0) {
//				indexEnd = url.length();
//			}
//			String projectId = url.substring(indexStart + URL_PROJECT.length(),indexEnd);
//			if(projectId.length() > 0) {
//				for (UserProject userProject : userProjects) {
//					if (projectId.equalsIgnoreCase(userProject.getProjectId())) {
//						return true;
//					}
//				}
//				throw new AccessDeniedException("You can not access this project");
//			}else {
//				return true;
//			}
//		}else {
//			return true;
//		}
//
//	}

}
