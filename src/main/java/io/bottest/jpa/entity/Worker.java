package io.bottest.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name = "worker")
@NamedQuery(name = "Worker.findAll", query = "SELECT w FROM Worker w")
public class Worker implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable=false)
	private Long id;
	
	private Long userId;
	
	private Long unit_salary;
	
	private String card_number;
	
	private String  branch_card;
	
	private Long experience;
	
	@Column(name = "create_day")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyyMMdd")
	private Date createDay;
	
	@Column(name = "end_day")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyyMMdd")
	private Date endDay;
	
	@OneToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "userId", referencedColumnName = "id",insertable=false, updatable=false)
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long long1) {
		this.userId = long1;
	}

	public Long getUnit_salary() {
		return unit_salary;
	}

	public void setUnit_salary(Long f) {
		this.unit_salary = f;
	}

	public String getCard_number() {
		return card_number;
	}

	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}

	public String getBranch_card() {
		return branch_card;
	}

	public void setBranch_card(String branch_card) {
		this.branch_card = branch_card;
	}

	public Long getExperience() {
		return experience;
	}

	public void setExperience(Long experience) {
		this.experience = experience;
	}

	public Date getCreateDay() {
		return createDay;
	}

	public void setCreateDay(Date createDay) {
		this.createDay = createDay;
	}

	public Date getEndDay() {
		return endDay;
	}

	public void setEndDay(Date endDay) {
		this.endDay = endDay;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Worker [id=" + id + ", userId=" + userId + ", unit_salary=" + unit_salary
				+ ", card_number=" + card_number + ", branch_card=" + branch_card + ", experience=" + experience
				+ ", createDay=" + createDay + ", endDay=" + endDay + ", user=" + user + "]";
	}
	
	
}
