package io.bottest.jpa.respository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.bottest.dto.FilterWareHouseDTO;
import io.bottest.jpa.entity.Product;
import io.bottest.jpa.entity.User;

@Component
public class WareHouseRepositoryImpl implements WareHouseRepositoryCustom{

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	ProductRepository productRepo;
	
	@Autowired
	UserRepository userRepo;
	
	@Override
	public List<Map<String, Object>> filterWarehouse(FilterWareHouseDTO filterWareHouse) {
		String condition = "";
		  String name = filterWareHouse.getName();
		  List<Integer> catId = filterWareHouse.getCatId();
		  Long amount = filterWareHouse.getAmount();
		  Long profit = filterWareHouse.getProfit();
		  int compareProfit = filterWareHouse.getCompareProfit();
		  Long stockQuantityFrom = filterWareHouse.getStockQuantityFrom();
		  Long stockQuantityTo = filterWareHouse.getStockQuantityTo();
		  String createdDateFrom = filterWareHouse.getCreatedDateFrom();
		  String createdDateTo = filterWareHouse.getCreatedDateTo();
		 
		  boolean checkTc = false;
		 
		  if(name != null) {
			  if(!name.isEmpty()) {
				  checkTc = true;
				  condition +=  "where p.name like '%" + name + "%'";
			  }
		  }else {
			  checkTc = true;
			  condition +=  "where p.name like '%%'" ;
		  }
		  
		  if(catId != null) {
			  if(catId.size() > 0) {
				  checkTc = true;
				  condition += " and (p.cat_id = "+catId.get(0)+"";
				  for(int i = 1; i < catId.size(); i++) {
					  condition += " or p.cat_id= "+catId.get(i)+"";
				  }
				  condition += ")";
			  }
		  }
		  
		  
		  if(profit != null) {
			  if(profit > 0) {
				  checkTc = true;
				  if(compareProfit == 1) {
					  condition += " and prW.profit <= "+compareProfit+"";
				  }else {
					  if(compareProfit == 2) {
						  condition += " and prW.profit >= "+compareProfit+"";
					  }else {
						  condition += " and prW.profit = "+compareProfit+"";
					  }
				  }
			  }
		  }
		  
		  if(stockQuantityFrom != null) {
			  if(stockQuantityFrom > 0) {
				  checkTc = true;
				  condition +=  " and prw.stock_quantity >= " + stockQuantityFrom + "";
			  }
		  }
		  
		  if(stockQuantityTo != null) {
			  if(stockQuantityTo > 0) {
				  checkTc = true;
				  condition +=  " and prw.stock_quantity <= " + stockQuantityTo + "";
			  }
		  }
		  
		  
		  if(createdDateFrom != null) {
			  if(!createdDateFrom.isEmpty()) {
				  checkTc = true;
				  condition +=  " and prW.created_date >= '" + createdDateFrom + "'";
			  }
		  }
		  
		  if(createdDateTo != null) {
			  if(!createdDateTo.isEmpty()) {
				  checkTc = true;
				  condition +=  " and prW.created_date <= '" + createdDateTo + "'";
			  }
		  }
		  
		  String query = "";
		  if(checkTc) {
			   query = "SELECT prW.id, prW.pro_id, prW.amount, prW.update_day, prW.created_date, prW.stock_quantity, prW.salerable_quantity, prW.total_money_buy,"
					   + " prW.total_money_sell, prW.profit, prW.owner_id "
					   + " FROM product_warehouse as prW left join product as p on prW.pro_id = p.id"
					   + " left join category as c on p.cat_id = c.id "
					   +" left join user as u on prW.owner_id = u.id "
					  +condition;
		  }
		List<Object> results = entityManager.createNativeQuery(query).getResultList();
		  List<Map<String, Object>> listOb = new ArrayList<Map<String,Object>>();
		  for(int i = 0; i < results.size(); i++) {
				Object[] result = (Object[]) results.get(i);
				
				Map<String, Object> ob = new HashMap<String, Object>();
				listOb = setDataToObject(ob, result, listOb);
				
			}
		  return listOb;
	}

	private List<Map<String, Object>> setDataToObject(Map<String, Object> ob, Object[] result,List<Map<String, Object>> listOb) {
		ob.put("id", result[0]);
		Product product = productRepo.findByProductId(Long.parseLong(String.valueOf(result[1])) );
		ob.put("product", product);
		ob.put("amount", result[2]);
		ob.put("updateDay", result[3]);
		ob.put("created_date", result[4]);
		ob.put("stockQuantity", result[5]);
		ob.put("salerableQuantity", result[6]);
		ob.put("totalMoneyBuy", result[7]);
		ob.put("totalMoneySell", result[8]);
		ob.put("profit", result[9]);
		User imple_staff = userRepo.findById(Long.parseLong(String.valueOf(result[10])));
		ob.put("imple_staff", imple_staff);
		listOb.add(ob);
		
		return listOb;
	}

}
