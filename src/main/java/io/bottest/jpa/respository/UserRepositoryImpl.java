package io.bottest.jpa.respository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import io.bottest.dto.FilterUserDTO;

@Component
public class UserRepositoryImpl implements UserRepositoryCustom{

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<Map<String, Object>> filterStaff(FilterUserDTO filterStaff, int role) {
		String condition = "";
		  String key = filterStaff.getKey();
		 
		  boolean checkTc = false;
		 
		  if(role > 0 ) {
			  checkTc = true;
			  if(role == 1) {
				  condition +=  "where (role='ROLE_STAFF' or role='ROLE_MANAGER') ";
			  }else {
				  if(role == 2) {
					  condition +=  "where (role='ROLE_CUSTOMER') ";
				  }
			  }
			  condition += " and enable=1 ";
		  }
		  if(key != null) {
			  if(!key.isEmpty()) {
				  checkTc = true;
				  condition +=  "and ( u.email like '%"+key+"%'";
				  condition += " or u.username like '%"+key+"%' )";
			  }
		  }
		  
		  
		  
		  
		  String query = "";
		  if(checkTc) {
			   query = "SELECT * FROM  user as u "
					  +condition;
		  }
		List<Object> results = entityManager.createNativeQuery(query).getResultList();
		  List<Map<String, Object>> listOb = new ArrayList<Map<String,Object>>();
		  for(int i = 0; i < results.size(); i++) {
				Object[] result = (Object[]) results.get(i);
				
				Map<String, Object> ob = new HashMap<String, Object>();
				listOb = setDataToObject(ob, result, listOb);
				
			}
		  return listOb;
	}

	private List<Map<String, Object>> setDataToObject(Map<String, Object> ob, Object[] result,List<Map<String, Object>> listOb) {
		ob.put("id", result[0]);
		ob.put("username", result[1]);
		ob.put("email", result[2]);
		ob.put("password", result[3]);
		ob.put("sex", result[4]);
		ob.put("facebook_url", result[5]);
		ob.put("role", result[6]);
		ob.put("birthDay", result[7]);
		ob.put("addr", result[8]);
		ob.put("startDay", result[9]);
		ob.put("enable", result[10]);
		ob.put("first_name", result[11]);
		ob.put("last_name", result[12]);
		ob.put("access_token", result[13]);
		ob.put("account_type", result[14]);
		ob.put("phone", result[15]);
		ob.put("avt", result[16]);
		ob.put("enabled", result[17]);
		
		listOb.add(ob);
		
		return listOb;
	}

}
