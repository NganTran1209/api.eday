package io.bottest.jpa.respository;

import java.util.List;
import java.util.Map;

import io.bottest.dto.FilterProductDTO;

public interface ProductRepositoryCustom {

	List<Map<String, Object>> filterProduct(FilterProductDTO filterProduct);

}
