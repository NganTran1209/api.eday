package io.bottest.jpa.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.jpa.entity.Worker;

@Transactional
@Repository
public interface WorkerRepository extends JpaRepository<Worker, Long> {

	@Query(value = "SELECT * FROM worker WHERE owner_id = :ownerId", nativeQuery = true)
	List<Worker> findAllWorkerByOwnerId(@Param("ownerId") Long id);

	@Query(value = "SELECT * FROM worker WHERE user_id = ?1", nativeQuery = true)
	Worker getStaff(Long userId);

	@Query(value = "SELECT * FROM worker WHERE user_id = ?1", nativeQuery = true)
	Worker getStaffById(int i);

	@Query(value = "SELECT * FROM worker WHERE user_id = ?1", nativeQuery = true)
	Worker findByUserId(Long id);

	@Query(value = "SELECT * FROM worker WHERE id = ?1", nativeQuery = true)
	Worker getWorkerById(Long workerId);
}
