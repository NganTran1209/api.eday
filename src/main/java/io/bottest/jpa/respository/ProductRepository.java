package io.bottest.jpa.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.bottest.dto.FilterProductDTO;
import io.bottest.dto.ProductDTO;
import io.bottest.jpa.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{

	@Query(value = "SELECT * FROM product ", nativeQuery = true)
	List<Product> getAllProduct();

	@Query(value= "SELECT * FROM  product where id =?1", nativeQuery = true)
	Product findByProductId(long id);

	@Query(value= "SELECT * FROM  product where cat_id =?1", nativeQuery = true)
	List<Product> getListProductByCatId(Long catId);


}
