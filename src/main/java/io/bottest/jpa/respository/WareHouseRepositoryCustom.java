package io.bottest.jpa.respository;

import java.util.List;
import java.util.Map;

import io.bottest.dto.FilterWareHouseDTO;

public interface WareHouseRepositoryCustom {

	List<Map<String, Object>> filterWarehouse(FilterWareHouseDTO filterWareHouse);

}
