package io.bottest.jpa.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import io.bottest.jpa.entity.ProductWareHouse;

@Repository
public interface ProductWareHouseResponsitory extends JpaRepository<ProductWareHouse, Long>{

	@Query(value= "SELECT * FROM  product_warehouse where pro_id =?1", nativeQuery = true)
	ProductWareHouse findByProductId(Long id);

}
