var listActions = [
    {
        "data": {
            "id": 2,
            "name": "open Url",
            "action_type_id": 3,
            "created_by": "System",
            "updated_by": "System",
            "delete_flag": "",
            "created_at": "2018-11-13T03:27:49.245Z",
            "updated_at": "2018-11-13T03:27:49.245Z",
            "description": "open {0}",
            "test_class": "AC_Open",
            "type": "in",
            
                
        },
        "params": [
            {
                "id": 2,
                "name": "Url",
                "test_action_id": 2,
                "param_type": "string",
            }
        ]
    },
    {
        "data": {
            "id": 3,
            "name": "enter value to item",
            "action_type_id": 3,
            "created_by": "System",
            "updated_by": "System",
            "delete_flag": "",
            "created_at": "2018-11-13T03:27:49.260Z",
            "updated_at": "2018-11-13T03:27:49.260Z",
            "description": "input {3} into {1}",
            "test_class": "AC_Enter",
            "type": "in",
            
        },
        "params": [
            {
                "id": 3,
                "name": "Screen",
                "test_action_id": 3,
                "param_type": "layout"
            },
            {
                "id": 4,
                "name": "Item",
                "test_action_id": 3,
                "param_type": "control"
            },
            {
                "id": 114,
                "name": "Index",
                "test_action_id": 3,
                "param_type": "number"
            },
            {
                "id": 5,
                "name": "Value",
                "test_action_id": 3,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 4,
            "name": "click",
            "action_type_id": 3,
            "created_by": "System",
            "updated_by": "System",
            "delete_flag": "",
            "created_at": "2018-11-13T03:27:49.260Z",
            "updated_at": "2018-11-13T03:27:49.260Z",
            "description": "click on {1}",
            "test_class": "AC_Click",
            "type": "in",
            
        },
        "params": [
            {
                "id": 6,
                "name": "Screen",
                "test_action_id": 4,
                "param_type": "layout"
            },
            {
                "id": 7,
                "name": "Item",
                "test_action_id": 4,
                "param_type": "control"
            },
            {
                "id": 114,
                "name": "Index",
                "test_action_id": 4,
                "param_type": "number"
            },
        ]
    },
    {
        "data": {
            "id": 5,
            "name": "take screenshot",
            "action_type_id": 3,
            "created_by": "System",
            "updated_by": "System",
            "delete_flag": "",
            "created_at": "2018-11-13T03:27:49.260Z",
            "updated_at": "2018-11-13T03:27:49.260Z",
            "description": "take screenshot",
            "test_class": "AC_TakeScreenshot",
            "type": "out",
            
        },
        "params": []
    },
    {
        "data": {
            "id": 6,
            "name": "close browser",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-11-14T04:20:26.188Z",
            "updated_at": "2018-11-14T04:20:26.188Z",
            "description": "close browser",
            "test_class": "AC_CloseBrowser",
            "type": "in",
            
        },
        "params": []
    },
    {
        "data": {
            "id": 7,
            "name": "set variable",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-11-14T05:21:17.991Z",
            "updated_at": "2018-11-14T05:21:17.991Z",
            "description": "set {2} for {0}",
            "test_class": "AC_SetVariable",
            "type": "in",
            
        },
        "params": [
            {
                "id": 8,
                "name": "Name",
                "test_action_id": 7,
                "param_type": "string"
            },
            {
                "id": 9,
                "name": "Variable Type",
                "test_action_id": 7,
                "param_type": "list=Set Value,Set Password"
            },
            {
                "id": 10,
                "name": "Value",
                "test_action_id": 7,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 8,
            "name": "if",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-11-21T09:57:36.490Z",
            "updated_at": "2018-11-21T09:57:36.490Z",
            "description": "check condition: {0} ",
            "test_class": "AC_If",
            "type": "in",
            
        },
        "params": [
            {
                "id": 11,
                "name": "Condition",
                "test_action_id": 8,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 9,
            "name": "end if",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2018-11-21T10:01:06.144Z",
            "updated_at": "2018-12-19T04:32:04.819Z",
            "description": "end if",
            "test_class": "AC_EndIf",
            "type": "in",
            
        },
        "params": []
    },
    {
        "data": {
            "id": 10,
            "name": "loop",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-11-21T10:01:31.494Z",
            "updated_at": "2018-11-21T10:01:31.494Z",
            "description": "loop {0} times",
            "test_class": "AC_Loop",
            "type": "in",
            
        },
        "params": [
            {
                "id": 12,
                "name": "Times",
                "test_action_id": 10,
                "param_type": "string"
            },
            {
                "id": 13,
                "name": "Variable",
                "test_action_id": 10,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 11,
            "name": "end loop",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-11-21T10:02:14.483Z",
            "updated_at": "2018-11-21T10:02:14.483Z",
            "description": "end loop",
            "test_class": "AC_EndLoop",
            "type": "in",
            
        },
        "params": []
    },
    {
        "data": {
            "id": 12,
            "name": "excel get range value",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-11-23T08:36:33.369Z",
            "updated_at": "2018-11-23T08:36:33.369Z",
            "description": "read {2} range from file [{0}][{1}]",
            "test_class": "AC_GetRangeValue",
            "type":"in",
            
        },
        "params": [
            {
                "id": 14,
                "name": "File name",
                "test_action_id": 12,
                "param_type": "string"
            },
            {
                "id": 15,
                "name": "Sheet name",
                "test_action_id": 12,
                "param_type": "string"
            },
            {
                "id": 16,
                "name": "Range",
                "test_action_id": 12,
                "param_type": "string"
            },
            {
                "id": 17,
                "name": "Result",
                "test_action_id": 12,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 25,
            "name": "excel get cell value",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-03-22T03:59:53.626Z",
            "updated_at": "2019-03-22T03:59:53.626Z",
            "description": "get value at {2} cell from file [{0}][{1}]",
            "test_class": "AC_GetCellValue",
            "type": "in",
            
        },
        "params": [
            {
                "id": 44,
                "name": "File name",
                "test_action_id": 25,
                "param_type": "string"
            },
            {
                "id": 45,
                "name": "Sheet name",
                "test_action_id": 25,
                "param_type": "string"
            },
            {
                "id": 46,
                "name": "Row",
                "test_action_id": 25,
                "param_type": "string"
            },
            {
                "id": 47,
                "name": "Column",
                "test_action_id": 25,
                "param_type": "string"
            },
            {
                "id": 48,
                "name": "Result",
                "test_action_id": 25,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 26,
            "name": "excel check cell value",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-03-22T04:02:06.804Z",
            "updated_at": "2019-03-22T04:02:06.804Z",
            "description": "check {3} equal value of {2} cell in file [{0}][{1}]",
            "test_class": "AC_CheckCellValue",
            "type": "out",
            
        },
        "params": [
            {
                "id": 49,
                "name": "File name",
                "test_action_id": 26,
                "param_type": "string"
            },
            {
                "id": 50,
                "name": "Sheet name",
                "test_action_id": 26,
                "param_type": "string"
            },
            {
                "id": 51,
                "name": "Row",
                "test_action_id": 26,
                "param_type": "string"
            },
            {
                "id": 52,
                "name": "Column",
                "test_action_id": 26,
                "param_type": "string"
            },
            {
                "id": 53,
                "name": "Result",
                "test_action_id": 26,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 49,
            "name": "excel set cell value",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-06-17T05:33:37.762Z",
            "updated_at": "2019-06-17T05:33:37.762Z",
            "description": "write {3} into {2} cell of file [{0}][{1}]",
            "test_class": "AC_SetCellValue",
            "type": "in",
            
        },
        "params": [
            {
                "id": 109,
                "name": "File name",
                "test_action_id": 49,
                "param_type": "filepath"
            },
            {
                "id": 110,
                "name": "Sheet name",
                "test_action_id": 49,
                "param_type": "string"
            },
            {
                "id": 111,
                "name": "Row",
                "test_action_id": 49,
                "param_type": "string"
            },
            {
                "id": 112,
                "name": "Column",
                "test_action_id": 49,
                "param_type": "string"
            },
            {
                "id": 113,
                "name": "Value",
                "test_action_id": 49,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 56,
            "name": "excel set range value",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
             "delete_flag": "",
            "created_at": "2019-07-03T07:08:49.176Z",
            "updated_at": "2019-07-03T07:08:49.176Z",
            "description": "excel: write {3} into {2} of file [{0}][{1}]",
            "test_class": "AC_SetRangeValue",
            "type": "in",
            
        },
        "params": [
            {
                "id": 125,
                "name": "File name",
                "test_action_id": 56,
                "param_type": "filepath"
            },
            {
                "id": 126,
                "name": "Sheet name",
                "test_action_id": 56,
                "param_type": "string"
            },
            {
                "id": 127,
                "name": "Row",
                "test_action_id": 56,
                "param_type": "string"
            },
            {
                "id": 128,
                "name": "Column",
                "test_action_id": 56,
                "param_type": "string"
            },
            {
                "id": 129,
                "name": "Value",
                "test_action_id": 56,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 13,
            "name": "sleep",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-12-04T04:51:21.144Z",
            "updated_at": "2018-12-04T04:51:21.144Z",
            "description": "wait {0} miliseconds",
            "test_class": "AC_Sleep",
            "type": "in",
            
        },
        "params": [
            {
                "id": 18,
                "name": "Miliseconds",
                "test_action_id": 13,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 14,
            "name": "select",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-12-04T09:08:56.670Z",
            "updated_at": "2018-12-04T09:08:56.670Z",
            "description": "select {3} in {1} dropdown",
            "test_class": "AC_Select",
            "type": "in",
            
        },
        "params": [
            {
                "id": 19,
                "name": "Screen",
                "test_action_id": 14,
                "param_type": "layout"
            },
            {
                "id": 20,
                "name": "Item",
                "test_action_id": 14,
                "param_type": "control"
            },
             {
                "id": 114,
                "name": "Index",
                "test_action_id": 14,
                "param_type": "number"
            },
            {
                "id": 21,
                "name": "Value",
                "test_action_id": 14,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 15,
            "name": "set checkbox on/off",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
           "delete_flag": "",
            "created_at": "2018-12-11T07:13:10.937Z",
            "updated_at": "2018-12-11T07:13:10.937Z",
            "description": "set {3} for {1}",
            "test_class": "AC_SetCheckboxValue",
            "type": "in",
            
        },
        "params": [
            {
                "id": 22,
                "name": "Screen",
                "test_action_id": 15,
                "param_type": "layout"
            },
            {
                "id": 23,
                "name": "Item",
                "test_action_id": 15,
                "param_type": "control"
            },
            {
                "id": 114,
                "name": "Index",
                "test_action_id": 15,
                "param_type": "number"
            },
            {
                "id": 24,
                "name": "Value",
                "test_action_id": 15,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 16,
            "name": "get checkbox value",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-12-11T07:14:35.685Z",
            "updated_at": "2018-12-11T07:14:35.685Z",
            "description": "get value {1} to {3}",
            "test_class": "AC_GetCheckboxValue",
            "type": "in",
            
        },
        "params": [
            {
                "id": 25,
                "name": "Screen",
                "test_action_id": 16,
                "param_type": "layout"
            },
            {
                "id": 26,
                "name": "Item",
                "test_action_id": 16,
                "param_type": "control"
            },
               {
                "id": 114,
                "name": "Index",
                "test_action_id": 16,
                "param_type": "number"
            },
            {
                "id": 27,
                "name": "Variable",
                "test_action_id": 16,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 17,
            "name": "check value",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
             "delete_flag": "",
            "created_at": "2018-12-11T07:16:02.792Z",
            "updated_at": "2018-12-11T07:16:02.792Z",
            "description": "check {0} {1} {2}",
            "test_class": "AC_CheckValue",
            "type": "out",
            
        },
        "params": [
            {
                "id": 28,
                "name": "Variable",
                "test_action_id": 17,
                "param_type": "string"
            },
            {
                "id": 95,
                "name": "Operator",
                "test_action_id": 17,
                "param_type": "list=equal,greater than,less than,greater than or equal,less than or equal,contain,incontain,wildcard"
            },
            {
                "id": 96,
                "name": "Expected",
                "test_action_id": 17,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 18,
            "name": "set javascript variable",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-12-11T07:18:10.242Z",
            "updated_at": "2018-12-11T07:18:10.242Z",
            "description": "set {0} for {1} js",
            "test_class": "AC_SetJavascriptVariable",
            "type": "in",
            
        },
        "params": [
            {
                "id": 30,
                "name": "Variable",
                "test_action_id": 18,
                "param_type": "string"
            },
            {
                "id": 31,
                "name": "Value",
                "test_action_id": 18,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 19,
            "name": "execute javascript",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-12-11T07:18:49.209Z",
            "updated_at": "2018-12-11T07:18:49.209Z",
            "description": "execute javascript",
            "test_class": "AC_ExecuteJavascript",
            "type": "in",
            
        },
        "params": [
            {
                "id": 32,
                "name": "Script",
                "test_action_id": 19,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 20,
            "name": "get javascript variable",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2018-12-11T07:19:24.755Z",
            "updated_at": "2018-12-11T07:19:24.755Z",
            "description": "get {0} js to {1}",
            "test_class": "AC_GetJavascriptVariable",
            "type": "in",
            
        },
        "params": [
            {
                "id": 33,
                "name": "Variable",
                "test_action_id": 20,
                "param_type": "string"
            },
            {
                "id": 34,
                "name": "Result",
                "test_action_id": 20,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 21,
            "name": "execute testcase",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2019-02-12T08:30:13.553Z",
            "updated_at": "2019-02-12T08:32:31.246Z",
            "description": "execute {0} testcase",
            "test_class": "AC_ExecuteTestcase",
            "type": "in",
            
        },
        "params": [
            {
                "id": 35,
                "name": "Testcase",
                "test_action_id": 21,
                "param_type": "testcase"
            }
        ]
    },
    {
        "data": {
            "id": 22,
            "name": "take full screenshot",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
             "delete_flag": "",
            "created_at": "2019-03-14T03:43:11.472Z",
            "updated_at": "2019-03-14T03:43:28.785Z",
            "description": "take full screenshot",
            "test_class": "AC_TakeFullScreen",
            "type": "out",
            
        },
        "params": []
    },
    {
        "data": {
            "id": 29,
            "name": "read csv file",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-03-22T04:17:28.264Z",
            "updated_at": "2019-03-22T04:17:28.264Z",
            "description": "read {0} file into {1}",
            "test_class": "AC_GetCsvFile",
            "type": "in",
            
        },
        "params": [
            {
                "id": 60,
                "name": "File name",
                "test_action_id": 29,
                "param_type": "string"
            },
            {
                "id": 61,
                "name": "Result",
                "test_action_id": 29,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 42,
            "name": "write to csv",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-05-15T08:47:15.109Z",
            "updated_at": "2019-05-15T08:47:15.109Z",
            "description": "write {0} to {1}",
            "test_class": "AC_WriteToCSV",
            "type": "in",
            
        },
        "params": [
            {
                "id": 85,
                "name": "Data",
                "test_action_id": 42,
                "param_type": "string"
            },
            {
                "id": 86,
                "name": "File name",
                "test_action_id": 42,
                "param_type": "filepath"
            },
            {
                "id": 87,
                "name": "Append",
                "test_action_id": 42,
                "param_type": "list=TRUE,FALSE"
            }
        ]
    },
    {
        "data": {
            "id": 30,
            "name": "set control value",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2019-03-22T04:19:39.917Z",
            "updated_at": "2019-03-22T04:32:26.549Z",
            "description": "set {3} to {1} item",
            "test_class": "AC_SetControlValue",
            "type": "in",
            
        },
        "params": [
            {
                "id": 62,
                "name": "Screen",
                "test_action_id": 30,
                "param_type": "layout"
            },
            {
                "id": 63,
                "name": "Item",
                "test_action_id": 30,
                "param_type": "control"
            },
                 {
                "id": 114,
                "name": "Index",
                "test_action_id": 30,
                "param_type": "number"
            },
            {
                "id": 64,
                "name": "Value",
                "test_action_id": 30,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 32,
            "name": "get control value",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
             "delete_flag": "",
            "created_at": "2019-03-22T04:22:02.176Z",
            "updated_at": "2019-03-22T04:34:10.953Z",
            "description": "get value of {1} item to {3}",
            "test_class": "AC_GetControlValue",
            "type": "in",
            
        },
        "params": [
            {
                "id": 65,
                "name": "Screen",
                "test_action_id": 32,
                "param_type": "layout"
            },
            {
                "id": 66,
                "name": "Item",
                "test_action_id": 32,
                "param_type": "control"
            },
             {
                "id": 114,
                "name": "Index",
                "test_action_id": 32,
                "param_type": "number"
            },
            {
                "id": 67,
                "name": "Return Variable",
                "test_action_id": 32,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 33,
            "name": "check control value",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-03-22T04:35:28.402Z",
            "updated_at": "2019-03-22T04:35:28.402Z",
            "description": "check value of {1}[{2}] item {3} {4}",
            "test_class": "AC_CheckControlValue",
            "type": "out",
            
        },
        "params": [
            {
                "id": 68,
                "name": "Screen",
                "test_action_id": 33,
                "param_type": "layout"
            },
            {
                "id": 69,
                "name": "Item",
                "test_action_id": 33,
                "param_type": "control"
            },
             {
                "id": 114,
                "name": "Index",
                "test_action_id": 33,
                "param_type": "number"
            },
            {
                "id": 93,
                "name": "Operator",
                "test_action_id": 33,
                "param_type": "list=equal,greater than,less than,greater than or equal,less than or equal,contain,incontain,wildcard"
            },
            {
                "id": 94,
                "name": "Expected",
                "test_action_id": 33,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 34,
            "name": "compare image",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
             "delete_flag": "",
            "created_at": "2019-03-22T04:37:10.227Z",
            "updated_at": "2019-03-22T04:37:10.227Z",
            "description": "compare image: {0} with {1}",
            "test_class": "AC_CompareImage",
            "type": "out",
            
        },
        "params": [
            {
                "id": 71,
                "name": "Recorded image",
                "test_action_id": 34,
                "param_type": "string"
            },
            {
                "id": 72,
                "name": "Expected image",
                "test_action_id": 34,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 35,
            "name": "take screenshot to variable",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-03-22T04:39:17.429Z",
            "updated_at": "2019-03-22T04:39:17.429Z",
            "description": "take screenshot to {0}",
            "test_class": "AC_TakeScreenshotWithResult",
            "type": "out",
            
        },
        "params": [
            {
                "id": 73,
                "name": "Image name",
                "test_action_id": 35,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 36,
            "name": "connect database",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-03-22T04:40:30.989Z",
            "updated_at": "2019-03-22T04:40:30.989Z",
            "description": "connect database {2}",
            "test_class": "AC_ConnectDatabase",
            "type": "in",
            
        },
        "params": [
        	{
                "id": 74,
                "name": "Connection name",
                "test_action_id": 36,
                "param_type": "string"
            },
            {
                "id": 75,
                "name": "Type sql",
                "test_action_id": 36,
                "param_type": "list=sqlserver,mysql,oracle,mongodb"
            },
            {
                "id": 76,
                "name": "Server string",
                "test_action_id": 36,
                "param_type": "string"
            },
            {
                "id": 77,
                "name": "Database name",
                "test_action_id": 36,
                "param_type": "string"
            },
            {
                "id": 78,
                "name": "Username",
                "test_action_id": 36,
                "param_type": "string"
            },
            {
                "id": 79,
                "name": "Password",
                "test_action_id": 36,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 37,
            "name": "execute query",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-03-22T04:43:04.940Z",
            "updated_at": "2019-03-22T04:43:04.940Z",
            "description": "execute query",
            "test_class": "AC_ExecuteQuery",
            "type": "in",
            
        },
        "params": [
        	{
                "id": 80,
                "name": "Connection name",
                "test_action_id": 37,
                "param_type": "string"
            },
            {
                "id": 80,
                "name": "Type",
                "test_action_id": 37,
                "param_type": "list=text,file"
            },
            {
                "id": 81,
                "name": "Query string",
                "test_action_id": 37,
                "param_type": "string"
            },
            {
                "id": 82,
                "name": "Result",
                "test_action_id": 37,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 38,
            "name": "check control attribute",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2019-04-23T06:41:57.082Z",
            "updated_at": "2019-04-23T07:18:11.838Z",
            "description": "Check {3} attribute of {1} equal {4}",
            "test_class": "AC_CheckControlAttribute",
            "type": "out",
            
        },
        "params": [
            {
                "id": 83,
                "name": "Screen",
                "test_action_id": 38,
                "param_type": "layout"
            },
            {
                "id": 84,
                "name": "Item",
                "test_action_id": 38,
                "param_type": "control"
            },
              {
                "id": 114,
                "name": "Index",
                "test_action_id": 38,
                "param_type": "number"
            },
            {
                "id": 85,
                "name": "Attribute",
                "test_action_id": 38,
                "param_type": "string"
            },
            {
                "id": 86,
                "name": "Value",
                "test_action_id": 38,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 43,
            "name": "check format",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-05-22T04:06:27.102Z",
            "updated_at": "2019-05-22T04:06:27.102Z",
            "description": "is {1} has {0} format?",
            "test_class": "AC_CheckValueFormat",
            "type": "out",
            
        },
        "params": [
            {
                "id": 87,
                "name": "Format",
                "test_action_id": 43,
                "param_type": "string"
            },
            {
                "id": 88,
                "name": "Value or variable",
                "test_action_id": 43,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 44,
            "name": "get screen table",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-05-24T03:47:15.115Z",
            "updated_at": "2019-05-24T03:47:15.115Z",
            "description": "get value of {1} table to {3}",
            "test_class": "AC_GetTableData",
            "type": "in",
            
        },
        "params": [
            {
                "id": 90,
                "name": "Screen",
                "test_action_id": 44,
                "param_type": "layout"
            },
            {
                "id": 91,
                "name": "Item",
                "test_action_id": 44,
                "param_type": "control"
            },
           {
                "id": 114,
                "name": "Index",
                "test_action_id": 44,
                "param_type": "number"
            },
            {
                "id": 92,
                "name": "Value",
                "test_action_id": 44,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 45,
            "name": "ftp connect",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-06-12T10:47:03.493Z",
            "updated_at": "2019-06-12T10:47:03.493Z",
            "description": "connect ftp server: {0}",
            "test_class": "AC_ConnectFTP",
            "type": "in",
            
        },
        "params": [
        	{
                "id": 93,
                "name": "Connect name",
                "test_action_id": 45,
                "param_type": "string"
            },
            {
                "id": 94,
                "name": "Host",
                "test_action_id": 45,
                "param_type": "string"
            },
            {
                "id": 95,
                "name": "Port",
                "test_action_id": 45,
                "param_type": "string"
            },
            {
                "id": 96,
                "name": "Username",
                "test_action_id": 45,
                "param_type": "string"
            },
            {
                "id": 97,
                "name": "Password",
                "test_action_id": 45,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 46,
            "name": "ftp download file",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2019-06-12T10:49:45.418Z",
            "updated_at": "2019-06-25T08:48:19.566Z",
            "description": "ftp: download {2} at {1}",
            "test_class": "AC_DowloadFTPFile",
            "type": "in",
            
        },
        "params": [
        	{
                "id": 98,
                "name": "Connect name",
                "test_action_id": 46,
                "param_type": "string"
            },
            {
                "id": 99,
                "name": "Remote Path",
                "test_action_id": 46,
                "param_type": "string"
            },
            {
                "id": 100,
                "name": "Local path",
                "test_action_id": 46,
                "param_type": "filepath"
            },
            {
                "id": 101,
                "name": "File name",
                "test_action_id": 46,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 47,
            "name": "ftp download folders",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2019-06-12T10:53:54.282Z",
            "updated_at": "2019-06-25T08:48:50.359Z",
            "description": "ftp download {0}",
            "test_class": "AC_DowloadFTPFiles",
            "type": "in",
            
        },
        "params": [
        	{
                "id": 102,
                "name": "Connect name",
                "test_action_id": 47,
                "param_type": "string"
            },
            {
                "id": 103,
                "name": "Remote path",
                "test_action_id": 47,
                "param_type": "string"
            },
            {
                "id": 104,
                "name": "Local path",
                "test_action_id": 47,
                "param_type": "filepath"
            }
        ]
    },
    {
        "data": {
            "id": 48,
            "name": "ftp upload file",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "testing",
            "delete_flag": "",
            "created_at": "2019-06-12T10:55:31.687Z",
            "updated_at": "2019-06-25T08:49:15.394Z",
            "description": "ftp upload {1}",
            "test_class": "AC_UploadFTPFile",
            "type": "in",
            
        },
        "params": [
        	{
                "id": 105,
                "name": "Connect name",
                "test_action_id": 48,
                "param_type": "string"
            },
            {
                "id": 106,
                "name": "Remote path",
                "test_action_id": 48,
                "param_type": "string"
            },
            {
                "id": 107,
                "name": "Local path",
                "test_action_id": 48,
                "param_type": "filepath"
            }
        ]
    },
    {
        "data": {
            "id": 60,
            "name": "ftp get folders",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-05T10:13:34.816Z",
            "updated_at": "2019-07-05T10:13:34.816Z",
            "description": "ftp: get folders in {0}",
            "test_class": "AC_GetFtpFolders",
            "type": "in",
            
        },
        "params": [
        	{
                "id": 135,
                "name": "Connect name",
                "test_action_id": 60,
                "param_type": "string"
            },
            {
                "id": 136,
                "name": "Remote path",
                "test_action_id": 60,
                "param_type": "string"
            },
            {
                "id": 137,
                "name": "Result",
                "test_action_id": 60,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 66,
            "name": "ftp create folder",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
             "delete_flag": "",
            "created_at": "2019-08-02T04:14:17.469Z",
            "updated_at": "2019-08-02T04:14:17.469Z",
            "description": "create {0} in ftp ",
            "test_class": "AC_CreateFolderFTP",
            "type": "in",
            
        },
        "params": [
        	{
                "id": 154,
                "name": "Connect name",
                "test_action_id": 66,
                "param_type": "string"
            },
            {
                "id": 155,
                "name": "Dirname",
                "test_action_id": 66,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 67,
            "name": "ftp delete",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-02T04:15:20.026Z",
            "updated_at": "2019-08-02T04:15:20.026Z",
            "description": "delete {0} in ftp",
            "test_class": "AC_DeleteInFTP",
            "type": "in",
            
        },
        "params": [
        	{
                "id": 156,
                "name": "Connect name",
                "test_action_id": 67,
                "param_type": "string"
            },
            {
                "id": 157,
                "name": "Dirname",
                "test_action_id": 67,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 73,
            "name": "ftp check file exist",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-06T08:12:27.691Z",
            "updated_at": "2019-08-06T08:12:27.691Z",
            "description": "check {1} type exist on ftp {0}",
            "test_class": "AC_CheckFileExistFTP",
            "type": "out",
            
        },
        "params": [
        	{
                "id": 166,
                "name": "Connect name",
                "test_action_id": 73,
                "param_type": "string"
            },
            {
                "id": 167,
                "name": "Remote path",
                "test_action_id": 73,
                "param_type": "string"
            },
            {
                "id": 168,
                "name": "Type file",
                "test_action_id": 73,
                "param_type": "string"
            },
            {
                "id": 169,
                "name": "Result",
                "test_action_id": 73,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 50,
            "name": "capture image of control",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-06-17T05:52:16.946Z",
            "updated_at": "2019-06-17T05:52:16.946Z",
            "description": "capture image of {1}",
            "test_class": "AC_CaptureControl",
            "type": "out",
            
        },
        "params": [
            {
                "id": 113,
                "name": "Screen",
                "test_action_id": 50,
                "param_type": "layout"
            },
            {
                "id": 114,
                "name": "Item",
                "test_action_id": 50,
                "param_type": "control"
            },
           {
                "id": 114,
                "name": "Index",
                "test_action_id": 50,
                "param_type": "number"
            },
            {
                "id": 115,
                "name": "File name",
                "test_action_id": 50,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 51,
            "name": "get attribute of element",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
             "delete_flag": "",
            "created_at": "2019-06-18T03:44:09.216Z",
            "updated_at": "2019-06-18T03:44:09.216Z",
            "description": "get {3} in {1}",
            "test_class": "AC_GetAttribute",
            "type": "in",
            
        },
        "params": [
            {
                "id": 116,
                "name": "Screen",
                "test_action_id": 51,
                "param_type": "layout"
            },
            {
                "id": 117,
                "name": "Item",
                "test_action_id": 51,
                "param_type": "control"
            },
            {
                "id": 114,
                "name": "Index",
                "test_action_id": 51,
                "param_type": "number"
            },
            {
                "id": 118,
                "name": "Attribute",
                "test_action_id": 51,
                "param_type": "string"
            },
            {
                "id": 119,
                "name": "Value",
                "test_action_id": 51,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 52,
            "name": "while",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
             "delete_flag": "",
            "created_at": "2019-07-02T03:56:41.625Z",
            "updated_at": "2019-07-02T03:56:41.625Z",
            "description": "loop with condition {0}",
            "test_class": "AC_While",
            "type": "in",
            
        },
        "params": [
            {
                "id": 120,
                "name": "Condition",
                "test_action_id": 52,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 53,
            "name": "end while",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-02T03:58:57.381Z",
            "updated_at": "2019-07-02T03:58:57.381Z",
            "description": "end while",
            "test_class": "AC_EndWhile",
            "type": "in",
            
        },
        "params": []
    },
    {
        "data": {
            "id": 54,
            "name": "get list files",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-02T04:48:44.804Z",
            "updated_at": "2019-07-02T04:48:44.804Z",
            "description": "get {1} files in {0}",
            "test_class": "AC_GetFilesInFolder",
            "type": "in"
        },
        "params": [
            {
                "id": 121,
                "name": "Path",
                "test_action_id": 54,
                "param_type": "filepath"
            },
            {
                "id": 122,
                "name": "Type",
                "test_action_id": 54,
                "param_type": "string"
            },
            {
                "id": 123,
                "name": "Result",
                "test_action_id": 54,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 55,
            "name": "send keys",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-03T04:22:49.289Z",
            "updated_at": "2019-07-03T04:22:49.289Z",
            "description": "send {0} keys",
            "test_class": "AC_Type",
            "type": "in",
            
        },
        "params": [
            {
                "id": 124,
                "name": "Value",
                "test_action_id": 55,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 61,
            "name": "get control by",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-09T03:43:15.004Z",
            "updated_at": "2019-07-09T03:43:15.004Z",
            "description": "get control {0} by {1}:{2}",
            "test_class": "AC_GetControlBy",
            "type": "in",
            
        },
        "params": [
            {
                "id": 137,
                "name": "Control name",
                "test_action_id": 61,
                "param_type": "string"
            },
            {
                "id": 138,
                "name": "Detect by",
                "test_action_id": 61,
                "param_type": "list=id,href,xpath,class,name,text,css"
            },
            {
                "id": 139,
                "name": "Value",
                "test_action_id": 61,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 62,
            "name": "define email sender",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-15T02:36:37.915Z",
            "updated_at": "2019-07-15T02:36:37.915Z",
            "description": "define sender: {2}",
            "test_class": "AC_EmailFromContact",
            "type": "in",
            
        },
        "params": [
        	{
                "id": 141,
                "name": "Connect email",
                "test_action_id": 62,
                "param_type": "string"
            },
            {
                "id": 142,
                "name": "Smtp server",
                "test_action_id": 62,
                "param_type": "string"
            },
            {
                "id": 143,
                "name": "Port",
                "test_action_id": 62,
                "param_type": "string"
            },
            {
                "id": 144,
                "name": "Email",
                "test_action_id": 62,
                "param_type": "string"
            },
            {
                "id": 145,
                "name": "Password",
                "test_action_id": 62,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 63,
            "name": "send email",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-15T02:38:51.348Z",
            "updated_at": "2019-07-15T02:38:51.348Z",
            "description": "send email to {1}",
            "test_class": "AC_SendEmail",
            "type": "in",
            
        },
        "params": [
        	{
                "id": 147,
                "name": "Connect email",
                "test_action_id": 63,
                "param_type": "string"
            },
            {
                "id": 148,
                "name": "To",
                "test_action_id": 63,
                "param_type": "string"
            },
            {
                "id": 149,
                "name": "Subject",
                "test_action_id": 63,
                "param_type": "string"
            },
            {
                "id": 150,
                "name": "Content",
                "test_action_id": 63,
                "param_type": "string"
            },
            {
                "id": 151,
                "name": "Attatch file",
                "test_action_id": 63,
                "param_type": "filepath"
            }
        ]
    },
    {
        "data": {
            "id": 64,
            "name": "get local folders",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-16T08:40:06.075Z",
            "updated_at": "2019-07-16T08:40:06.075Z",
            "description": "get all folders in {0}",
            "test_class": "AC_GetFolders",
            "type": "in",
            
        },
        "params": [
            {
                "id": 153,
                "name": "Path",
                "test_action_id": 64,
                "param_type": "string"
            },
            {
                "id": 154,
                "name": "Result",
                "test_action_id": 64,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 65,
            "name": "copy file",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-07-31T08:47:24.470Z",
            "updated_at": "2019-07-31T08:47:24.470Z",
            "description": "copy {0} file to {1}",
            "test_class": "AC_CopyFile",
            "type": "in",
            
        },
        "params": [
            {
                "id": 155,
                "name": "File path",
                "test_action_id": 65,
                "param_type": "filepath"
            },
            {
                "id": 156,
                "name": "New file",
                "test_action_id": 65,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 68,
            "name": "get all file in folder",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-02T07:33:59.014Z",
            "updated_at": "2019-08-02T07:33:59.014Z",
            "description": "get all file in {0}",
            "test_class": "AC_GetAllFileInFolder",
            "type": "in",
            
        },
        "params": [
            {
                "id": 157,
                "name": "Path",
                "test_action_id": 68,
                "param_type": "filepath"
            },
            {
                "id": 158,
                "name": "Result",
                "test_action_id": 68,
                "param_type": "string"
            }
        ]
    },
    {
        "data": {
            "id": 69,
            "name": "delete file",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-02T09:28:06.476Z",
            "updated_at": "2019-08-02T09:28:06.476Z",
            "description": "delete {0}",
            "test_class": "AC_DeleteFile",
            "type": "in",
            
        },
        "params": [
            {
                "id": 159,
                "name": "File path",
                "test_action_id": 69,
                "param_type": "filepath"
            }
        ]
    },
    {
        "data": {
            "id": 70,
            "name": "move folder",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-05T06:30:43.705Z",
            "updated_at": "2019-08-05T06:30:43.705Z",
            "description": "move {0} into {1}",
            "test_class": "AC_MoveFolder",
            "type": "in",
            
        },
        "params": [
            {
                "id": 160,
                "name": "Folder path",
                "test_action_id": 70,
                "param_type": "string"
            },
            {
                "id": 161,
                "name": "New directory",
                "test_action_id": 70,
                "param_type": "string"
            }
        ]
    },
    
    {
        "data": {
            "id": 73,
            "name": "use testdata",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-06T04:57:31.512Z",
            "updated_at": "2019-08-06T04:57:31.512Z",
            "description": "use {1} testdata of {0}",
            "test_class": "AC_UseTestData",
            "type": "in",
            
        },
        "params": [
            {
                "id": 166,
                "name": "Screen",
                "test_action_id": 73,
                "param_type": "testdata"
            },
            {
                "id": 167,
                "name": "Casenum",
                "test_action_id": 73,
                "param_type": "casenum"
            }
        ]
    },
    {
        "data": {
            "id": 74,
            "name": "check testdata",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-06T04:57:31.512Z",
            "updated_at": "2019-08-06T04:57:31.512Z",
            "description": "check {1} testdata of {0}",
            "test_class": "AC_CheckData",
            "type": "in",
            
        },
        "params": [
            {
                "id": 168,
                "name": "Screen",
                "test_action_id": 74,
                "param_type": "testdata"
            },
            {
                "id": 169,
                "name": "Casenum",
                "test_action_id": 74,
                "param_type": "casenum"
            }
        ]
    },
    {
        "data": {
            "id": 75,
            "name": "Restfull API",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-06T04:57:31.512Z",
            "updated_at": "2019-08-06T04:57:31.512Z",
            "description": "call API {0} {1} testdata of {6}",
            "test_class": "AC_API",
            "type": "in",
            
        },
        "params": [
            {
                "id":170,
                "test_action_id": 75,
                "name": "Method",
                "param_type": "list=POST,GET,PUT,UPDATE,DELETE"
            },
            {
                "id":180,
                "test_action_id": 75,
                "name": "url",
                "param_type": "string"
            },
            {
                "name": "Params",
                "param_type":"key-value"
            },
            {
                "name": "Authorization",
                "param_type":"auth",
            },
            {
                "name": "Header",
                "param_type":"header",
            },
            {
                "name": "Body",
                "param_type":"body"
            },
            //Return to a json value into #variableName
            {
                "name": "output",
                "param_type": "variable"
            }
           
        ]
         },
    {
        "data": {
            "id": 76,
            "name": "right click",
            "action_type_id": 3,
            "created_by": "Administrator",
            "updated_by": "Administrator",
            "delete_flag": "",
            "created_at": "2019-08-06T04:57:31.512Z",
            "updated_at": "2019-08-06T04:57:31.512Z",
            "description": "click on {1}",
            "test_class": "AC_RightClick",
            "type": "in",
            
        },
        "params": [
            {
                "id": 168,
                "name": "Screen",
                "test_action_id": 76,
                "param_type": "layout"
            },
            {
                "id": 169,
                "name": "Item",
                "test_action_id": 76,
                "param_type": "control"
            },
            {
                "id": 114,
                "name": "Index",
                "test_action_id": 38,
                "param_type": "number"
            }
        ]
    },
         {
             "data": {
                 "id": 77,
                 "name": "find elements by",
                 "action_type_id": 3,
                 "created_by": "Administrator",
                 "updated_by": "Administrator",
                 "delete_flag": "",
                 "created_at": "2019-07-09T03:43:15.004Z",
                 "updated_at": "2019-07-09T03:43:15.004Z",
                 "description": "find elements {0} by {3}",
                 "test_class": "AC_FindElementsBy",
                 "type": "in",
                 
             },
             "params": [
                 {
                     "id": 137,
                     "name": "Return name",
                     "test_action_id": 77,
                     "param_type": "string"
                 },
                 {
                     "id": 137,
                     "name": "From parent",
                     "test_action_id": 77,
                     "param_type": "string"
                 },
                 {
                     "id": 137,
                     "name": "Index",
                     "test_action_id": 77,
                     "param_type": "string"
                 },
                 {
                     "id": 139,
                     "name": "Css Selector",
                     "test_action_id": 77,
                     "param_type": "string"
                 }
             ]
         },
         {
             "data": {
                 "id": 12,
                 "name": "excel get column",
                 "action_type_id": 3,
                 "created_by": "Administrator",
                 "updated_by": "Administrator",
                 "delete_flag": "",
                 "created_at": "2018-11-23T08:36:33.369Z",
                 "updated_at": "2018-11-23T08:36:33.369Z",
                 "description": "read {2} range from file [{0}][{1}]",
                 "test_class": "AC_GetColumn",
                 "type":"in",
                 
             },
             "params": [
                 {
                     "id": 14,
                     "name": "File name",
                     "test_action_id": 12,
                     "param_type": "filepath"
                 },
                 {
                     "id": 15,
                     "name": "Sheet name",
                     "test_action_id": 12,
                     "param_type": "string"
                 },
                 {
                     "id": 16,
                     "name": "Range",
                     "test_action_id": 12,
                     "param_type": "string"
                 },
                 {
                     "id": 17,
                     "name": "Result",
                     "test_action_id": 12,
                     "param_type": "string"
                 }
             ]
         }
];

 var actionHelp={
        "AC_Open": {
            "document": {
                " Url": "A url address,  example:  https://www.google.com.vn/"
            	},
            "picture": "/assets/img/AC_Open.PNG"
        	},
        "AC_Enter": {
            "document":{
                "Screen": "The screen that was created in SCREEN DESIGN.",
                "Item": "Item in the screen.",
                "Index": "",
                "Value": "Value or variable or expression.",
                "Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }."
            },
            "picture": "/assets/img/AC_Enter.PNG"
        },
        "AC_Click": {
            "document": {
                "Screen": "The screen that was created in SCREEN DESIGN.",
                "Item": "Item in the screen.",
                "Index":"",
            },
            "picture": "/assets/img/AC_Click.PNG"
        },
        "AC_TakeScreenshot": {
            "document": "Current screen capture.",
            "picture": "/assets/img/AC_TakeScreenshot.PNG"
        },
        "AC_CloseBrowser": {
            "document": "Close browser",
            "picture": "/assets/img/AC_CloseBrowser.PNG"
        },
        "AC_SetVariable": {
            "document": {
                "Name": "Variable name have a format starting with #  example:#username.",
                "Variable Type": "There are 2 types: Set Value, Set password. Value displays text type to use Set Value, displays password type to user Set Password.",
                "Value": "Value or  variable or expression.",
                "Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }."
            },
            "picture": "/assets/img/AC_SetVariable.PNG"
        },
        "AC_If": {
            "document": {
                "Condition": "Condition is A boolean value, boolean variable, boolean expression.Action If always goes with action End If.",
                "Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }."
            },
            "picture": "/assets/img/AC_If.PNG"
        },
        "AC_EndIf": {
            "document": " Action End if always goes with the action If.",
            "picture": "/assets/img/AC_If.PNG"
        },
        "AC_Loop": {
            "document": {
                "Times": "Times is An Integer value or integer variable  or  expression with integer result.  Action Loop always goes with action End Loop.",
                "Variable": "Output variable indicated for the loop time nth. Example #n. You can use the input variable for the loop.",
                	"Variable": "startwith #, example #username.",
                    "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_Loop.PNG"
        },
        "AC_EndLoop": {
            "document": "Action End Loop always goes with the action Loop.",
            "picture": "/assets/img/AC_Loop.PNG"
        },
        "AC_GetRangeValue": {
            "document": {
                "File name": "Filename is the path of the excel file. Excel file belongs to one of the following 2 formats: xlsx, xls. In case the file has been uploaded on the testuite of testuite, enter the file name in the format # testData / 'filename'. In case the file is the test result this time, please input in the format: # testResult / 'filename'.",
                "Sheet name": "Sheet name is the name of the sheet to retrieve data example: Sheet1.",
                "Range": "Range name, example: A2:B11.",
                "Result": "Result is the variable containing the returned result example: #result.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_GetRangeValue.PNG"
        },
        "AC_GetCellValue": {
            "document": {
                "File name": "Filename is the path of the excel file. Excel file belongs to one of the following 2 formats: xlsx, xls. In case the file has been uploaded on the testuite of testuite, enter the file name in the format # testData /'filename'. In case the file is the test result this time, please input in the format: # testResult / 'filename'.",
                "Sheet name": "Sheet name is the name of the sheet to retrieve data example: Sheet1.",
                "Row": "The row is the row number of the sheet.",
                "Column": "The column is the column number of the sheet.",
                "Value": "Value is the variable containing the returned result example: #result.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_GetCellValue.PNG"
        },
        "AC_CheckCellValue": {
            "document": {
                "File name": "Filename is the path of the excel file. Excel file belongs to one of the following 2 formats: xlsx, xls. In case the file has been uploaded on the testuite of testuite, enter the file name in the format # testData / 'filename'. In case the file is the test result this time, please input in the format: # testResult / 'filename'.",
                "Sheet name": "Sheet name is the name of the sheet to retrieve data example: Sheet1.",
                "Row": "The row is the row number of the sheet.",
                "Column": "The column is the column number of the sheet.",
                "Value": "The Value is value compared to the result obtained from Row and Column.Can be value or variable or expression.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_CheckCellValue.PNG"
        },
        "AC_SetCellValue": {
            "document": {
                "File name": "Filename is the path of the excel file. Excel file belongs to one of the following 2 formats: xlsx, xls. In case the file has been uploaded on the testuite of testuite, enter the file name in the format # testData / 'filename'. In case the file is the test result this time, please input in the format: # testResult / 'filename'.",
                "Sheet name": "Sheet name is the name of the sheet to retrieve data, example: Sheet1.",
                "Row": "The row is the row number of the sheet.",
                "Column": "The column is the column number of the sheet.",
                "Value": "The Value is value compared to the result from Column and Row. Can be value or variable or expression.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_SetCellValue.PNG"
        },
        "AC_SetRangeValue": {
            "document": {
            	"Expression": "start with ${ and end with}.",
                "File name": "Filename is the path of the excel file. Excel file belongs to one of the following 2 formats: xlsx, xls. In case the file has been uploaded on the testuite of testuite, enter the file name in the format # testData / 'filename'. In case the file is the test result this time, please input in the format: # testResult / 'filename'.",
                "Sheet name": "Sheet name is the name of the sheet to retrieve data.",
                "Row": "The row is the row number of the sheet.",
                "Column": "The column is the column number of the sheet.",
                "Value": "The Value is value compared to the result from Column and Row. Can be value or variable or expression.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_SetRangeValue.PNG"
        },
        "AC_Sleep": {
            "document": {
                "Miliseconds": "The next step will be executed after wait milinseconds."
            },
            "picture": "/assets/img/AC_Sleep.PNG"
        },
        "AC_Select": {
            "document": {	
                "Screen": "The screen that was created in SCREEN DESIGN.",
                "Item": "Item in the screen.",
                "Value": "Value is value or variable or expression in the list.",
                "Index":"",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_Select.PNG"
        },
        "AC_SetCheckboxValue": {
            "document": {
                "Screen": "The screen that was created in SCREEN DESIGN.",
                "Item": "Item in the screen.",
                "Value": "Value is  True or On if check box, False or Off otherwise.",
                "Index":""
            },
            "picture": "/assets/img/AC_SetCheckboxValue.PNG"
        },
        "AC_GetCheckboxValue": {
            "document": {
                "Screen": "The screen that was created in SCREEN DESIGN.",
                "Item": "Item in the screen.",
                "Variable": "Valiable containing the returned result.",
                "Index":"",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_GetCheckboxValue.PNG"
        },
        "AC_CheckValue": {
            "document": {
                "Variable": "Variable is value or variable or expression.",
                "Operator": "The operator want to compare.",
                "Expected": "Expected is value or variable or expression Return True if value = expected, otherwise return false.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_CheckValue.PNG"
        },
        "AC_SetJavascriptVariable": {
            "document": {
                "Variable": "javascript variable example: #value.",
                "Value ": "Value or variable or expression for javascript variable.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_SetJavascriptVariable.PNG"
        },
        "AC_ExecuteJavascript": {
            "document": {
                "Script": "javascript statement. "
            },
            "picture": "/assets/img/AC_ExecuteJavascript.PNG"
        },
        "AC_GetJavascriptVariable": {
            "document": {
            	"Expression": "start with ${ and end with}.",
                "Variable": "Variable is the variable that already contains data.",
                "Result": "Result is Valiable containing the returned result, Example: #data.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_GetJavascriptVariable.PNG"
        },
        "AC_ExecuteTestcase": {
            "document": {
                "Testcase": "The testcase has been created."
            },
            "picture": "/assets/img/AC_ExecuteTestcase.PNG"
        },
        "AC_TakeFullScreen": {
            "document": " take full screen.",
            "picture": "/assets/img/AC_TakeFullScreen.PNG"
        },
        "AC_GetCsvFile": {
            "document": {
                "File name": "Filename is the path of the excel file. Excel file belongs to one of the following 2 formats: xlsx, xls . In case the file has been uploaded on the testuite of testuite, enter the file name in the format # testData / 'filename' . In case the file is the test result this time, please input in the format: # testResult / 'filename'.",
                "Result": "Result is Valiable containing the returned result , Example: #listdata.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_GetCsvFile.PNG"
        },
        "AC_WriteToCSV": {
            "document": {
                "Data": "Data is value or variable or expression  to be transferred into the file.",
                "File name": "Filename is the path of the excel file. Excel file belongs to one of the following 2 formats: xlsx, xls. In case the file has been uploaded on the testuite of testuite, enter the file name in the format # testData / 'filename'. In case the file is the test result this time, please input in the format: # testResult / 'filename'.",
                "Append": "Append isTrue if you want to write data to the file, False otherwise.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_WriteToCSV.PNG"
        },
        "AC_SetControlValue": {
            "document": {
                "Screen": "The screen that was created in SCREEN DESIGN.",
                "Item": "Item in the screen.",
                "Value": "Value or variable or expression.",
                "Index":"",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_SetControlValue.PNG"
        },
        "AC_GetControlValue": {
            "document": {
                "Screen": "The screen that was created in SCREEN DESIGN.",
                "Item": "Item in the screen.",
                " Value": "Value is Valiable containing the returned result, Example: #data.",
                "Index":"",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_GetControlValue.PNG"
        },
        "AC_CheckControlValue": {
            "document": {
                "Screen": "The screen that was created in SCREEN DESIGN.",
                "Item": "Item in the screen.",
                "Operator ": "The operator want to compare.",
                "Expected ": "Expected is value or variable or expression. Return True if value = expected, otherwise return false.",
                "Index":"",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_CheckControlValue.PNG"
        },
        "AC_CompareImage": {
            "document": {
                "Recorded image": "compare two images of the same size, with the image path being filled in both Recordimage and Expected image."
            },
            "picture": "/assets/img/AC_CompareImage.PNG"
        },
        "AC_TakeScreenshotWithResult": {
            "document": {
                "Image name": "Image name is the path containing the image file and image name, example: C:/Paracel/img/login.PNG. In case want to upload image file to testdata of testsuite please input with format #testData/<name image> , example: #testData/login.PNG."
            },
            "picture": "/assets/img/AC_TakeScreenshotWithResult.PNG"
        },
        "AC_ConnectDatabase": {
            "document": {
                "Connection name": "Database named, example: #connectData.",
                "Type sql": "sql type , example: mysql.",
                "Server string": "the server corresponds to the database.",
                "Database name": "Database name to connect.",
                "Username": "the Username corresponding to the database.",
                "Password": "The Password corresponding to the database."
            },
            "picture": "/assets/img/AC_ConnectDatabase.PNG"
        },
        "AC_ExecuteQuery": {
            "document": {
                "Connection name": "Database named, example: #connectData.",
                "Query string": "Query statement , example: Select * From customer.",
                "Result": "Result is Valiable containing the returned result.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_ExecuteQuery.PNG"
        },
        "AC_CheckControlAttribute": {
            "document": {
                "Screen": "The screen that was created in SCREEN DESIGN.",
                "Item": "Item in the screen.",
                "Attribute": "Attribute is the attribute placed in the opening html tag, corresponding to the item, example: name.",
                "Value ": "Value or an variable or expression to compare the attribute value.",
                "Index":"",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_CheckControlAttribute.PNG"
        },
        "AC_CheckValueFormat": {
            "document": {
                "Format": "the format of comparison value, example: dd/mm/yyyy.",
                "Value or variable": "Value or an variable or expression to compare with the above format.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_CheckValueFormat.PNG"
        },
        "AC_GetTableData": {
            "document": {
                "Screen": "The screen that was created in SCREEN DESIGN.",
                "Item": "Item in the screen.",
                "Value": "Value is Valiable containing the returned result, Example: #data.",
                "Index":"",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_GetTableData.PNG"
        },
        "AC_ConnectFTP": {
            "document": {
                "Connect name": "Ftp named , example: ftpconnect.",
                "Host": "Host address",
                "Port": "Port",
                "Username": "Username",
                " Password": "Password"
            },
            "picture": "/assets/img/AC_ConnectFTP.PNG"
        },
        "AC_DowloadFTPFile": {
            "document": {
                "Connect name": "Ftp named.",
                "Remote Path": "The path of the folder on the ftp server.",
                "Local path": "The path of the folder on the ftp download.",
                "File name": "File name to download."
            },
            "picture": "/assets/img/AC_DowloadFTPFile.PNG"
        },
        "AC_DowloadFTPFiles": {
            "document": {
                "Connect name": "Ftp named.",
                "Remote path": "The path of the folder on the ftp server.",
                "Local path": "The path of the folder on the ftp download."
            },
            "picture": "/assets/img/AC_DowloadFTPFiles.PNG"
        },
        "AC_UploadFTPFile": {
            "document": {
                "Connect name": "Ftp named.",
                "Remote path": "The path of the folder on the ftp server.",
                "Local path": "The path of the folder on the ftp download."
            },
            "picture": "/assets/img/AC_UploadFTPFile.PNG"
        },
        "AC_GetFtpFolders": {
            "document": {
                "Connect name": "Ftp named.",
                "Remote path": "The path of the folder on the ftp server.",
                "Result": "Result is Valiable containing the returned result , Example: #listdata.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_GetFtpFolders.PNG"
        },
        "AC_CreateFolderFTP": {
            "document": {
                "Connect name": "Ftp named.",
                "Dirname": "Name the new forder."
            },
            "picture": "/assets/img/AC_CreateFolderFTP.PNG"
        },
        "AC_DeleteInFTP": {
            "document": {
                "Connect name": "Ftp named.",
                "Dirname": "The path to the folder to be deleted."
            },
            "picture": "/assets/img/AC_DeleteInFTP.PNG"
        },
        "AC_CheckFileExistFTP": {
            "document": {
                "Connect name": "Ftp named.",
                "Remote path": "The path of the folder on the ftp server.",
                "Type file": "file type , example: xlsx.",
                "Result": "Result is Valiable containing the returned result, Example: #listdata.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_CheckFileExistFTP.PNG"
        },
        "AC_CaptureControl": {
            "document": {
                "Screen": "The screen that was created in SCREEN DESIGN.",
                "Item": "Item in screen.",
                "File name": "Image name is the path containing the image file. In case want to upload image file to testdata of testsuite please input with format #testData. ",
                "Index":""
            },
            "picture": "/assets/img/AC_CaptureControl.PNG"
        },
        "AC_GetAttribute": {
            "document": {
                "Screen": "The screen that was created in SCREEN DESIGN.",
                "Item": " Item in the screen.",
                "Attribute": "Attribute is the attribute placed in the opening html tag, corresponding to the item.",
                "Value": "Value is Valiable containing the returned result, Example: #data.",
                "Index":"",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_GetAttribute.PNG"
        },
        "AC_While": {
            "document": {
                "Condition": "Output variable indicated for the loop time nth. Example #n.you can use the input variable for the loop.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_While.PNG"
        },
        "AC_EndWhile": {
            "document": "Action End While always goes with the action While",
            "picture": "/assets/img/AC_While.PNG"
        },
        "AC_GetFilesInFolder": {
            "document": {
                "Path": "The path of file.",
                "Type": "The type file to retrieve.",
                "Result": "Result is Valiable containing the returned result, Example: #listdata.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_GetFilesInFolder.PNG"
        },
        "AC_Type": {
            "document": { "Value"  : "Send a special key or special keys to Browser" },
            "picture": "/assets/img/AC_Type.PNG"
        },
        "AC_GetControlBy": {
            "document": {
                "Item name": "The new item name.",
                "Detect by": "Detect by  is the attribute placed in the opening html tag, corresponding to the item.",
                " Value": " Value or an variable or expression for attribute.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_GetControlBy.PNG"
        },
        "AC_EmailFromContact": {
            "document": {
                "Connect email": "Account Email named , example: #emailconnect.",
                "Smtp server": "smtp server address.",
                "Port": "Port",
                "Email": "Email address",
                "Password": "Password"
            },
            "picture": "/assets/img/AC_EmailFromContact.PNG"
        },
        "AC_SendEmail": {
            "document": {
                "Connect email": "Account Email named , example: #emailconnec.",
                "From": "Email address of the sender.",
                "To": "Recipient Email address.",
                "Subject": "The email subject.",
                "Content": "The email content.",
                "Attatch file": "The path contains the file to be sent. "
            },
            "picture": "/assets/img/AC_SendEmail.PNG"
        },
        "AC_GetFolders": {
            "document": {
                "Path": "Path is the path containing the folders to retrieve.",
                "Result": "Result is Valiable containing the returned result, Example: #listdata.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }.",
            },
            "picture": "/assets/img/AC_GetFolders.PNG"
        },
        "AC_CopyFile": {
            "document": {
                "File path": "File Path is the path of the file to copy. In case the file has been uploaded on the testuite of testuite, enter the file name in the format # testData / 'filename' . In case the file is the test result this time, please input in the format: # testResult / 'filename'.",
                "New file": "New file address."
            },
            "picture": "/assets/img/AC_CopyFile.PNG"
        },
        "AC_GetAllFileInFolder": {
            "document": {
                "Path": "Path is the path of the folder containing the files to retrieve. In case the folder has been uploaded on the testuite of testuite, enter the folder name in the format # testData. In case the folder is the test result this time, please input in the format: # testResult.",
                "Result": "Result is Valiable containing the returned result, Example: #listdata.",
            	"Variable": "startwith #, example #username.",
                "Expression": "start with ${ and end with }."
            },
            "picture": "/assets/img/AC_GetAllFileInFolder.PNG"
        },
        "AC_DeleteFile": {
            "document": {
                "File path": "File Path is the path of the file to delete. In case the file has been uploaded on the testuite of testuite, enter the file name in the format # testData / 'filename' . In case the file is the test result this time, please input in the format: # testResult / 'filename'."
            },
            "picture": "/assets/img/AC_DeleteFile.PNG"
        },
        "AC_MoveFolder": {
            "document": {
                "Folder path": "Folder path is the path of the  folder to be deleted.",
                "New directory": "New folder address."
            },
            "picture": "/assets/img/AC_MoveFolder.PNG"
        },
        "AC_UseTestData": {
            "document": {
                "Screen": "The screen that was created in SCREEN DESIGN.",
                "Casenum ": "The testcase number has been created. "
            },
            "picture": "/assets/img/AC_UseTestData.PNG"
        },
        "AC_CheckData": {
            "document": {
                "Screen": "The screen that was created in SCREEN DESIGN.",
                "Casenum": "The testcase number has been created. "
            },
            "picture": "/assets/img/AC_CheckData.PNG"
        },
        "AC_API": {
            "document": " Method  :  ",
            "picture": "/assets/img/AC_API.PNG"
        },
        "AC_RightClick":{
        	"document": {
        		"Screen":"The screen that was created in SCREEN DESIGN",
        		"Item":" Item in the screen.",
        		"Index":""
        	},
        	"picture": "/assets/img/AC_RightClick.PNG"
        },
        "AC_FindElementsBy":{
        	"document": {
        		"Return name":"Return name is Valiable containing the search resultS.",
        		"From parent":"Form parent is the element parent containing the element to search ",
        		"Index":"",
        		"Css Selector":"<a href='https://www.w3schools.com/cssref/css_selectors.asp' target='_blank'>https://www.w3schools.com/cssref/css_selectors.asp</a>"
        	},
        	"picture": "/assets/img/AC_FindElementsBy.PNG"
        },
        "AC_GetColumn":{
        	"document": {
        		"Return name":"",
        		"From parent":"",
        		"Index":"",
        		"Css Selector":"<a href='https://www.w3schools.com/cssref/css_selectors.asp' target='_blank'>https://www.w3schools.com/cssref/css_selectors.asp</a>"
        	},
        	"picture": "/assets/img/AC_FindElementsBy.PNG"
        }
}

