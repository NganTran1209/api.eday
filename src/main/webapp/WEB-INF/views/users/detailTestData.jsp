<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<jsp:include page="/WEB-INF/views/users/projectHeader.jsp"></jsp:include>
<div class="card card-box card-topline-green changeAddIssue" id="boxTestDataDetail">
	<div class="card-head card-head-icon">
		<header>
			<span class="my-auto d-flex"><i class="my-auto ti-receipt"></i> <spring:message code="label.TestData"></spring:message>: <span id="loadNameLayout" class="ml-1">${layoutName}</span></span>
		</header>
		<div class="tools">
		<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;" onclick="collsapseCardBody(this)"></a>
		</div> 
    </div>
	<div class="card-body">
		<div class="w-100 form-group">
			<ul class="list-unstyled docListWindow listCaseAddNew h-100 d-block">
				
			</ul> 
		</div>
		<script type="text/javascript">
		var layoutName = "${layoutName}";
		var convertedObject = ${convertedObject};
		var testData = ${testData};
		var projectId= ${projectId};
		var idTestData = ${idData};
		var testsuiteName = "${testsuiteName}";
		</script>
		<div class="col-md-8 col-center-block text-center mt-5 mx-auto">
			<button class="btnPlay btn bottest-button btn-success mr-2" onclick="creatCase(convertedObject.Controls);">
				<i class="ti-plus text-white mr-2"></i> <spring:message code="label.CreateCase"></spring:message>
			</button>
			<button class="btnPlay btn bottest-button btn-success mr-2" onclick="saveCase();" id="btnSaveCaseNum">
				<i class="ti-check text-white mr-2"></i> <spring:message code="label.Save"></spring:message>
			</button>
			<button class="btnPlay btn bottest-button btn-secondary mr-2" onclick="window.location.href='${context}/project/${projectId}/testData'">
			
				<i class="ti-angle-left text-white mr-2"></i> <spring:message code="label.Back"></spring:message>
			</button>
		</div>
	</div>
</div>
<jsp:include page="/WEB-INF/views/users/modal_add_new_issue.jsp"></jsp:include>

<div class="modal fade" id="helpEnterInput" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success">
        <h5 class="modal-title mt-0" id="exampleModalLabel">Help</h5> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn bottest-button btn-secondary button-sm"><i class="ti-close mr-2"></i>Close</button> 
      </div>
    </div>
  </div>
</div>
<script>
	var classTab = $(".text-uppercase.border-success");
	$.each(classTab, function(index, values){
	    if($(values).hasClass("active show") == true){
	        $(values).removeClass("active show");
	    }
	});
	$("#callTestDataTab").addClass("active show");
	var listBug=${listBug};
 	var taskList = ${taskList};
</script>

<script src="${context}/assets/js/management/testDataDetail.js"></script>
<script src="${context}/assets/js/management/tasks.js" ></script>