<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="modalInviteMember" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0">Invited Member</h4>
				<button type="button" class="close" data-dismiss="modal" onclick="resetFormProject()">×</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal form-label-left form-addNew-common" id="invitedMemberModal" action="javascript:void(0)" accept-charset="UTF-8" method="post">
				    <div class="item form-group bad ">
				        <label for="emailTo">
				        	<spring:message code="label.EmailAddress"></spring:message><span class="required">*</span>
				        </label>
				        <div class="input-icon right"><i class="fa projectName"></i>
				        <spring:message code="label.Pleaseenter" var="pleaseEnter"></spring:message>
			            <input class="form-control emailTo" placeholder="${pleaseEnter}" required="required" type="email" name="emailTo" id="emailTo" >
			        	</div>
				    </div>
				    <div id="apend_email"></div>
				    <div class="item form-group">
				       <div class="w-100">
				            <button type="button" class="w-100 btn bottest-button bg-transparent button-sm text-left border" id="addNewMaiTo" onclick="addNewMailTo();">
				            	<i class="icon-plus mr-2"></i>Add email address to invite
			            	</button>
				        </div>
				    </div>
				    <div class="item form-group">
				       <label for="emailTo">
				        	Role Project<span class="required">*</span>
				        </label>
				        <div class="input-icon right">
			            	<select name="roleProject" id="roleProject" class="form-control">
			            		<!-- <option value="">Select role</option>
			            		<option value="Manager">Manager</option>
			            		<option value="Developer">Developer</option>
			            		<option value="Tester">Tester</option>
			            		<option value="Customer">Customer</option> -->
			            	</select>
			        	</div>
				    </div>
				    <div class="item form-group">
				         <label for="messageTo">
				        	Message (optional)
				        </label>
				       <textarea class="form-control" rows="3" placeholder="Please enter" name="messageTo" id="messageTo"></textarea>
				    </div>
				    <div class="form-group justify-content-center">
				        <button type="submit" class="btn bottest-button btn-success mr-2" onclick="validateInviteMember();" id="buttonInvited">
				        	<i class="icon-user-follow mr-1"></i> Invited
			        	</button>
				        <button type="button" class="btn bottest-button btn-secondary" onclick="resetFormProject();" data-dismiss="modal" >
				        	<i class="ti-close">&nbsp;</i>   Close
				        </button>
				    </div>
				</form>
			</div>
		</div>
	</div>
</div>
<script src="${context}/assets/js/management/role.js"></script>