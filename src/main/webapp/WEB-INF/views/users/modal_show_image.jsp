<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="modalShowImage" class="modal" tabindex="-1" role="dialog" aria-labelledby="modalShowImage" aria-hidden="true">
	<div id="bodyShowImg" hidden>
		<div id="modalImge" class="slideshow-container w-100 h-100">
		</div>
		
	</div>
	<div id="showVideo" hidden style="height: 100vh; display: flex;">
		
		
	</div>
</div>