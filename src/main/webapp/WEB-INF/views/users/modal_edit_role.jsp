<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="modalEditRole" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog" style="max-width:800px">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0" id="title-role"></h4>
				<button type="button" class="close" data-dismiss="modal" onclick="resetFormEditRoleUser()">×</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal form-label-left form-addNew-common" id="EditMemberModal" action="javascript:void(0)" accept-charset="UTF-8" method="post">
				    <table class="table table-hover table-checkable order-column full-width table-common" id="taskList">
                     <thead>
                         <tr id="thead-tbRole">
                         	<%-- <th scope="col"  class="text-center" style="width: 20%"><div class="mb-1" style="color: white; text-align: center"> # </div> </th> 
							<th scope="col" ><div class="mb-1" style="color: white"><input type="checkbox"  id="checkRoleAll_view" onclick='selectAllAction("view")'><spring:message code="label.View"></spring:message> </div></th>
							<th scope="col">   
								<div class="mb-1" style="color: white"><input type="checkbox"  id="checkRoleAll_edit" onclick='selectAllAction("edit")'><spring:message code="label.Edit"></spring:message></div>
							</th>
							<th scope="col"> 
								<div class="mb-1" style="color: white"><input type="checkbox"  id="checkRoleAll_deleted" onclick='selectAllAction("deleted")'><spring:message code="label.Delete"></spring:message> </div>
							</th>
							<th scope="col" ><div class="mb-1" style="color: white"><input type="checkbox"  id="checkRoleAll_excute" onclick='selectAllAction("excute")'><spring:message code="label.Execute"></spring:message></div></th> --%>
							
                         </tr>
                     </thead>
                     <tbody id="tableRole">
                     	
					</tbody>
					
               </table>
				<div class="modal-footer" id="modalFooterRole">
					
					
				</div>
			
		</form>
		</div>
		</div>
	</div>
</div>
<script>
$( document ).ready(function() {
	hideModalEditRole();
});
</script>