<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:include page="/WEB-INF/views/users/projectHeader.jsp"></jsp:include>

<div class="card card-box card-topline-green changeAddIssue" id="drawTestData">
	<div class="card-body">
		<div class="col-md-12 form-group">
			<div class="row">
				<div class="col-md-4">
					<div class="row">
						<button class="btnPlay btn bottest-button btn-success mr-2 mb-3" onclick="showModalCreateTD();">
							<i class="ti-plus text-white"> </i> <spring:message code="label.CreateTestData" ></spring:message>
						</button>
						<a class="btnPlay btn bottest-button btn-success mb-3" onclick="showModalUploadTD();">
							<i class="icon-cloud-upload text-white">&nbsp;</i><spring:message code="label.UploadTestData" ></spring:message>
						</a>
					</div>
				</div>
				<div class="col-md-7 ml-auto" >
					<div class="row" id="filterTestdata_content">
						<div class="">
							<spring:message code="label.Enterfilename" var="enterfilename" ></spring:message>
							<input class="form-control colorPlace" placeholder="${enterfilename}"  id="filterFileName" value=""/>
						</div>
						<div class="">
							<select class="form-control testsuiteFilter" multiple="multiple" id="filterTestsuite">
							</select>
						</div>
						<div class="">
							<input class="form-control createDate colorPlace" id="filterCreateDate" placeholder="Create Date" />
						</div>
						<div class="">
							<select class="form-control" id="filterTypeFile" multiple="multiple">
								<option value="file"><spring:message code="label.File" ></spring:message></option>
								<option value="layout"><spring:message code="label.ScreenDesign" ></spring:message></option>
							</select>
						</div>
						<div>
							<button id="filTestData" class="btnPlay btn bottest-button btn-success" onclick="filterTestdata();">
								<i class="ti-search text-white"> </i> <spring:message code="label.Search" ></spring:message>
							</button>
						</div>
					</div>
					
					
				</div>
			</div>  
		</div>
		<div class="position-relative w-100" id="" style="min-height:115px;">
			<div class="preloader loading-important position-absolute" id="loaderTestData" style="display: none;">
			   <div class="loader" style="top:30%">
			        <div class="loader__figure"></div>
			        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting" ></spring:message><br> Bottest.io</p>
			    </div>
			</div>
			<div class="w-100 table-responsive">
				<table class="table table-hover table-common">
					<thead>
						<tr class="tr-bottest-hd bg-success">
							<th class="border-bottom-0"><spring:message code="label.FileName" ></spring:message></th> 
							<th class="border-bottom-0"><spring:message code="label.CreateDate" ></spring:message></th>
							<th class="border-bottom-0"><spring:message code="label.Description" ></spring:message></th>
							<th class="border-bottom-0"><spring:message code="label.DesignData" ></spring:message></th>
							<th class="border-bottom-0"><spring:message code="label.Action" ></spring:message></th>
						</tr>
					</thead>
					<tbody id="bodyTestDataRecord">
						<div style="display:none;">
					        <div class="col-md-6 col-sm-6 col-xs-12">
					        	<select id="listLayoutss" class="form-control"></select>
						    </div>
						</div>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<jsp:include page="/WEB-INF/views/users/modal_add_new_issue.jsp"></jsp:include>
	
<script src="${context}/assets/js/management/testData.js"></script>
<jsp:include page="/WEB-INF/views/users/modal_upload_testData.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/users/modal_copy_testdata.jsp"></jsp:include>
<script src="${context}/assets/js/management/tasks.js" ></script>
<script>
	var classTab = $(".text-uppercase.border-success");
	$.each(classTab, function(index, values){
	    if($(values).hasClass("active show") == true){
	        $(values).removeClass("active show");
	    }
	});
	$("#callTestDataTab").addClass("active show");
	var projectId = ${projectId};
	var listRole = ${listPermisson};
 	var listPermisson =JSON.parse(listRole[0].role_detail);
	var lsTestData = ${lsUploadTestdata};
	
	var testsuiteNameSS = '${tsNameSession}';
	var createDateSS = "${createDateSession}";
	var typeSS = '${typeSession}';
	var fileNameSS = "${fileNameSession}";
	var checkSession = true;
	var listBug=${listBug};
 	var taskList = ${taskList};
 	var checkSessionExist = ${checkSessionExist};
	
	var testsuiteNameParse = "";
	if(testsuiteNameSS != ""){
		testsuiteNameParse = JSON.parse(testsuiteNameSS);
	}
	
	var typeParse = "";
	if(typeSS != ""){
		typeParse = JSON.parse(typeSS);
	}
	
	getListTestsuiteData(function(testsuiteLs){
		//
		tsByProjectId = [];
		$.each(testsuiteLs, function(index, values){
			var obTs = {};
			obTs["id"] = values.testsuite;
			obTs["text"] = values.testsuite;
			tsByProjectId.push(obTs);
		});
			
		
		$("#listTestsuite").select2({
			dropdownParent: $("#chooseScreenTestData"),
			placeholder: getLang(currentLocale,"Select a testsuite"),
			width: '100%',
			theme: "bootstrap"
		});
		
		$("#listTestsuiteUp").select2({
			dropdownParent: "",
			placeholder: getLang(currentLocale,"Select a testsuite"),
			width: '100%',
			theme: "bootstrap"
		});
		
		$("#listTestsuite").empty();
		$("#testsuiteListss").empty();
		$("#listTestsuiteUp").empty();
		$.each( testsuiteLs, function(indexRecordLayout, valuesRecordLayout) { 
			var htmlOptionChooseScreen = '<option value="'+valuesRecordLayout.testsuite+'">'+valuesRecordLayout.testsuite+'</option>';
			$("#listTestsuite").append(htmlOptionChooseScreen);
			$("#testsuiteListss").append(htmlOptionChooseScreen);
			$("#listTestsuiteUp").append(htmlOptionChooseScreen);
		});
		var testsuiteName = $("#listTestsuite").val();
		loadListScreenLayout(testsuiteName, "#listScreenLayout", true);
		
		//filter
		$("#filterTestsuite").select2({
			placeholder: getLang(currentLocale,"Select Testsuite"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			data: tsByProjectId,
			tags: true,
			dropdownParent: "",
			theme: "bootstrap"
		});
		$("#filterTestsuite").val(testsuiteNameParse);
		$("#filterTestsuite").trigger('change');
		
		$("#filterTypeFile").select2({
			placeholder: getLang(currentLocale,"Select Type"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			tags: true,
			dropdownParent: "",
			theme: "bootstrap"
		});
		
		$("#filterTypeFile").val(typeParse);
		$("#filterTypeFile").trigger('change');
		
		
	});
	
	$('#filterCreateDate').bootstrapMaterialDatePicker
	({
		format: 'YYYY-MM-DD',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});
	$('#filterCreateDate').val(createDateSS);
	
	$("#filterFileName").val(fileNameSS);
	
	if((testsuiteNameSS != "" && testsuiteNameSS != "[]") || createDateSS != "" ||( typeSS != "" && typeSS != "[]") || fileNameSS){
		
	} else if(!checkSessionExist){
		testsuiteNameParse = [];
		createDateSS = "";
		typeParse = [];
		fileNameSS = "";
	}
	drawTestcaseByFilter(testsuiteNameParse, createDateSS, typeParse, fileNameSS);
	
</script>

<jsp:include page="/WEB-INF/views/users/modal_choose_screen_new_testData.jsp"></jsp:include>
