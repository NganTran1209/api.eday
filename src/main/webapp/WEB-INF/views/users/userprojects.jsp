<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="preloader loading-important">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting"></spring:message> <br> Bottest.io</p>
    </div>
</div> 
<div class="clearfix"></div>
<div class="borderBox shadow-none border-0 light card-box p-0 mb-3 tabsBorderCustom " style="overflow:revert;height: 50px;margin-bottom: 10px;">
	<div class="borderBox-title tabbable-line mb-0" style="min-height: 50px">
	     <ul class="nav nav-tabs-border float-left">
	     	 <li class="nav-item" id="calSummaryTabs">
	             <a href="#projectList" data-toggle="tab" class="text-uppercase border-success active show" id="callProjectList"><spring:message code="label.ProjectList"></spring:message> </a>
	         </li>
	         <li class="nav-item">
	             <a href="#invitedToJoinProject" data-toggle="tab" class="text-uppercase border-success" id="callInvitedToJoinProject" onclick="invitedToJoinProject()"><spring:message code="label.InvitedToJoinProject"></spring:message> </a>
	         </li>
	     </ul>
	</div> 
</div>
<div class="tab-content">
	<div class="tab-pane active show" id="projectList">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-box card-topline-green" id="listProject"> 
					<div class="card-head card-head-icon">
			           	<header><i class="ti-layout-grid2"></i> <spring:message code="label.ProjectList"></spring:message>  <small class="countProject ml-2 text-dark font-weight-bold"></small></header>
			           	<div class="tools">
							<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;" onclick="collsapseCardBody(this)"></a>
			           	</div> 
		        	 </div>
		          	<div class="card-body " style=""> 
			          	<div class="form-group d-flex">
			         		<button type="button" class="btn bottest-button btn-success" id="showModalNewProject">
			         			 <i class="fa fa-plus"> &nbsp;</i>  <spring:message code="label.Createnewproject">  </spring:message>
			       			 </button>
			         	</div> 
			          	<div id="projects-index">
						    <ul class="projects root list-unstyled">
						    
							</ul>
							
						</div>
		          </div>
		      </div>
		   </div>
	   </div>
	</div>
	<div class="tab-pane" id="invitedToJoinProject">
	<jsp:include page="/WEB-INF/views/users/invited-to-join-project.jsp"></jsp:include>
	</div>
</div>
<script>
	$(function(){
		  var hash = window.location.hash;
		  hash && $('ul.nav a[href="' + hash + '"]').tab('show');
		  $('.nav-tabs a').click(function (e) {
		    $(this).tab('show');
		    var scrollmem = $('body').scrollTop();
		    window.location.hash = this.hash;
		    $('html,body').scrollTop(scrollmem);
		  });
	});
	loadGraphic();
	getProjects("true", function(project){
	});
	
</script>
<jsp:include page="/WEB-INF/views/users/modal_add_new_project.jsp"></jsp:include>
<script src="${context}/assets/js/management/userprojects.js"></script>