
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="modalAddNewProject" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0"><spring:message code="label.AddNewProject" ></spring:message></h4>
				<button type="button" class="close" data-dismiss="modal" onclick="resetFormInvite();" >×</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal form-label-left form-addNew-common" id="new_project" action="javascript:void(0)" accept-charset="UTF-8" method="post">
				    <input type="hidden" name="id" id="idProject">
				    <div class="row item form-group bad newProjectName">
				        <label class="control-label col-md-3 col-sm-12 col-xs-3 text-left">
				        	<spring:message code="label.ProjectName" ></spring:message><span class="required">*</span>
				        </label>
				        <div class="col-md-9 col-sm-12 col-xs-9 text-left">
				        	<div class="input-icon right"><i class="fa projectName"></i>
				        		<spring:message code="label.Pleaseenter"  var="Pleaseenter"></spring:message>
				            	<input class="form-control" placeholder="${Pleaseenter}" required="required" type="text" name="name" id="nameProject" title="" alt="">
				        	</div>
				        </div>
				    </div>
				    <div class="row item form-group bad">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left">
				        	<spring:message code="label.ProjectType"></spring:message>
				        </label>
				         <div class="col-md-9 col-sm-12 col-xs-9 text-left">
				            <select class="form-control" name="type" id="typeProject">				            
				             <option ><spring:message code="label.WebApplication"></spring:message></option>
				            	<option><spring:message code="label.MobileApplication"></spring:message></option>
				            	<option><spring:message code="label.WindowApplication"></spring:message></option>
				            </select> 
				        </div> 
				    </div>
				    <div class="row item form-group">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left">
				        	<spring:message code="label.Description" ></spring:message>
				        </label>
				       <div class="col-md-9 col-sm-12 col-xs-9 text-left input-icon right"><i class="fa mr-3 descriptionProject"></i>
				      		<spring:message code="label.Pleaseenter"  var="Pleaseenter"></spring:message>
				            <textarea class="form-control" rows="3" placeholder="${Pleaseenter}" name="description" id="descriptionProject"></textarea>
				        </div>
				    </div>
				    <div class="row item form-group itemStatusProject " hidden>
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left">
				        	<spring:message code="label.Status" ></spring:message>
				        </label>
				        <div class="col-md-9 col-sm-12 col-xs-9 text-left">
				            <select class="form-control" name="status" id="statusProject">
				            	<option value="Open">Open</option>
				            	<option value="Close">Close</option>
				            	<option value="Pending">Pending</option>
				            </select>
				        </div>
				    </div>
				    <div class="row item form-group">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left pt-2">
				        	<spring:message code="label.DailyReportTime" ></spring:message>
				        </label>
				       <div class="col-md-2 col-sm-3 col-xs-9 text-left">
				      		<select class="form-control" name="reportTime" id="reportTime">
								<option value="00:00">00:00</option>
								<option value="01:00">01:00</option>
								<option value="02:00">02:00</option>
								<option value="03:00">03:00</option>
								<option value="04:00">04:00</option>
								<option value="05:00">05:00</option>
								<option value="06:00">06:00</option>
								<option value="07:00">07:00</option>
								<option value="08:00">08:00</option>
								<option value="09:00">09:00</option>
								<option value="10:00">10:00</option>
								<option value="11:00">11:00</option>
								<option value="12:00">12:00</option>
								<option value="13:00">13:00</option>
								<option value="14:00">14:00</option>
								<option value="15:00">15:00</option>
								<option value="16:00">16:00</option>
								<option value="17:00">17:00</option>
								<option value="18:00">18:00</option>
								<option value="19:00">19:00</option>
								<option value="20:00">20:00</option>
								<option value="21:00">21:00</option>
								<option value="22:00">22:00</option>
								<option value="23:00">23:00</option>
							</select>
				        </div>
				        <label class="col-md-2 col-sm-12 col-xs-9 text-right pt-2">
				        	<spring:message code="label.TimeZone" ></spring:message>
				        </label>
				        <div class="col-md-5 col-sm-12 col-xs-12 text-left">
				       		<select class="form-control" name="reportTimeZone" id="reportTimeZone">
								<option value="(GMT-12:00) International Date Line West">(GMT-12:00) International Date Line West</option>
								<option value="(GMT-11:00) Midway Island, Samoa">(GMT-11:00) Midway Island, Samoa</option>
								<option value="(GMT-10:00) Hawaii">(GMT-10:00) Hawaii</option>
								<option value="(GMT-09:00) Alaska">(GMT-09:00) Alaska</option>
								<option value="(GMT-08:00) Pacific Time (US & Canada)">(GMT-08:00) Pacific Time (US & Canada)</option>
								<option value="(GMT-08:00) Tijuana, Baja California">(GMT-08:00) Tijuana, Baja California</option>
								<option value="(GMT-07:00) Arizona">(GMT-07:00) Arizona</option>
								<option value="(GMT-07:00) Chihuahua, La Paz, Mazatlan">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>
								<option value="(GMT-07:00) Mountain Time (US & Canada)">(GMT-07:00) Mountain Time (US & Canada)</option>
								<option value="(GMT-06:00) Central America">(GMT-06:00) Central America</option>
								<option value="(GMT-06:00) Central Time (US & Canada)">(GMT-06:00) Central Time (US & Canada)</option>
								<option value="(GMT-06:00) Guadalajara, Mexico City, Monterrey">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
								<option value="(GMT-06:00) Saskatchewan">(GMT-06:00) Saskatchewan</option>
								<option value="(GMT-05:00) Bogota, Lima, Quito, Rio Branco">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
								<option value="(GMT-05:00) Eastern Time (US & Canada)">(GMT-05:00) Eastern Time (US & Canada)</option>
								<option value="(GMT-05:00) Indiana (East)">(GMT-05:00) Indiana (East)</option>
								<option value="(GMT-04:00) Atlantic Time (Canada)">(GMT-04:00) Atlantic Time (Canada)</option>
								<option value="(GMT-04:00) Caracas, La Paz">(GMT-04:00) Caracas, La Paz</option>
								<option value="(GMT-04:00) Manaus">(GMT-04:00) Manaus</option>
								<option value="(GMT-04:00) Santiago">(GMT-04:00) Santiago</option>
								<option value="(GMT-03:30) Newfoundland">(GMT-03:30) Newfoundland</option>
								<option value="(GMT-03:00) Brasilia">(GMT-03:00) Brasilia</option>
								<option value="(GMT-03:00) Buenos Aires, Georgetown">(GMT-03:00) Buenos Aires, Georgetown</option>
								<option value="(GMT-03:00) Greenland">(GMT-03:00) Greenland</option>
								<option value="(GMT-03:00) Montevideo">(GMT-03:00) Montevideo</option>
								<option value="(GMT-02:00) Mid-Atlantic">(GMT-02:00) Mid-Atlantic</option>
								<option value="(GMT-01:00) Cape Verde Is.">(GMT-01:00) Cape Verde Is.</option>
								<option value="(GMT-01:00) Azores">(GMT-01:00) Azores</option>
								<option value="(GMT+00:00) Casablanca, Monrovia, Reykjavik">(GMT+00:00) Casablanca, Monrovia, Reykjavik</option>
								<option value="(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London">(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London</option>
								<option value="(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
								<option value="(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
								<option value="(GMT+01:00) Brussels, Copenhagen, Madrid, Paris">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
								<option value="(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb">(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
								<option value="(GMT+01:00) West Central Africa">(GMT+01:00) West Central Africa</option>
								<option value="(GMT+02:00) Amman">(GMT+02:00) Amman</option>
								<option value="(GMT+02:00) Athens, Bucharest, Istanbul">(GMT+02:00) Athens, Bucharest, Istanbul</option>
								<option value="(GMT+02:00) Beirut">(GMT+02:00) Beirut</option>
								<option value="(GMT+02:00) Cairo">(GMT+02:00) Cairo</option>
								<option value="(GMT+02:00) Harare, Pretoria">(GMT+02:00) Harare, Pretoria</option>
								<option value="(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius">(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
								<option value="(GMT+02:00) Jerusalem">(GMT+02:00) Jerusalem</option>
								<option value="(GMT+02:00) Minsk">(GMT+02:00) Minsk</option>
								<option value="(GMT+02:00) Windhoek">(GMT+02:00) Windhoek</option>
								<option value="(GMT+03:00) Kuwait, Riyadh, Baghdad">(GMT+03:00) Kuwait, Riyadh, Baghdad</option>
								<option value="(GMT+03:00) Moscow, St. Petersburg, Volgograd">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
								<option value="(GMT+03:00) Nairobi">(GMT+03:00) Nairobi</option>
								<option value="(GMT+03:00) Tbilisi">(GMT+03:00) Tbilisi</option>
								<option value="(GMT+03:30) Tehran">(GMT+03:30) Tehran</option>
								<option value="(GMT+04:00) Abu Dhabi, Muscat">(GMT+04:00) Abu Dhabi, Muscat</option>
								<option value="(GMT+04:00) Baku">(GMT+04:00) Baku</option>
								<option value="(GMT+04:00) Yerevan">(GMT+04:00) Yerevan</option>
								<option value="(GMT+04:30) Kabul">(GMT+04:30) Kabul</option>
								<option value="(GMT+05:00) Yekaterinburg">(GMT+05:00) Yekaterinburg</option>
								<option value="(GMT+05:00) Islamabad, Karachi, Tashkent">(GMT+05:00) Islamabad, Karachi, Tashkent</option>
								<option value="(GMT+05:30) Sri Jayawardenapura">(GMT+05:30) Sri Jayawardenapura</option>
								<option value="(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
								<option value="(GMT+05:45) Kathmandu">(GMT+05:45) Kathmandu</option>
								<option value="(GMT+06:00) Almaty, Novosibirsk">(GMT+06:00) Almaty, Novosibirsk</option>
								<option value="(GMT+06:00) Astana, Dhaka">(GMT+06:00) Astana, Dhaka</option>
								<option value="(GMT+07:00) Bangkok, Hanoi, Jakarta">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
								<option value="(GMT+07:00) Krasnoyarsk">(GMT+07:00) Krasnoyarsk</option>
								<option value="(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
								<option value="(GMT+08:00) Kuala Lumpur, Singapore">(GMT+08:00) Kuala Lumpur, Singapore</option>
								<option value="(GMT+08:00) Irkutsk, Ulaan Bataar">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
								<option value="(GMT+08:00) Perth">(GMT+08:00) Perth</option>
								<option value="(GMT+08:00) Taipei">(GMT+08:00) Taipei</option>
								<option value="(GMT+09:00) Osaka, Sapporo, Tokyo">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
								<option value="(GMT+09:00) Seoul">(GMT+09:00) Seoul</option>
								<option value="(GMT+09:00) Yakutsk">(GMT+09:00) Yakutsk</option>
								<option value="(GMT+09:30) Adelaide">(GMT+09:30) Adelaide</option>
								<option value="(GMT+09:30) Darwin">(GMT+09:30) Darwin</option>
								<option value="(GMT+10:00) Brisbane">(GMT+10:00) Brisbane</option>
								<option value="(GMT+10:00) Canberra, Melbourne, Sydney">(GMT+10:00) Canberra, Melbourne, Sydney</option>
								<option value="(GMT+10:00) Hobart">(GMT+10:00) Hobart</option>
								<option value="(GMT+10:00) Guam, Port Moresby">(GMT+10:00) Guam, Port Moresby</option>
								<option value="(GMT+10:00) Vladivostok">(GMT+10:00) Vladivostok</option>
								<option value="(GMT+11:00) Magadan, Solomon Is., New Caledonia">(GMT+11:00) Magadan, Solomon Is., New Caledonia</option>
								<option value="(GMT+12:00) Auckland, Wellington">(GMT+12:00) Auckland, Wellington</option>
								<option value="(GMT+12:00) Fiji, Kamchatka, Marshall Is.">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>
								<option value="(GMT+13:00) Nuku'alofa">(GMT+13:00) Nuku'alofa</option>
							</select>
				       	</div>
				    </div>
				    <div class="row item form-group bad" id="scmTypeHide" style="display: none;">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left">
				        	<spring:message code="label.SCMType" ></spring:message>
				        </label>
				         <div class="col-md-9 col-sm-12 col-xs-9 text-left">
				            <select class="form-control" id ="scmType" name="scmType">
				            	<option value="svn" >Subversion</option>
				            	<option value="git">Git</option>
				            </select>
				        </div>
				    </div>
				    <div class="row item form-group bad" id="scmUrlHide" style="display: none;">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left">
				        	<spring:message code="label.SCMURL" ></spring:message>
				        </label>
				        <div class="col-md-9 col-sm-12 col-xs-9 text-left">
				            <spring:message code="label.Pleaseenter" var="Pleaseenter" ></spring:message>
				            <input class="form-control" placeholder="${Pleaseenter}" type="text" name="scmUrl" id="scmUrl">
				        </div>
				        
				    </div>
				    <div class="row item form-group bad" id="scmUserHide" style="display: none;">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left">
				        	Scm Login Username
				        </label>
				         <div class="col-md-9 col-sm-12 col-xs-9 text-left">
				        	<spring:message code="label.Pleaseenter" var="Pleaseenter" ></spring:message>
				            <input class="form-control" placeholder="${Pleaseenter}" type="text" name="scmUsername" id="scmUsername">
				        </div>
				    </div>
				    <div class="row item form-group bad" id="ScmLoginPasswordHide" style="display: none;">
				         <label class="col-md-3 col-sm-12 col-xs-9 text-left">
				        	<spring:message code="label.ScmLoginPasswordL" ></spring:message>
				        </label>
				         <div class="col-md-9 col-sm-12 col-xs-9 text-left">
				            <spring:message code="label.Pleaseenter" var="Pleaseenter" ></spring:message>
				            <input class="form-control" placeholder="${Pleaseenter}" type="password" name="scmPassword" id="scmPassword">
				        </div>
				    </div>

				    <div class="row form-group justify-content-center">
				        <button type="submit" class="btn bottest-button btn-success mr-2" onclick="validateAddInvited();" id="svCreateProject">
				        	<i class="ti-check mr-2"></i><spring:message code="label.Save" ></spring:message>
			        	</button>
				        <button type="button" class="btn bottest-button btn-secondary" onclick="resetFormInvite();" data-dismiss="modal" >
				        	<i class="ti-close mr-2"></i><spring:message code="label.Close" ></spring:message>
				        </button>
				    </div>
				</form>
			</div>
		</div>
	</div>
</div>



