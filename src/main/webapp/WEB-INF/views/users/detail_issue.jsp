<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<jsp:include page="/WEB-INF/views/users/projectHeader.jsp"></jsp:include>

<div class="card card-box card-topline-green" id="detailTask"> 
	<div class="content changeAddIssue" id="dtTask">
		<div>
			<div class="col-md-12 form-group">
				<button class="btnBackDetail btn bottest-button btn-secondary" onclick="window.location.href='${context}/project/${projectId}/issue'">
					<i class="icon-arrow-left"></i> Back
				</button>
				
			</div>	
			<div class="contextualtask mr-4">
				<a class="icon icon-edit" onclick ="editIssue()"id="edit-task">Edit</a>
				
			</div>
			<div id="title-detail-task"></div>
			<input id="taskIdentity" hidden value="">
			<div class="iFTask">
				<p id="parentTest" class="my-2"></p>
				<!-- <div class="next-prev-links next-task"><a href="#" id="previousIssue"><< Previous</a> |<a href="${context}/project/${projectId}/issue"> <span id="numberChange">2</span> of <span id="numberTotal">2</span> </a>| <a href="#" id="nextIssue">Next >></a> </div> -->
				<div class="title-subject"><p id="subject"></p>
					<p class="author" id="author"></p>
				</div>	
				<div class="row attributesDetail">
					<div class="col-md-6 contentTaskLeft">
						<div class="content-task d-flex">
							<div class="col-md-4 titleTask titleTask" ><spring:message code="label.Testsuite"></spring:message>:</div>
							<p class="col-md-8 value" id="testsuite"> </p>
						</div>
						<div class="content-task d-flex">
							<div class="col-md-4 titleTask titleTask" ><spring:message code="label.Milestone"></spring:message>:</div>
							<p class="col-md-8 value" id="milestone"> </p>
						</div>
						<div class="content-task d-flex">
							<div class="col-md-4 titleTask titleTask" ><spring:message code="label.Status"></spring:message>:</div>
							<p class="col-md-8 value" id="status"> </p>
						</div>
						<div class="content-task d-flex">
							<div class="col-md-4 titleTask">% <spring:message code="label.Done"></spring:message>:</div>
							<p class="col-md-8 value" id="percentDone"></p>
						</div>
						<div class="content-task d-flex">
							<div class="col-md-4 titleTask">Category:</div>
							<p class="col-md-8 value" id="category"></p>
						</div>
						<div class="content-task d-flex">
							<div class="col-md-4 titleTask"><spring:message code="label.Priority"></spring:message>:</div>
							<p class="col-md-8 value" id="priority"></p>
						</div>
					</div>
					<div class="col-md-6 contentTaskLeft">
						<div class="content-task d-flex">
							<div class="col-md-4 titleTask"><spring:message code="label.Assignto"></spring:message>:</div>
							<p class=" col-md-8 value" id="assignee"></p>
						</div>
						<div class="content-task d-flex">
							<div class="col-md-4 titleTask"><spring:message code="label.PlanStartDate"></spring:message>:</div>
							<p class="col-md-8 value" id="planStartDate"></p>
						</div>
						<div class="content-task d-flex">
							<div class="col-md-4 titleTask"><spring:message code="label.PlanEndDate"></spring:message>:</div>
							<p class="col-md-8 value" id="planEndDate"></p>
						</div>
						<div class="content-task d-flex">
							<div class="col-md-4 titleTask"><spring:message code="label.ActualStartDate"></spring:message>:</div>
							<p class="col-md-8 value" id="actualStartDate"></p>
						</div>
						<div class="content-task d-flex">
							<div class="col-md-4 titleTask"><spring:message code="label.ActualEndDate"></spring:message>:</div>
							<p class="col-md-8 value" id="actualEndDate"></p>
						</div>
						<div class="content-task d-flex">
							<div class="col-md-4 titleTask"><spring:message code="label.EstimatedTimes" ></spring:message>:</div>
							<p class="col-md-8 value" id="estimatedTime"></p>
						</div>
						<div class="content-task d-flex">
							<div class="col-md-4 titleTask"><spring:message code="label.ActualTimes"></spring:message>:</div>
							<p class="col-md-8 value" id="actualTime"></p>
						</div>
					</div>
				</div>
				<hr>
				<div class="task-tree">
					<p class="subtasks"><strong>Description</strong></p>
					<div id="description">
					</div>
					<div class="col-md-12">
						<div class="row" id="fileTask">
					        
					    </div>
					</div>
				</div>
				<hr>
				<div class="task-tree">
					<div class="add-subtasks"><a href ="#" onclick="addSubTask('true', 'true');">Add</a></div>
					<p  class="subtasks" ><strong><spring:message code="label.Subtasks"></spring:message></strong></p>
					<div class="d-flex flex-row">
						<table class="table table-borderless table-result-action">
							<tbody id="detailSubTask">
								
							</tbody>
						</table>
					</div>
				</div>
				<!-- <hr>
				<div class="task-tree d-flex flex-row-reverse justify-content-between w-100">
					<div class="add-subtasks issues m-0"><a href="">Add</a></div>
					<p class="subtasks"><strong>Related issues</strong></p>
				</div>
				 -->
			</div>
			<div id="history">
				<p class="ml-3 history font-weight-bold"><spring:message code="label.History"></spring:message></p>
				<div  class="ml-3" id="contentUpDate"></div>
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/users/modal_add_new_issue.jsp"></jsp:include>
</div>


<script src="${context}/assets/js/management/issueDetail.js" ></script>
<script src="${context}/assets/js/management/tasks.js" ></script>
<script>
	var classTab = $(".text-uppercase.border-success");
	$.each(classTab, function(index, values){
	    if($(values).hasClass("active show") == true){
	        $(values).removeClass("active show");
	    }
	});
	$("#callIssueTab").addClass("active show");
 	var task = ${task};
 	var context = "${context}";
 	var projectId = ${projectId};
 	//var numberDraw = "${numberDraw}";
 	//var lengthTask = "${lengthTask}";
 	//var taskPreId = "${taskPre}";
 	//var taskNextId = "${taskNext}";
 	var taskList = ${taskList};
 	var listBug = ${listBug};
 	
 	issueDetailById(task);
</script>
<jsp:include page="/WEB-INF/views/users/modal_show_image.jsp"></jsp:include>
