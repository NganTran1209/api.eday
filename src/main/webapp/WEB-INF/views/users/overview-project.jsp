<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="preloader loading-important">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting"></spring:message><br> Bottest.io</p>
    </div>
</div>
<jsp:include page="/WEB-INF/views/users/projectHeader.jsp"></jsp:include>

<div class="clearfix"></div>

<div class="row changeAddIssue" id="drawSummary">
	<div class="col-md-12 mb-2">
         <div class="card card-box card-topline-green">
             <div class="card-head card-head-icon">
             	<header class="headerIssue"><spring:message code="label.Issue"></spring:message></header>
                 <div class="legend headerLe w-100 text-right mr-3" id="legendIssue"></div>
             </div>
             <div class="card-body no-padding height-9">
             	<div class="text-danger" style="text-align: center;" id="issueCircleEmpty"></div>
               	<div class="col-12">
               		<div class="row"  id="progressIssueAdd"></div>
                </div>
            </div>
        </div>
    </div>
     <div class="col-md-12 mb-2">
         <div class="card card-box card-topline-green">
             <div class="card-head card-head-icon">
                 <header><spring:message code="label.CreatingProgress"></spring:message></header>
                 <div class="legend headerLe w-100 text-right mr-3" id="legendCreateProgress"></div>
             </div>
             <div class="card-body no-padding height-9">
             	 <div  class="text-danger" style="text-align: center;" id="creatingProgressCircleEmpty"></div>
                 <div class="row" id="progressCreateAdd">
	      			
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mb-2">
         <div class="card card-box card-topline-green">
             <div class="card-head card-head-icon">
                 <header><spring:message code="label.TestProgress"></spring:message></header>
                 <div class="legend headerLe w-100 text-right mr-3" id="legendTestProgress"></div>
             </div>
             <div class="card-body no-padding height-9">
             	<div class="text-danger" style="text-align: center;" id="testProgressCircleEmpty"></div>
                 <div class="row" id="progressTestAdd">
                 	
                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="/WEB-INF/views/users/modal_add_new_issue.jsp"></jsp:include>

<script src="${context}/assets/js/management/summary-chart.js"></script>
<script src="${context}/assets/js/management/tasks.js" ></script>
<script>
	var classTab = $(".text-uppercase.border-success");
	$.each(classTab, function(index, values){
	    if($(values).hasClass("active show") == true){
	        $(values).removeClass("active show");
	    }
	});
	$("#tabsSummary").addClass("active show");
	 var listGraphics = '${listGraphics}';
	 var listTestPassFail = '${listTestPassFail}';
	 var listIssueGraphics = '${listIssue}';
	 var listManAut = '${listManAut}';
	 var listStatus = '${listStatus}';
	 var projectId = ${projectId};
	 var listBug=${listBug};
	 var taskList = ${taskList};
	 var listRole = ${listPermisson};
	 var listPermisson =JSON.parse(listRole[0].role_detail);
	 $(function(){
		 var hash = window.location.hash;
		 hash && $('ul.nav a[href="' + hash + '"]').tab('show');
		 $('.nav-tabs a').click(function (e) {
		   $(this).tab('show');
		   var scrollmem = $('body').scrollTop();
		   window.location.hash = this.hash;
		   $('html,body').scrollTop(scrollmem);
		 });
	 });
</script>