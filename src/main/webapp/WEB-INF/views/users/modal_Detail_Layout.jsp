<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="modalDetailLayout" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-success">
            	<h4 class="modal-title mt-0"><spring:message code="label.LayoutDetail"></spring:message></h4>
                <button type="button" class="close" data-dismiss="modal" onclick="resetFormDeitalLayout()">×</button>
            </div>
            <form class="container-fluid d-inline-block form-horizontal mt-4" id="bodyLayoutDetails" action="javascript:void(0)" accept-charset="UTF-8" method="post"> 
            <label hidden id="hiddenDataName"></label>
            <div class="modal-body p-0">
                	<div class="row"> 
	                    <div class="col-md-6">
	                        <div class="row item form-group bad controllNameVal">
	                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Control Name (<span class="text-danger">*</span>)</label>
	                            <div class="col-md-7 col-sm-12 col-xs-8">
	                            	<div class="input-icon right "><i class="fa controlNameIcon"></i>
	                                	<input class="form-control" type="text" name="controlName" id="valControlName" required="required">
	                               	</div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-md-6">
	                        <div class="row item form-group bad">
	                            <label class="control-label col-md-5 col-sm-12 col-xs-4">
	                                <span class="translation_missing" title="translation missing: en.label.controlType">Control Type</span> (<span class="text-danger">*</span>)
	                            </label>
	                            <div class="col-md-7 col-sm-12 col-xs-8">
	                                <select name="type" id="controlType" class="form-control" required = "required">
	                                    <option value="">Please select</option>
	                                    <option value="a">A</option>
	                                    <option value="label">LABEL</option>
	                                    <option value="text">TEXT</option>
	                                    <option value="password">PASSWORD</option>
	                                    <option value="textarea">TEXTAREA</option>
	                                    <option value="submit">SUBMIT</option>
	                                    <option value="button">BUTTON</option>
	                                    <option value="radio">RADIO</option>
	                                    <option value="select">SELECT</option>
	                                    <option value="checkbox">CHECKBOX</option>
	                                    <option value="img">IMG</option>
	                                    <option value="input">INPUT</option>
	                                    <option value="span">SPAN</option>
	                                    <option value="div">DIV</option>
	                                    <option value="h1">H1</option>
	                                    <option value="h2">H2</option>
	                                    <option value="h3">H3</option>
	                                    <option value="h4">H4</option>
	                                    <option value="h5">H5</option>
	                                    <option value="h6">H6</option>
	                                    <option value="tr">TR</option>
	                                    <option value="td">TD</option>
	                                    <option value="li">LI</option>
	                                    <option value="ul">UL</option>
	                                    <option value="i">I</option>
	                                    <option value="p">P</option>
	                                    <option value="svg">SVG</option>
	                                </select>
	                            </div>
	                        </div>
	                    </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12"><hr></div>
                    <div class="row">
	                    <div class="col-md-6">
	                        <div class="row form-group">
	                            <label class="control-label col-md-5 col-sm-12 col-xs-4">ID</label>
	                            <div class="col-md-7 col-sm-12 col-xs-8">
	                                <input class="form-control" type="text" name="id" id="idLayout">
	                            </div>
	                            <!-- <div class="col-md-2 col-sm-12 col-xs-2 text-center">
	                                <div class="checkbox checkbox-green">
	                                	<input type="checkbox" class="flat" name="id" id="idDetect">
	                                	<label for="idDetect"></label>
	                                </div>
	                            </div> -->
	                        </div>
                        </div>
                        <div class="col-md-6">
	                        <div class="row form-group">
	                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Name</label>
	                            <div class="col-md-7 col-sm-12 col-xs-8">
	                                <input class="form-control" type="text" name="name" id="nameLayout">
	                            </div>
	                            <!-- <div class="col-md-2 col-sm-12 col-xs-2 text-center">
	                            	<div class="checkbox checkbox-green">
	                                	<input type="checkbox" class="flat" name="name" id="nameDetect">
	                                	<label for="nameDetect"></label>
	                                </div>
	                            </div> -->
	                        </div>
                       	</div>
                        
	                  </div>
	                  <div class="row">
	                  		<div class="col-md-6">
		                        <div class="row form-group">
		                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Class</label>
		                            <div class="col-md-7 col-sm-12 col-xs-8">
		                                <input class="form-control" type="text" name="class" id="classLayout">
		                            </div>
		                            <!-- <div class="col-md-2 col-sm-12 col-xs-2 text-center">
		                                <div class="checkbox checkbox-green">
		                                	<input type="checkbox" class="flat" name="class" id="classDetect">
		                                	<label for="classDetect"></label>
		                                </div>
		                            </div> -->
		                        </div>
	                        </div>
	                   		<div class="col-md-6">
		                        <div class="row form-group">
		                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Xpath</label>
		                            <div class="col-md-7 col-sm-12 col-xs-8">
		                                <input class="form-control" type="text" name="xpath" id="xpathLayout">
		                            </div>
		                            <!-- <div class="col-md-2 col-sm-12 col-xs-2 text-center">
		                            	 <div class="checkbox checkbox-green">
		                                	<input type="checkbox" class="flat" name="xpath" id="xpathDetect">
		                                	<label for="xpathDetect"></label>
		                                </div>
		                            </div> -->
		                        </div>
	                        </div>
	                   	</div>
	                   	<div class="row">
	                   		<div class="col-md-6">
		                         <div class="row form-group">
		                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Href</label>
		                            <div class="col-md-7 col-sm-12 col-xs-8">
		                                <input class="form-control" type="text" name="href" id="hrefLayout">
		                            </div>
		                            <!-- <div class="col-md-2 col-sm-12 col-xs-2 text-center">
		                            	<div class="checkbox checkbox-green">
		                                	<input type="checkbox" class="flat" name="href" id="hrefDetect">
		                                	<label for="hrefDetect"></label>
		                                </div>
		                            </div> -->
		                        </div>
	                   		</div>
	                  		
	                        <div class="col-md-6">
		                        <div class="row form-group">
		                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Text value</label>
		                            <div class="col-md-7 col-sm-12 col-xs-8">
		                                <input class="form-control" type="text" name="textContent" id="textContentLayout">
		                            </div>
		                            <!-- <div class="col-md-2 col-sm-12 col-xs-2 text-center">
		                            	<div class="checkbox checkbox-green">
		                                	<input type="checkbox" class="flat" name="textContent" id="textContentDectect">
		                                	<label for="textContentDectect"></label>
		                                </div>
		                            </div> -->
		                        </div>
	                        </div>
                        </div>
                        <div class="row">
	                        <div class="col-md-6">
		                        <div class="row form-group">
		                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Init value</label>
		                            <div class="col-md-7 col-sm-12 col-xs-8">
		                                <input class="form-control" type="text" name="initvalue" id="initValueLayout">
		                            </div>
		                        </div>
	                        </div>
	                        <div class="col-md-6">
		                        <div class="row form-group">
		                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Data format</label>
		                            <div class="col-md-7 col-sm-12 col-xs-8">
		                                <select name="dataformat" id="dataFormatControlType" class="form-control">
		                                    <option value="">Please select</option>
		                                    <option value="Text">Text</option>
		                                    <option value="Number">Number</option>
		                                    <option value="HIRAGANA">HIRAGANA</option>
		                                    <option value="KATAKANA">KATAKANA</option>
		                                    <option value="####,###,###.###">####,###,###.###</option>
		                                    <option value="YYYY/MM/DD">YYYY/MM/DD</option>
		                                    <option value="MMM-DD-YYYY">MMM-DD-YYYY</option>
		                                    <option value="MM-DD-YYYY">MM-DD-YYYY</option>
		                                    <option value="DD-MM-YYYY">DD-MM-YYYY</option>
		                                    <option value="DD/MM/YYYY">DD/MM/YYYY</option>
		                                    <option value="MM/DD/YYYY">MM/DD/YYYY</option>
		                                    <option value="YYYY/DD/MM">YYYY/DD/MM</option>
		                                    <option value="YYYYMMDD">YYYYMMDD</option>
		                                    <option value="YYYYDDMM">YYYYDDMM</option>
		                                    <option value="MMDDYYYY">MMDDYYYY</option>
		                                    <option value="DDMMYYYY">DDMMYYYY</option>
		                                    <option value="DD-MMM-YYYY">DD-MMM-YYYY</option>
		                                    <option value="YYYY-MMM-DD">YYYY-MMM-DD</option>
		                                    <option value="YYYY-DD-MMM">YYYY-DD-MMM</option>
		                                    <option value="YYYY-MM-DD">YYYY-MM-DD</option>
		                                    <option value="YYYY-DD-MM">YYYY-DD-MM</option>
		                                    <option value="#,###,###">#,###,###</option>
		                                    <option value="HALFSIZEKATAKANA">HALFSIZEKATAKANA</option>
		                                </select>
		                            </div>
		                        </div>
	                        </div>
                     	</div>
                       	<div class="row">
                       		<div class="col-md-6">
                       	 		<div class="row form-group">
		                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Readonly</label>
		                            <div class="col-md-2 col-sm-12 col-xs-2 text-center">
		                            	<div class="checkbox checkbox-green">
		                                	<input type="checkbox" class="flat" name="readonly" value="" id="readOnlyLayout">
		                                	<label for="readOnlyLayout"></label>
		                                </div>
		                            </div>
		                        </div>
                       	 	</div>
                       	 	<div class="col-md-6">
                       	 		 <div class="row form-group">
		                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Disabled</label>
		                            <div class="col-md-2 col-sm-12 col-xs-2 text-center">
		                            	<div class="checkbox checkbox-green">
		                                	<input class="flat" type="checkbox" name="disabled" value="" id="disableLayout">
		                                	<label for="disableLayout"></label>
		                                </div>
		                            </div>
		                        </div>
                       	 	</div>	
                        </div>
                        
                        <div class="row">	
                       	   
                       	 	<div class="col-md-6">
		                        <div class="row form-group">
		                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Min length</label>
		                            <div class="col-md-7 col-sm-12 col-xs-8">
		                                <input class="form-control setlengthlayout" type="text" name="minlength" id="minLengthLayout">
		                            </div>
		                        </div>
	                        </div>
                       	 	<div class="col-md-6">
                       	 		<div class="row form-group">
		                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Max length</label>
		                            <div class="col-md-7 col-sm-12 col-xs-8">
		                                <input class="form-control setlengthlayout" type="text" name="maxlength" id="maxLengthLayout">
		                            </div>
		                        </div>
                       	 	</div>
                     	 </div>
                     	 <div class="row">	
                     	 	<div class="col-md-6">
                       	 		<div class="row form-group">
		                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Required</label>
		                            <div class="col-md-2 col-sm-12 col-xs-2 text-center">
		                            	<div class="checkbox checkbox-green">
		                                	<input class="flat" type="checkbox" name="required" value="" id="requiredLayout">
		                                	<label for="requiredLayout"></label>
		                                </div>
		                            </div>
		                        </div>
                       	 	</div>
                     	 	<div class="col-md-6">
		                        <div class="row form-group">
		                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Background Color</label>
		                            <div class="col-md-7 col-sm-12 col-xs-8">
		                                <input class="form-control" type="text" name="background-color" id="backgroundColorLayout">
		                            </div>
		                        </div>
	                        </div>
                        </div>   
                    	<div class="row">
                    		<div class="col-md-6">
                       	 		<div class="row form-group">
		                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Font color</label>
		                            <div class="col-md-7 col-sm-12 col-xs-8">
		                                <input class="form-control" type="text" name="color" id="fontColorLayout">
		                            </div>
		                        </div>
                       	 	</div>
                      	 	<div class="col-md-6">
                      			<div class="row form-group">
	                            	<label class="control-label col-md-5 col-sm-12 col-xs-4">Text Align</label>
		                            <div class="col-md-7 col-sm-12 col-xs-8">
		                                <input class="form-control" type="text" name="text-align" id="textAlignLayout">
		                            </div>
	                       		 </div>
	                        </div>
                    	 </div>
                    	 <div class="row">
                    		<div class="col-md-6">
                       	 		<div class="row item form-group bad errorAttName">
		                            <label class="control-label col-md-5 col-sm-12 col-xs-4">Error Attribute Name</label>
		                            <div class="col-md-7 col-sm-12 col-xs-8">
			                            <div class="input-icon right "><i class="fa errorAttName"></i>
			                                <input class="form-control" type="text" name="errorAttributeName" id="errorAttributeName">
			                            </div>
		                            </div>
		                        </div>
                       	 	</div>
                      	 	<div class="col-md-6">
                      			<div class="row item form-group bad AttrValue">
	                            	<label class="control-label col-md-5 col-sm-12 col-xs-4">Error Attribute Value</label>
		                            <div class="col-md-7 col-sm-12 col-xs-8">
	                            		<div class="input-icon right "><i class="fa itemAttrValue"></i>
		                                	<input class="form-control" type="text" name="errorAttributeValue" id="errorAttributeValue">
		                                </div>
		                            </div>
	                       		 </div>
	                        </div>
	                        <div class="col-md-12">
                      			<div class="row item form-group bad AttrValue">
	                            	<label class="control-label col-md-2 col-sm-12 col-xs-4" >Note</label>
		                            <div class="col-md-10 col-sm-12 col-xs-8  form-note" >
	                            		<div class="input-icon right "><i class="fa itemAttrValue"></i>
		                                	<!-- <input class="form-control" type="text" name="noteLayout" id="noteLayout"> -->
		                                	<textarea class="form-control" rows="3" placeholder="Please enter" name="noteLayout" 
												id="noteLayout"></textarea>
		                                </div>
		                            </div>
	                       		 </div>
	                        </div>
                    	 </div>
               	
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bottest-button btn-success hidden" id="updateControl" onclick="updateControls();"><i class="ti-check f-left mr-2"></i> Update</button>
                <button type="button" class="btn bottest-button btn-success hidden" id="saveNewControl" onclick="saveNewControls(this);"><i class="ti-check f-left mr-2"></i> Save</button>
                <button type="button" class="btn bottest-button btn-success hidden" id="btnNewControlToLayout" onclick="saveNewControlToLayout();"><i class="ti-check f-left mr-2"></i>Create</button>
                <button type="button" class="btn bottest-button btn-secondary" onclick="resetFormDeitalLayout();" data-dismiss="modal"><i class="ti-close mr-2"></i> Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
<script>
 //extrude some character special and negative number
	$(".setlengthlayout").on("keypress keyup blur",function (event) {    
		    $(this).val($(this).val().replace(/[^\d].+/, ""));
		     if ((event.which < 48 || event.which > 57)) {
		         event.preventDefault();
		     }
	 });
 
	
</script>