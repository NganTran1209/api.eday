<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="modalAddNewTestsuite" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0">Add New Testsuite</h4>
				<button type="button" class="close" data-dismiss="modal"  onclick="resetFormTestsuite()" >×</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal form-label-left form-addNew-common" id="new_testsuite" action="javascript:void(0)" accept-charset="UTF-8" method="post">
				    <div class="row item form-group bad addNewTS">
				        <label class="control-label col-md-4 col-sm-12 col-xs-4 text-right" for="name">
				        	Testsuite Name<span class="required">*</span>
				        </label>
				        <div class="col-md-6 col-sm-6 col-xs-12">
				        	<div class="input-icon right"><i class="fa testsuiteName"></i>
				            	<input class="form-control" placeholder="" required="required" type="text" name="testsuiteName" id="testsuite_name" title="" alt="" >
				        	</div>
				        </div>
				    </div>
				    <div class="row item form-group">
				        <label class="control-label col-md-4 col-sm-12 col-xs-4 text-right" for="name">
				        	Description<span class="required">*</span>
				        </label>
				        <div class="col-md-6 col-sm-6 col-xs-12">
				        	<div class="input-icon right"><i class="fa testSuiteDes" data-toggle="tooltip"></i>
				            	<textarea class="form-control" placeholder="" required="required" rows="3" name="testsuiteDescription" id="testsuite_description"></textarea>
				       		</div>
				        </div>
				    </div>
				    <div class="row form-group justify-content-center">
				        <button type="submit" class="btn bottest-button btn-success mr-2" onclick="validateAddTestSuite();" id="addNewTestsuite"><i class="ti-check"> </i> Save</button>
				        <button type="button" class="btn bottest-button btn-secondary" data-dismiss="modal" onclick="resetFormTestsuite()"><i class="ti-close" > </i> Close</button>
				    </div>
				</form>
			</div>
		</div>
	</div>
</div>