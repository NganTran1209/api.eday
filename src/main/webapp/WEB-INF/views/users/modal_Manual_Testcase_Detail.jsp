<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="modalDetailManualTestcase" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-lg" style="max-width:80%">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0">Add New Manual Testcase</h4>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-body">
				<form class="col-md-12 form-horizontal form-label-left form-addNew-common " id="formGetDataMenualTestcase" action="javascript:void(0)" accept-charset="UTF-8" method="post" novalidate="novalidate">
				    <div class="row mb-3 newNameTestCase elementValidate">
				        <div class="col-md-7">
				        	<!-- <div class="item form-group mb-0 bad testcase-input">
					        	 <label class="control-label w-100 text-left">
						        	Testcase Name<span class="required">*</span>
						        </label>
						        <div class="input-icon right"><i class="fa testCaseName"></i>
					            	<input class="form-control" required="required" type="text" name="testcaseName" maxlength="30" id="testcaseName" aria-required="true" placeholder="Please enter...">
					            </div>
				            </div> -->
				            <div class="item form-group mb-2 bad ">
		            			<label class="control-label w-100 text-left">
						        	Testsuite <span class="required">*</span>
						        </label>
						        <div class="input-icon right"><i class="fa descriptionTC" data-toggle="tooltip"></i>
						        	<select class="form-control" id="testsuiteMan" name="testsuiteLs">
									</select>
						        </div>
						        
						    	
							</div>
							<div class="item form-group mb-2 bad ">
								<label class="control-label w-100 text-left">
						        	Description<span class="required">*</span>
						        </label>
						        <div class="input-icon right"><i class="fa descriptionTC" data-toggle="tooltip"></i>
						            <textarea class="form-control" rows="5" required="required" name="testcaseDescription" id="testcaseDesMal" aria-required="true" placeholder="Please enter"  style="height:126px;"></textarea>
					            </div>
				            </div>
							<div class="item form-group mb-2 bad ">
				            	<div class="row">
				            		<div class="col-md-6">
				            			<label class="w-100 control-label text-left">Assignee to<span class="required">*</span></label>
										<select class="form-control" id="userBottestAss" name="userBottestAss">
										</select>
				            		</div>
				            		<div class="col-md-6">
				            			<label class="control-label w-100 text-left">
								        	<spring:message code="label.Status"></spring:message> <span class="required">*</span>
								        </label>
								        <select class="form-control" id="statustaskTc" name="statustaskTc">
										</select>
				            		</div>
				            	</div>
							</div>
				            <div class="item mb-2 bad ">
					            <div class="row">
					            	<div class="col-md-6">
					            		<div class="item form-group mb-0">
					            			<label class="control-label w-100 text-left">
									        	Milestone <span class="required">*</span>
									        </label>
									        <div class="input-icon right"><i class="fa" data-toggle="tooltip"></i>
									        	<select class="form-control" id="milestoneManulTC" name="milestoneManulTC">
												</select>
									        </div>
					            		</div>
				            		</div>
					            	
				            		<div class="col-md-6">
				            			<label class="control-label w-100 text-left">
								        	Category 
								        </label>
								        <select class="form-control" id="categorytaskTc" name="categorytaskTc">
										</select>
				            		</div>
				            	</div>
							</div>
				            <div class="item form-group mb-2 bad ">
				            	<div class="row">
			            			<div class="col-md-6">
				            			<label class="control-label w-100 text-left">
								        	<spring:message code="label.PlanStartDate"></spring:message> 
								        </label>
								        <input id="planStarDa" class="form-control date" value="" type="text" placeholder="mm/dd/yyyy"/>
			            			</div>
				            		<div class="col-md-6">
				            			<div class="item form-group mb-0">
					            			<label class="w-100 control-label text-left"><spring:message code="label.PlanEndDate"></spring:message></label>
					            			<div class="input-icon right"><i class="fa" data-toggle="tooltip"></i>
												<input id="planEndDa" name="endDate" class="form-control date" value="" type="text" placeholder="mm/dd/yyyy"/>
											</div>
										</div>
				            		</div>
				            	</div>
				            </div>
				            <div class="item form-group mb-2 bad ">
				            	<div class="row">
						            <div class="col-md-6">
						            	<div class="row">
						            		<div class="col-md-6">
				            					<label class="control-label w-100 text-left">
										        	<spring:message code="label.EstimatedTimes"></spring:message>
										        </label>
										        <input id="estimatedTime" class="form-control" value=""/>
				            				</div>
				            				<div class="col-md-6">
					            				<label class="w-100 control-label text-left"><spring:message code="label.ActualTimes"></spring:message></label>
												<input id="actualTime" class="form-control " value=""/>
				            				</div>
						            	</div>
						            </div>
						         </div>
						      </div>   
				        </div>
				        <div class="col-md-5">
				        	<div class="item form-group mb-2 bad ">
				        		<label class="control-label w-100 text-left">
							        	Pre-Condition
						        </label>
						        <div class="input-icon right"><i class="fa descriptionTC" data-toggle="tooltip"></i>
						            <textarea class="form-control" rows="4" id="preCondition"  name="preCondition" placeholder="Please enter" style="height: 101px;"></textarea>
					            </div>
				        	</div>
				        	<div class="item form-group mb-2 bad ">
					        	<label class="w-100 control-label text-left">Testcase Procedure<span class="required">*</span></label>
					        	<div class="input-icon right"><i class="fa" data-toggle="tooltip"></i>
									<textarea rows="9" class="form-control" placeholder="Please enter" id="tcProcedure" name="tcProcedure"></textarea>
								</div>
							</div>
							<div class="item form-group mb-2 bad ">
								<label class="w-100 control-label text-left">Expected Output<span class="required">*</span></label>
								<div class="input-icon right"><i class="fa" data-toggle="tooltip"></i>
									<textarea rows="9" class="form-control" placeholder="Please enter" id="expOutput" name="expOutput"></textarea>
								</div>
							</div>
						</div>
				    </div>
				    <input id="dateCreateStart" type="hidden" name="dateCreateStart">
				    <input id="manualTestcaseName" type="hidden" name="manualTcName">
				    <input id="orderIdMan" type="hidden"/>
 					<input id="checkScreen" type="hidden"/>
				    <div class="row form-group justify-content-center">
				        <button type="submit" class="btn bottest-button btn-success mr-2" id="btnSaveNewManualTsc" style="display:none" onclick="saveNewManualTsc()"><i class="ti-check mr-2"></i> Save</button>
				        <button type="submit" class="btn bottest-button btn-success mr-2" id="btnUpdateNewManualTsc"  style="display:none" onclick="updateManualTsc()"><i class="ti-check mr-2"></i> Update</button>
				        <button type="button" class="btn bottest-button btn-secondary" data-dismiss="modal" onclick="hidenModalManual()"><i class="ti-close mr-2"></i> Close</button>
				    </div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	$('#planStarDa').bootstrapMaterialDatePicker
	({
		format: 'MM/DD/YYYY',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});
	$('#planEndDa').bootstrapMaterialDatePicker
	({
		format: 'MM/DD/YYYY',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});
</script>