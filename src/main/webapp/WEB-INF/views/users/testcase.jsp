<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<style>
.error {
 color:red;
 text-align: center;
}

</style>
<jsp:include page="/WEB-INF/views/users/projectHeader.jsp"></jsp:include>

<div class="card card-box card-topline-green">
	<div class="card-body">
		<div id="tableListTestcase" class="changeAddIssue">
			<div class="col-md-12 form-group">
				<div class="row">
					<div class="col-md-12">
						<div class="row" >
							<spring:message code="label.SelectTestsuite" var="selectTestsuite"></spring:message>
							<div class="col-md-2 mb-3" data-toggle="tooltip" data-placement="top" data-original-title="${selectTestsuite}">
								<select class="form-control testsuiteFilter" multiple="multiple" id="filterTestsuite">
								</select>
							</div>
							<spring:message code="label.SelectMilestone" var="selectMilestone"></spring:message>
							<div class="col-md-2 mb-3" data-toggle="tooltip" data-placement="top" data-original-title="${selectMilestone}">
								<select class="form-control milestoneFilter" multiple="multiple" id="filterTestcaseMilestone">
								</select>
							</div>
							<spring:message code="label.Testcasecategory" var ="testcasescategory"></spring:message>
							<div class="col-md-2 mb-3" data-toggle="tooltip" data-placement="top" data-original-title="${testcasescategory}">
								<select class="form-control" multiple="multiple" id="filterTestcaseCategory">
								</select>
							</div>
							<spring:message code="label.Testcasestatus" var ="testcasestatus"></spring:message>
							<div class="col-md-2 mb-3" data-toggle="tooltip" data-placement="top" data-original-title="${testcasestatus}">
								<select class="form-control" multiple="multiple" id="filterTestcaseStatus">
								</select>
							</div>
							<spring:message code="label.SelectassignTo" var ="selectassignTo"></spring:message>
							<div class="col-md-2 mb-3" data-toggle="tooltip" data-placement="top" data-original-title="${selectassignTo}">
								<select class="form-control assignTo" multiple="multiple" id="filterAssignTo">
								</select>
							</div>
							<div class="col-md-2 mb-3">
								<spring:message code="label.Enterdescription" var="enterDescription"></spring:message>
								<input class="form-control colorPlace" placeholder="${enterDescription}" id="filterDescription" data-toggle="tooltip" data-placement="top" data-original-title="${enterDescription}" value=""/>
							</div>
						</div>
						<div class="row">
							<spring:message code="label.ExecuteStatus" var="executeStatus"></spring:message>
							<div class="col-md-2 mb-3" data-toggle="tooltip" data-placement="top" data-original-title="${executeStatus}">
								<select class="form-control" id="filterStatusTestcase">
									<option value=""></option>
									<option value="PASS"><spring:message code="label.PASS"></spring:message></option>
									<option value="FAILED"><spring:message code="label.FAILED"></spring:message></option>
									<option value="DEGRADE"><spring:message code="label.DEGRADE"></spring:message></option>
									<option value="NOT-RUN">- ( <spring:message code="label.NotRun"></spring:message> )</option>
								</select>
							</div>
							<spring:message code="label.SelectExecuteBy" var="selectExecuteBy"></spring:message>
							<div class="col-md-2 mb-3" data-toggle="tooltip" data-placement="top" data-original-title="${selectExecuteBy}">
								<select class="form-control executeBy" multiple="multiple" id="filterExecuteBy">
								</select>
							</div>
							<div class="col-md-2 mb-3">
								<spring:message code="label.ExecuteFromDate" var="executeFromDate"></spring:message>
								<input class="form-control fromDay tooltip-primary colorPlace" data-toggle="tooltip" data-placement="top" data-original-title="${executeFromDate}" id="filterFromDate" placeholder="<spring:message code="label.ExecuteFromDate"></spring:message>" />	
							</div>
							<div class="col-md-2 mb-3">
								<spring:message code="label.ExecuteToDate" var="executeToDate"></spring:message>
								<input class="form-control toDay colorPlace" data-toggle="tooltip" data-placement="top" data-original-title="${executeToDate}" id="filterToDate" placeholder="<spring:message code="label.ExecuteToDate"></spring:message>" />
							</div>
							<div class="col-md-2 mb-3">
								<button id="filterTcAuto" class="btnPlay btn bottest-button btn-success mr-2 w-100" onclick="filterAutoTestcase();"><i class="ti-search text-white"> </i> <spring:message code="label.Search"></spring:message></button>
							</div>
							<div class="col-md-2 mb-3">
								<button class="btnPlay btn bottest-button btn-success mr-2 w-100" id="btnAddNewTestcaseAutomation" onclick="funcAddNewTestcase()"><i class="ti-plus text-white"> </i><spring:message code="label.NewTestcase"></spring:message></button>
							</div>
						</div>
					</div>
				</div>  
				<div class="">
					<div class=" float-left " style="padding-top: 9px"> <strong ><spring:message code="label.Totaltestcases"></spring:message>: </strong><span id="totalTcAuto"></span></div>
					<div class="float-right" style="margin-bottom: 13px">
						<button type="button" class="btn bottest-button btn-success" id="exportTypeTestcase" onclick="getTypeTestcaseToCSV('automation')">
           			 	<i class="ti-export text-white mr-1"> </i> <spring:message code="label.Export"> </spring:message> 
         			 </button> 
					 </div>
				</div>
			</div>
			<div class="w-100 table-responsive" >
				<table class="table table-hover table-common" >
					<thead>
						<tr class="tr-bottest-hd bg-success">
							<th class="border-bottom-0 text-center">#</th> 
							<th class="border-bottom-0"><spring:message code="label.Testcases"></spring:message></th> 
							<th class="border-bottom-0"><spring:message code="label.TestcaseProcedure"></spring:message></th>
							<th class="border-bottom-0"><spring:message code="label.ExpectedOutput"></spring:message> </th>
							<th class="border-bottom-0"><spring:message code="label.LastRun"></spring:message> </th>
						</tr>
					</thead>
			
					<tbody id="bodyTestcaseRecord"></tbody>
				</table>
				
			</div>
			<div class="row position-relative">
					<div class="preloader progress-loadMainContent" id="loadListTestCase">
					    <div class="loader" style="top:30%">
					        <div class="loader__figure"></div>
					        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting"></spring:message><br> Bottest.io</p>
					    </div>
					</div>
				</div>
		</div>
		<jsp:include page="/WEB-INF/views/users/modal_add_new_issue.jsp"></jsp:include>
	</div>
</div>
<script src="${context}/assets/js/management/testcases.js"></script>
<script src="${context}/assets/js/management/tasks.js" ></script>
<script>
	var classTab = $(".text-uppercase.border-success");
	$.each(classTab, function(index, values){
	    if($(values).hasClass("active show") == true){
	        $(values).removeClass("active show");
	    }
	});
	$("#callAutomationTestcaseTab").addClass("active show");
	var projectId = ${projectId};
	var listRole = ${listPermisson};
 	var listPermisson =JSON.parse(listRole[0].role_detail);
	var context = "${context}";
	var autoTcLs = ${listTestcase};
	var taskList = ${tasks};
	var testsuiteNameSS = '${tsNameSession}';
	var executeBySlSS = '${executeBySession}';
	var statusTestcaseSS = "${stSession}";
	var fromDateSS = "${fromDate}";
	var toDateSS = "${toDate}";
	var assignToSS = '${assignTo}';
	var descriptionSS = "${descriptionSession}";
	var statusIssueSS = '${statusIssueSS}';
	var categoryIssueSS = '${categoryIssueSS}';
	var milestoneIssueSS = '${milestoneTestcaseSS}';
	var milestoneDate = '${mlStone}';
	var listBug="";
	var checkSession = true;
	var checkSessionExist = ${checkSessionExist};
	var statusIssueParse = "";
	if(statusIssueSS != ""){
		statusIssueParse = JSON.parse(statusIssueSS);
	}
	
	var categoryIssueParse = "";
	if(categoryIssueSS != ""){
		categoryIssueParse = JSON.parse(categoryIssueSS);
	}
	
	var testsuiteNameParse = "";
	if(testsuiteNameSS != ""){
		testsuiteNameParse = JSON.parse(testsuiteNameSS);
	}
	
	var executeBySlParse = "";
	if(executeBySlSS != ""){
		executeBySlParse = JSON.parse(executeBySlSS);
	}
	
	var assignToParse = "";
	if(assignToSS != ""){
		assignToParse = JSON.parse(assignToSS);
	}
	
	var milestoneParse = "";
	if(milestoneIssueSS != ""){
		milestoneParse = JSON.parse(milestoneIssueSS);
	}
	
	$('#filterFromDate').bootstrapMaterialDatePicker
	({
		format: 'YYYY-MM-DD',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});

	$('#filterToDate').bootstrapMaterialDatePicker
	({
		format: 'YYYY-MM-DD',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
	});
	
	
	getListTestsuite("true", function(tsByProjectId){
		$("#filterTestsuite").select2({
			placeholder: getLang(currentLocale,"Select Testsuite"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			tags: true,
			data: tsByProjectId,
			dropdownParent: ""
		});
		$("#filterTestsuite").select2({placeholder: getLang(currentLocale,"Select Testsuite"), theme: 'bootstrap'});
		$("#filterTestsuite").val(testsuiteNameParse);
		$("#filterTestsuite").trigger('change');
		
		$("#filterStatusTestcase").select2({
			placeholder: getLang(currentLocale,"Execute Status"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			allowClear: true,
			dropdownParent: ""
		});
		$("#filterStatusTestcase").select2({placeholder: getLang(currentLocale,"Execute Status"),allowClear: true, theme: 'bootstrap'});
		$("#filterStatusTestcase").val(statusTestcaseSS);
		$("#filterStatusTestcase").trigger('change')
	});
	
	infProjectCom(function(projectCo){
		var lsStatus = [];
		var lsCategory = [];
		for(var i = 0 ; i < projectCo.length; i++){
			if(projectCo[i]["groupCode"] == "Status"){
				lsStatus.push({"id": projectCo[i]["itemName"], "text": projectCo[i]["itemName"]});
			}
			if(projectCo[i]["groupCode"] == "Category"){
				lsCategory.push({"id": projectCo[i]["itemName"], "text": projectCo[i]["itemName"]});
			}
		}
		$("#filterTestcaseStatus").select2({
			placeholder: getLang(currentLocale,"Testcase status"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			tags: true,
			data: lsStatus,
			dropdownParent: ""
		});
		$("#filterTestcaseStatus").select2({placeholder: getLang(currentLocale,"Testcase status"), theme: 'bootstrap'});
		$("#filterTestcaseStatus").val(statusIssueParse);
		$("#filterTestcaseStatus").trigger('change');
		
		$("#filterTestcaseCategory").select2({
			placeholder: getLang(currentLocale,"Testcase category"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			tags: true,
			data: lsCategory,
			dropdownParent: ""
		});
		$("#filterTestcaseCategory").select2({placeholder: getLang(currentLocale,"Testcase category"), theme: 'bootstrap'});
		$("#filterTestcaseCategory").val(categoryIssueParse);
		$("#filterTestcaseCategory").trigger('change');
	});
	
	getAllMilestoneInIssue(function(milestoneLs){
		var lsMilesone = [];
		$.each( milestoneLs, function(index, value) { 
			lsMilesone.push({"id": value.projectMilestoneIdentity["mileStoneName"], "text": value.projectMilestoneIdentity["mileStoneName"]});
		});
		$("#filterTestcaseMilestone").select2({
			placeholder: getLang(currentLocale,"Testcase Milestone"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			tags: true,
			data: lsMilesone,
			dropdownParent: ""
		});
		$("#filterTestcaseMilestone").select2({placeholder: getLang(currentLocale,"Testcase Milestone"), theme: 'bootstrap'});
		$("#filterTestcaseMilestone").val(milestoneParse);
		$("#filterTestcaseMilestone").trigger('change');
	});
	
	getExecuteBy(function(listExecuteBy){
		$("#filterExecuteBy").select2({
			placeholder: getLang(currentLocale,"Select Execute By"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			data: listExecuteBy,
			tags: true,
			dropdownParent: ""
		});
		$("#filterExecuteBy").select2({placeholder: getLang(currentLocale,"Select Execute By"), theme: 'bootstrap'});
		$("#filterExecuteBy").val(executeBySlParse);
		$("#filterExecuteBy").trigger('change');
	});
	
	getUserAssignTo("true", function(lsAssignTo){
		$("#filterAssignTo").select2({
			placeholder: getLang(currentLocale,"Select assign To"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			data: lsAssignTo,
			tags: true,
			dropdownParent: ""
		});
		$("#filterAssignTo").select2({placeholder: getLang(currentLocale,"Select assign To"), theme: 'bootstrap'});
		$("#filterAssignTo").val(assignToParse);
		$("#filterAssignTo").trigger('change');
	});
	
	$("#filterFromDate").val(fromDateSS);
	$("#filterToDate").val(toDateSS);
	$("#filterDescription").val(descriptionSS);
	
	if((testsuiteNameSS != "" && testsuiteNameSS != "[]") || (executeBySlSS != "" && executeBySlSS != "[]") || statusTestcaseSS != "" || fromDateSS != "" || toDateSS != "" || (assignToSS != "" && assignToSS != "[]") || descriptionSS != "" || (statusIssueSS != "" && statusIssueSS != "[]") || (milestoneIssueSS != "" && milestoneIssueSS != "[]")  ){
		
	} else if(!checkSessionExist){
		testsuiteNameParse = [];
		executeBySlParse = [];
		fromDateSS = "";
		toDateSS = "";
		assignToParse = [];
		descriptionSS = "";
		statusTestcaseSS = "";
		statusIssueParse = [];
		categoryIssueParse = [];
		if(milestoneDate != ""){
			milestoneParse = [milestoneDate];
		} else {
			milestoneParse = [];
		}
		
	}
	drawTestcaseByFilter(testsuiteNameParse, executeBySlParse, statusTestcaseSS, fromDateSS, toDateSS, assignToParse, descriptionSS, statusIssueParse,categoryIssueParse,milestoneParse);
	
	
	
</script> 

<jsp:include page="/WEB-INF/views/users/modal_add_new_testcase.jsp"></jsp:include> 
<jsp:include page="/WEB-INF/views/users/modal_copy_testcase.jsp"></jsp:include>
<script src="${context}/assets/js/management/modal_run_testcases.js"></script>
<jsp:include page="/WEB-INF/views/users/modal_run_testcases.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/users/modal_copy_link.jsp"></jsp:include>
