<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="modal fade bs-example-modal-sm" id="chooseScreenTestData" tabindex="1" role="dialog" aria-hidden="true">
 	<div class="modal-dialog">
 	<form name="formChooseScreenTestData" id="formChooseScreenTestData" >
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0">Choose Screen</h4>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-body" id="modalBody">
				<div class="form-group">
				<div class=" col-xs-12 input-icon right "><i class="fa" data-toggle="tooltip"></i>
					<select id="listTestsuite" class="form-control" name="listTestsuite">
					</select>
					</div>
				</div>
				
			    <div class="form-group">
			    	<div class=" col-xs-12 input-icon right "><i class="fa" data-toggle="tooltip"></i>
				    <select id="listScreenLayout" class="form-control" name="listScreenLayout">
				    	
				    </select>
				    </div>
			    </div>
			    
			</div>
			<div class="modal-footer" id="modalFooter">
				<button type="submit" class="btn bottest-button btn-success mr-2" onclick="validateChooseScreenLayout();" id="saveChooseLayout">
					<i class="ti-check mr-2"></i>Save
				</button>
				<button type="button" class="btn bottest-button btn-secondary"
					data-dismiss="modal">
					<i class="ti-close mr-2"></i> Close
				</button>
			</div>
		</div>
		</form>
	</div>
</div>
<script>
	$("#listTestsuite").on('change.select2', function(){
		var testsuiteName = $("#listTestsuite").val();
		loadListScreenLayout(testsuiteName, "#listScreenLayout", false);
	})
</script>