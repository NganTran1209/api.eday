<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="row">
						<div class="col-sm-6">
							<div class="card  card-box">
								<div class="card-head">
									<header>Default Buttons</header>
								</div> 
								<div class="card-body ">
									<div class="row">
										<div class="col-md-12">
											<button type="button" class="btn btn-round btn-secondary">Default</button>
											<button type="button" class="btn btn-round btn-primary">Primary</button>
											<button type="button" class="btn btn-round btn-success">Success</button>
											<button type="button" class="btn btn-round btn-info">Info</button>
											<button type="button" class="btn btn-round btn-warning">Warning</button>
											<button type="button" class="btn btn-round btn-danger">Danger</button>
											<br>
											<h4 class="sub-title">Disable buttons</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<button class="btn btn-secondary disabled m-b-10">Disabled</button>
											<button class="btn btn-primary disabled m-b-10">Disabled</button>
											<button class="btn btn-success disabled m-b-10">Disabled</button>
											<button class="btn btn-info disabled m-b-10">Disabled</button>
											<button class="btn btn-warning disabled m-b-10">Disabled</button>
											<button class="btn btn-danger disabled m-b-10">Disabled</button>
											<br>
											<h4 class="sub-title">Outline Buttons</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<button type="button" class="btn default btn-outline m-b-10">Default</button>
											<button type="button" class="btn red btn-outline m-b-10">Red</button>
											<button type="button" class="btn blue-bgcolor btn-outline m-b-10">Blue</button>
											<button type="button" class="btn deepPink btn-outline m-b-10">Deep Pink</button>
											<button type="button" class="btn yellow btn-outline m-b-10">Yellow</button>
											<button type="button" class="btn purple btn-outline m-b-10">Purple</button>
											<button type="button" class="btn dark btn-outline m-b-10">Dark</button>
											<br>
											<h4 class="sub-title">Buttons Size</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<button type="button" class="btn btn-secondary btn-lg m-b-10">Large
												Button</button>
											<button type="button" class="btn btn-primary m-b-10">Default
												Button</button>
											<button type="button" class="btn btn-success btn-sm m-b-10">Small
												Button</button>
											<button type="button" class="btn btn-info btn-xs m-b-10">Extra
												Small Button</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="card  card-box">
								<div class="card-head">
									<header>Rounded Buttons</header>
								</div>
								<div class="card-body ">
									<div class="row">
										<div class="col-md-12">
											<button type="button" class="btn btn-circle btn-secondary">Default</button>
											<button type="button" class="btn btn-circle btn-primary">Primary</button>
											<button type="button" class="btn btn-circle btn-success">Success</button>
											<button type="button" class="btn btn-circle btn-info">Info</button>
											<button type="button" class="btn btn-circle btn-warning">Warning</button>
											<button type="button" class="btn btn-circle btn-danger">Danger</button>
											<br>
											<h4 class="sub-title">Disable Rounded Buttons</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<button class="btn btn-circle btn-secondary disabled m-b-10">Disabled</button>
											<button class="btn btn-circle btn-primary disabled m-b-10">Disabled</button>
											<button class="btn btn-circle btn-success disabled m-b-10">Disabled</button>
											<button class="btn btn-circle btn-info disabled m-b-10">Disabled</button>
											<button class="btn btn-circle btn-warning disabled m-b-10">Disabled</button>
											<button class="btn btn-circle btn-danger disabled m-b-10">Disabled</button>
											<br>
											<h4 class="sub-title">Outline Rounded Buttons</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<button type="button" class="btn default btn-outline btn-circle m-b-10">Default</button>
											<button type="button" class="btn red btn-outline btn-circle m-b-10">Red</button>
											<button type="button" class="btn blue-bgcolor btn-outline btn-circle m-b-10">Blue</button>
											<button type="button" class="btn deepPink btn-outline btn-circle m-b-10">Deep Pink</button>
											<button type="button" class="btn yellow btn-outline btn-circle m-b-10">Yellow</button>
											<button type="button" class="btn purple btn-outline btn-circle m-b-10">Purple</button>
											<button type="button" class="btn dark btn-outline btn-circle m-b-10">Dark</button>
											<br>
											<h4 class="sub-title">Rounded Buttons Size</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<button type="button" class="btn btn-circle btn-secondary btn-lg m-b-10">Large button</button>
											<button type="button" class="btn btn-circle btn-primary m-b-10">Default button</button>
											<button type="button" class="btn btn-circle btn-success btn-sm m-b-10">Small button</button>
											<button type="button" class="btn btn-circle btn-info btn-xs m-b-10">Extra small button</button>
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div>