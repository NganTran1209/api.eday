<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<div class="preloader preloader-main">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting" ></spring:message><br> Bottest.io</p>
    </div>
</div>
<div class="card card-box card-topline-green"  id="modalBill" >
	<div class="card-head card-head-icon" >
        <header><i class="ti-money"></i> Billing</header>
        <div class="tools">
			<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;" onclick="collsapseCardBody(this)"></a>
      	</div> 
    </div>
    <div class="borderBox shadow-none border-0 light card-box py-0 px-2 mb-3 tabsBorderCustom " style="border-radius:10px;overflow:hidden;height: 50px;margin-bottom: 10px;">
		<div class="borderBox-title tabbable-line mb-0 "  id="tabBilling" style="min-height: 50px">
		     <ul class="nav nav-tabs-border float-left">
		     	 <li class="nav-item">
		             <a href="#paymentAccount" id="paymentTab" data-toggle="tab" class="text-uppercase border-success active" >  Payment Account </a>
		         </li>
		         <li class="nav-item">
		             <a href="#historyBilling" id="historyBillTab" data-toggle="tab" class="text-uppercase border-success " > History Billing </a>
		         </li>     
		     </ul>
		</div> 
	</div>
	<div class="col-md-12 col-ms-12 tab-content">
		<div class="tab-pane active show" id="paymentAccount">
			<div class="col-md-10 col-ms-12 mx-auto px-0"><p class="title-bill">My Creadit Cards</p></div>
			<div class="row">
				<div class="col-md-7 col-ms-12 ml-auto d-block">
					<div class="show-credit-card">
						<div class="list-credit-card">
						</div>
						<div class="input-group mb-3 btn-add-credit-card">
						    <div class=" radio radio-green input-group-text w-100">
						    	<input type="radio" id="newCredit" name="creditCard" value="newCredit">
						    	<label class="form-control border-0 text-left mb-0" for="newCredit">Add a New Credit Card </label>
						    </div>
					   	</div>
					</div>
					<form class="form-horizontal " id="detailPaymentAccount" action="javascript:void(0)" accept-charset="UTF-8" method="post">
						<div class="tab-pane active show" id="paymentAccount">
							<div class="new-credit-card" id= "newCreditCard" style="display:none">
								<div class="mb-3  col-md-12" >
									<div class="row item form-group bad ">
									 	<div class="input-icon right w-100"><i class="fa yourName" data-toggle="tooltip"></i>
											<input value="" id="yourName" class="form-control rounded" placeholder="Your Name" name="yourName">
										</div>
									</div>
									<div  class="row item form-group bad ">
										<div class="input-icon right w-100"><i class="fa billAddress" data-toggle="tooltip"></i>
											<input value="" id="billAddress" class="form-control rounded" placeholder="Bill Address" name="billAddress">
										</div>
									</div>
									<div class="row">
										<div class="col-md-4 col-ms-12 pl-0 item form-group bad">
											<div class="input-icon right "><i class="fa billCity" data-toggle="tooltip"></i>
												<input class="form-control rounded"  id="billCity" placeholder="Bill City" name="billCity">
											</div> 
										</div>
										<div class="col-md-4 col-ms-12  item form-group bad">
											<div class="input-icon right" ><i class="fa billPostalCode" data-toggle="tooltip"></i>
												<input class="form-control rounded"  id="billPostalCode" placeholder="Bill Postal Code " name="billPostalCode"> 
											</div>
										</div>
										<div class="col-md-4 col-ms-12 pr-0 item form-group bad">
											<div class="">
								            	<select name="billCountry" id="billCountry" class="form-control rounded">
								            		<option value="">Billing Country/ Region</option>
							                        <option label="Afghanistan" value="AF">Afghanistan</option>
							                        <option label="Aland Islands" value="AX">Aland Islands</option>
							                        <option label="Albania" value="AL">Albania</option>
							                        <option label="Algeria" value="DZ">Algeria</option>
							                        <option label="Andorra" value="AD">Andorra</option>
							                        <option label="Angola" value="AO">Angola</option>
							                        <option label="Anguilla" value="AI">Anguilla</option>
							                        <option label="Antarctica" value="AQ">Antarctica</option>
							                        <option label="Antigua and Barbuda" value="AG">Antigua and Barbuda</option>
							                        <option label="Argentina" value="AR">Argentina</option>
							                        <option label="Armenia" value="AM">Armenia</option>
							                        <option label="Aruba" value="AW">Aruba</option>
							                        <option label="Australia" value="AU">Australia</option>
							                        <option label="Austria" value="AT">Austria</option>
							                        <option label="Azerbaijan" value="AZ">Azerbaijan</option>
							                        <option label="Bahamas" value="BS">Bahamas</option>
							                        <option label="Bahrain" value="BH">Bahrain</option>
							                        <option label="Bangladesh" value="BD">Bangladesh</option>
							                        <option label="Barbados" value="BB">Barbados</option>
							                        <option label="Belarus" value="BY">Belarus</option>
							                        <option label="Belgium" value="BE">Belgium</option>
							                        <option label="Belize" value="BZ">Belize</option>
							                        <option label="Benin" value="BJ">Benin</option>
							                        <option label="Bermuda" value="BM">Bermuda</option>
							                        <option label="Bhutan" value="BT">Bhutan</option>
							                        <option label="Bolivia, Plurinational State of" value="BO">Bolivia, Plurinational State of</option>
							                        <option label="Bonaire, Sint Eustatius and Saba" value="BQ">Bonaire, Sint Eustatius and Saba</option>
							                        <option label="Bosnia and Herzegovina" value="BA">Bosnia and Herzegovina</option>
							                        <option label="Botswana" value="BW">Botswana</option>
							                        <option label="Bouvet Island" value="BV">Bouvet Island</option>
							                        <option label="Brazil" value="BR">Brazil</option>
							                        <option label="British Indian Ocean Territory" value="IO">British Indian Ocean Territory</option>
							                        <option label="Brunei Darussalam" value="BN">Brunei Darussalam
							                        </option>
							                        <option label="Bulgaria" value="BG">Bulgaria</option>
							                        <option label="Burkina Faso" value="BF">Burkina Faso</option>
							                        <option label="Burundi" value="BI">Burundi</option>
							                        <option label="Cambodia" value="KH">Cambodia</option>
							                        <option label="Cameroon" value="CM">Cameroon</option>
							                        <option label="Canada" value="CA">Canada</option>
							                        <option label="Cape Verde" value="CV">Cape Verde</option>
							                        <option label="Cayman Islands" value="KY">Cayman Islands</option>
							                        <option label="Central African Republic" value="CF">Central African Republic</option>
							                        <option label="Chad" value="TD">Chad</option>
							                        <option label="Chile" value="CL">Chile</option>
							                        <option label="China" value="CN">China</option>
							                        <option label="Chinese Taipei" value="TW">Chinese Taipei</option>
							                        <option label="Christmas Island" value="CX">Christmas Island
							                        </option>
							                        <option label="Cocos (Keeling) Islands" value="CC">Cocos (Keeling) Islands</option>
							                        <option label="Colombia" value="CO">Colombia</option>
							                        <option label="Comoros" value="KM">Comoros</option>
							                        <option label="Congo" value="CG">Congo</option>
							                        <option label="Congo, the Democratic Republic of the"  value="CD"> Congo, the Democratic Republic of the</option>
							                        <option label="Cook Islands" value="CK">Cook Islands</option>
							                        <option label="Costa Rica" value="CR">Costa Rica</option>
							                        <option label="Cote d'Ivoire" value="CI">Cote d'Ivoire</option>
							                        <option label="Croatia" value="HR">Croatia</option>
							                        <option label="Cuba" value="CU">Cuba</option>
							                        <option label="Curaçao" value="CW">Curaçao</option>
							                        <option label="Cyprus" value="CY">Cyprus</option>
							                        <option label="Czech Republic" value="CZ">Czech Republic</option>
							                        <option label="Denmark" value="DK">Denmark</option>
							                        <option label="Djibouti" value="DJ">Djibouti</option>
							                        <option label="Dominica" value="DM">Dominica</option>
							                        <option label="Dominican Republic" value="DO">Dominican Republic
							                        </option>
							                        <option label="Ecuador" value="EC">Ecuador</option>
							                        <option label="Egypt" value="EG">Egypt</option>
							                        <option label="El Salvador" value="SV">El Salvador</option>
							                        <option label="Equatorial Guinea" value="GQ">Equatorial Guinea
							                        </option>
							                        <option label="Eritrea" value="ER">Eritrea</option>
							                        <option label="Estonia" value="EE">Estonia</option>
							                        <option label="Ethiopia" value="ET">Ethiopia</option>
							                        <option label="Falkland Islands (Malvinas)" value="FK">Falkland Islands (Malvinas)</option>
							                        <option label="Faroe Islands" value="FO">Faroe Islands</option>
							                        <option label="Fiji" value="FJ">Fiji</option>
							                        <option label="Finland" value="FI">Finland</option>
							                        <option label="France" value="FR">France</option>
							                        <option label="French Guiana" value="GF">French Guiana</option>
							                        <option label="French Polynesia" value="PF">French Polynesia
							                        </option>
							                        <option label="French Southern Territories" value="TF">French Southern Territories</option>
							                        <option label="Gabon" value="GA">Gabon</option>
							                        <option label="Gambia" value="GM">Gambia</option>
							                        <option label="Georgia" value="GE">Georgia</option>
							                        <option label="Germany" value="DE">Germany</option>
							                        <option label="Ghana" value="GH">Ghana</option>
							                        <option label="Gibraltar" value="GI">Gibraltar</option>
							                        <option label="Greece" value="GR">Greece</option>
							                        <option label="Greenland" value="GL">Greenland</option>
							                        <option label="Grenada" value="GD">Grenada</option>
							                        <option label="Guadeloupe" value="GP">Guadeloupe</option>
							                        <option label="Guam" value="GU">Guam</option>
							                        <option label="Guatemala" value="GT">Guatemala</option>
							                        <option label="Guernsey" value="GG">Guernsey</option>
							                        <option label="Guinea" value="GN">Guinea</option>
							                        <option label="Guinea-Bissau" value="GW">Guinea-Bissau</option>
							                        <option label="Guyana" value="GY">Guyana</option>
							                        <option label="Haiti" value="HT">Haiti</option>
							                        <option label="Heard Island and McDonald Islands" value="HM">Heard Island and McDonald Islands</option>
							                        <option label="Holy See (Vatican City State)" value="VA">Holy See (Vatican City State)</option>
							                        <option label="Honduras" value="HN">Honduras</option>
							                        <option label="Hungary" value="HU">Hungary</option>
							                        <option label="Iceland" value="IS">Iceland</option>
							                        <option label="India" value="IN">India</option>
							                        <option label="Indonesia" value="ID">Indonesia</option>
							                        <option label="Iran, Islamic Republic of" value="IR">Iran, Islamic Republic of</option>
							                        <option label="Iraq" value="IQ">Iraq</option>
							                        <option label="Ireland" value="IE">Ireland</option>
							                        <option label="Isle of Man" value="IM">Isle of Man</option>
							                        <option label="Israel" value="IL">Israel</option>
							                        <option label="Italy" value="IT">Italy</option>
							                        <option label="Jamaica" value="JM">Jamaica</option>
							                        <option label="Japan" value="JP">Japan</option>
							                        <option label="Jersey" value="JE">Jersey</option>
							                        <option label="Jordan" value="JO">Jordan</option>
							                        <option label="Kazakhstan" value="KZ">Kazakhstan</option>
							                        <option label="Kenya" value="KE">Kenya</option>
							                        <option label="Kiribati" value="KI">Kiribati</option>
							                        <option label="Korea" value="KP">Korea</option>
							                        <option label="Kuwait" value="KW">Kuwait</option>
							                        <option label="Kyrgyzstan" value="KG">Kyrgyzstan</option>
							                        <option label="Lao People's Democratic Republic" value="LA">Lao People's Democratic Republic</option>
							                        <option label="Latvia" value="LV">Latvia</option>
							                        <option label="Lebanon" value="LB">Lebanon</option>
							                        <option label="Lesotho" value="LS">Lesotho</option>
							                        <option label="Liberia" value="LR">Liberia</option>
							                        <option label="Libyan Arab Jamahiriya" value="LY">Libyan Arab Jamahiriya</option>
							                        <option label="Liechtenstein" value="LI">Liechtenstein</option>
							                        <option label="Lithuania" value="LT">Lithuania</option>
							                        <option label="Luxembourg" value="LU">Luxembourg</option>
							                        <option label="Macao" value="MO">Macao</option>
							                        <option label="Macedonia, the former Yugoslav Republic of" value="MK">Macedonia, the former Yugoslav Republic of</option>
							                        <option label="Madagascar" value="MG">Madagascar</option>
							                        <option label="Malawi" value="MW">Malawi</option>
							                        <option label="Malaysia" value="MY">Malaysia</option>
							                        <option label="Maldives" value="MV">Maldives</option>
							                        <option label="Mali" value="ML">Mali</option>
							                        <option label="Malta" value="MT">Malta</option>
							                        <option label="Martinique" value="MQ">Martinique</option>
							                        <option label="Mauritania" value="MR">Mauritania</option>
							                        <option label="Mauritius" value="MU">Mauritius</option>
							                        <option label="Mayotte" value="YT">Mayotte</option>
							                        <option label="Mexico" value="MX">Mexico</option>
							                        <option label="Moldova, Republic of" value="MD">Moldova, Republic of
							                        </option>
							                        <option label="Monaco" value="MC">Monaco</option>
							                        <option label="Mongolia" value="MN">Mongolia</option>
							                        <option label="Montenegro" value="ME">Montenegro</option>
							                        <option label="Montserrat" value="MS">Montserrat</option>
							                        <option label="Morocco" value="MA">Morocco</option>
							                        <option label="Mozambique" value="MZ">Mozambique</option>
							                        <option label="Myanmar" value="MM">Myanmar</option>
							                        <option label="Namibia" value="NA">Namibia</option>
							                        <option label="Nauru" value="NR">Nauru</option>
							                        <option label="Nepal" value="NP">Nepal</option>
							                        <option label="Netherlands" value="NL">Netherlands</option>
							                        <option label="New Caledonia" value="NC">New Caledonia</option>
							                        <option label="New Zealand" value="NZ">New Zealand</option>
							                        <option label="Nicaragua" value="NI">Nicaragua</option>
							                        <option label="Niger" value="NE">Niger</option>
							                        <option label="Nigeria" value="NG">Nigeria</option>
							                        <option label="Niue" value="NU">Niue</option>
							                        <option label="Norfolk Island" value="NF">Norfolk Island</option>
							                        <option label="Norway" value="NO">Norway</option>
							                        <option label="Oman" value="OM">Oman</option>
							                        <option label="Pakistan" value="PK">Pakistan</option>
							                        <option label="Palestinian Territory, Occupied" value="PS"> Palestinian Territory, Occupied</option>
							                        <option label="Panama" value="PA">Panama</option>
							                        <option label="Papua New Guinea" value="PG">Papua New Guinea
							                        </option>
							                        <option label="Paraguay" value="PY">Paraguay</option>
							                        <option label="Peru" value="PE">Peru</option>
							                        <option label="Philippines" value="PH">Philippines</option>
							                        <option label="Pitcairn" value="PN">Pitcairn</option>
							                        <option label="Poland" value="PL">Poland</option>
							                        <option label="Portugal" value="PT">Portugal</option>
							                        <option label="Puerto Rico" value="PR">Puerto Rico</option>
							                        <option label="Qatar" value="QA">Qatar</option>
							                        <option label="Reunion" value="RE">Reunion</option>
							                        <option label="Romania" value="RO">Romania</option>
							                        <option label="Russian Federation" value="RU">Russian Federation
							                        </option>
							                        <option label="Rwanda" value="RW">Rwanda</option>
							                        <option label="Saint Barthélemy" value="BL">Saint Barthélemy
							                        </option>
							                        <option label="Saint Helena, Ascension and Tristan da Cunha" value="SH"
							                           >Saint Helena, Ascension and Tristan da Cunha</option>
							                        <option label="Saint Kitts and Nevis" value="KN">Saint Kitts and Nevis</option>
							                        <option label="Saint Lucia" value="LC">Saint Lucia</option>
							                        <option label="Saint Maarten (Dutch part)" value="SX">Saint Maarten (Dutch part)</option>
							                        <option label="Saint Martin (French part)" value="MF">Saint Martin (French part)</option>
							                        <option label="Saint Pierre and Miquelon" value="PM">Saint Pierre and Miquelon</option>
							                        <option label="Saint Vincent and the Grenadines" value="VC">Saint Vincent and the Grenadines</option>
							                        <option label="Samoa" value="WS">Samoa</option>
							                        <option label="San Marino" value="SM">San Marino</option>
							                        <option label="Sao Tome and Principe" value="ST">Sao Tome and Principe</option>
							                        <option label="Saudi Arabia" value="SA">Saudi Arabia</option>
							                        <option label="Senegal" value="SN">Senegal</option>
							                        <option label="Serbia" value="RS">Serbia</option>
							                        <option label="Seychelles" value="SC">Seychelles</option>
							                        <option label="Sierra Leone" value="SL">Sierra Leone</option>
							                        <option label="Singapore" value="SG">Singapore</option>
							                        <option label="Slovakia" value="SK">Slovakia</option>
							                        <option label="Slovenia" value="SI">Slovenia</option>
							                        <option label="Solomon Islands" value="SB">Solomon Islands</option>
							                        <option label="Somalia" value="SO">Somalia</option>
							                        <option label="South Africa" value="ZA">South Africa</option>
							                        <option label="South Georgia and the South Sandwich Islands" value="GS"
							                           >South Georgia and the South Sandwich Islands</option>
							                        <option label="South Korea" value="KR">South Korea</option>
							                        <option label="South Sudan" value="SS">South Sudan</option>
							                        <option label="Spain" value="ES">Spain</option>
							                        <option label="Sri Lanka" value="LK">Sri Lanka</option>
							                        <option label="Sudan" value="SD">Sudan</option>
							                        <option label="Suriname" value="SR">Suriname</option>
							                        <option label="Svalbard and Jan Mayen" value="SJ">Svalbard and Jan Mayen</option>
							                        <option label="Swaziland" value="SZ">Swaziland</option>
							                        <option label="Sweden" value="SE">Sweden</option>
							                        <option label="Switzerland" value="CH">Switzerland</option>
							                        <option label="Syrian Arab Republic" value="SY">Syrian Arab Republic
							                        </option>
							                        <option label="Tajikistan" value="TJ">Tajikistan</option>
							                        <option label="Tanzania, United Republic of" value="TZ">Tanzania, United Republic of</option>
							                        <option label="Thailand" value="TH">Thailand</option>
							                        <option label="Timor-Leste" value="TL">Timor-Leste</option>
							                        <option label="Togo" value="TG">Togo</option>
							                        <option label="Tokelau" value="TK">Tokelau</option>
							                        <option label="Tonga" value="TO">Tonga</option>
							                        <option label="Trinidad and Tobago" value="TT">Trinidad and Tobago
							                        </option>
							                        <option label="Tunisia" value="TN">Tunisia</option>
							                        <option label="Turkey" value="TR">Turkey</option>
							                        <option label="Turkmenistan" value="TM">Turkmenistan</option>
							                        <option label="Turks and Caicos Islands" value="TC">Turks and Caicos Islands</option>
							                        <option label="Tuvalu" value="TV">Tuvalu</option>
							                        <option label="Uganda" value="UG">Uganda</option>
							                        <option label="Ukraine" value="UA">Ukraine</option>
							                        <option label="United Arab Emirates" value="AE">United Arab Emirates
							                        </option>
							                        <option label="United Kingdom" value="GB">United Kingdom</option>
							                        <option label="United States" value="US">United States</option>
							                        <option label="Uruguay" value="UY">Uruguay</option>
							                        <option label="Uzbekistan" value="UZ">Uzbekistan</option>
							                        <option label="Vanuatu" value="VU">Vanuatu</option>
							                        <option label="Venezuela, Bolivarian Republic of" value="VE">
							                            Venezuela, Bolivarian Republic of</option>
							                        <option label="Viet Nam" value="VN">Viet Nam</option>
							                        <option label="Virgin Islands, British" value="VG">Virgin Islands, British</option>
							                        <option label="Wallis and Futuna" value="WF">Wallis and Futuna
							                        </option>
							                        <option label="Western Sahara" value="EH">Western Sahara</option>
							                        <option label="Yemen" value="YE">Yemen</option>
							                        <option label="Zambia" value="ZM">Zambia</option>
							                        <option label="Zimbabwe" value="ZW">Zimbabwe</option>
								            	</select>
							        		</div>
							        	</div>
									</div>
								</div>
								<div class="mb-4">
									<p class="title-bill">Credit Card Details</p>
									<div class="border col-md-12 rounded">
										<div class="row mt-3">
											<div class="col-md-6 col-ms-12 item form-group bad">
												<div class=" input-icon right " id="validateCard"><i class="fa cardNumber" data-toggle="tooltip"></i>
													<input class="form-control rounded cardnumber" type="text"
		  												id="cardNumber" placeholder="Card Number" name="cardNumber" data-creditcard="true">
												</div> 
											</div>
											<div class="col-md-3 col-ms-12 item form-group bad">
												<div class="input-icon right" ><i class="fa dateCreate" data-toggle="tooltip"></i>
													<input class="form-control rounded to"  type="text" id="dateCreate" placeholder="MM/YYYY" name="dateCreate">
												</div>
											</div>
											<div class="col-md-3 col-ms-12 item form-group bad">
												<div class="input-icon right" ><i class="fa cvv" data-toggle="tooltip"></i>
													<input class="form-control rounded"  type= number id="cvv"  placeholder="CVV" name="cvv">
												</div>
											</div>
										</div>
										<p class="error_type_card" style = "display:none ; color:red"> Invalid card number </p>
										<div class="col-md-12 form-row pl-1 card-brand">
											<span>Credit Card brands we support: </span>
											<div class="ml-3">
												<img class="rounded " src="${context}/assets/img/visa.png">
												<img class="rounded " src="${context}/assets/img/mastercard.png">
												<img class="rounded " src="${context}/assets/img/americanexpress.png">
												<img class="rounded " src="${context}/assets/img/discover.png">
												<img class="rounded " src="${context}/assets/img/jcb.png">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="mb-3">
						<p class="title-bill">Choose Payment Amount</p>
						<div class="col-md-12 mb-3 px-0">
							<div class="row price-card ">
						 	</div>
						</div>
						<div class="checkbox checkbox-green pl-0 confirm-pay mb-4" style="display:none">
		                   	<input type="checkbox" class="flat" id="confirmPay">
		                   	<label  for="confirmPay">I just want to link my credit card -$0.00 deposit</label>
		                </div>
		                <div class="btn-payment btnCreateCard"><button type="submit" class="btn mdl-button bottest-button btn-success mb-3" onclick="payments()" id="createCard" >Payment</button></div>
					</div>
				</div>
				<div class="col-md-3 col-ms-12 mr-auto" >
					<div class="detail-user-payment">
						<p class="title-bill text-center">Detail User</p>
						<div class="inforUser">
							<div class="w-100  mb-2"><span>Name user: </span> <span class="ml-2" id="infoNameUser"> </span></div>
							<div class="w-100  mb-2"><span >Account Type: </span> <span class="ml-2" id="infoAccountType"> </span></div>
							<div class="w-100  mb-2"><span >Expire Date: </span> <span class="ml-2" id="infoExpireDate"> </span></div>
							<div class="w-100  mb-2"><span >Limited Project: </span> <span class="ml-2" id="infoLimitedProject"> </span></div>
						</div>
					</div>
				</div>
			</div>	
		</div>
		<div class="tab-pane" id="historyBilling">
			<div class="row col-md-9 col-ms-12 mx-auto">
				<div class="col-md-4 col-ms-12 text-center mr-auto">
					<h2 class="text-danger " id="expireDateHis"></h2>
					<p>Expire Date</p>
				</div>
				<div class="col-md-4 col-ms-12 text-center ml-auto">
					<h2 class="text-warning" id="limitedProjectHis"></h2>
					<p>Limited Project</p>
				</div>
			</div>
			<div class="col-md-11 col-ms-12 mx-auto mt-5">
				<table class="display nowrap w-100" id="billingHistory">
	                    <thead></thead>
						<tbody></tbody>
            	   </table>
			</div>
		</div>
	</div>
</div>
<script>
var username = "${userDetails.getUsername()}";
var listCreditCard = ${listCreditCard};
var context = '${context}';
var listAccountPrice = ${listAccountPrice};
var bottestUserinf = ${bottestUser};
	bottestUserinf["expireDate"] = Date.parse(bottestUserinf["expireDate"]);
$( "body" ).removeClass("sidemenu-closed");
$(window).on('load', function(){ 
	$(".preloader").fadeOut("slow");
	$(".preloader").css("z-index"," 1040");
});

$(function(){
	 var hash = window.location.hash;
	 hash && $('ul.nav a[href="' + hash + '"]').tab('show');
	 $('.nav-tabs a').click(function (e) {
	   $(this).tab('show');
	   var scrollmem = $('body').scrollTop();
	   window.location.hash = this.hash;
	   $('html,body').scrollTop(scrollmem);
	 });
});
</script>
<script src="${context}/assets/js/management/bill.js"></script>

