<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="modalAddNewLayout" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0">Add New Layout</h4>
				<button type="button" class="close" data-dismiss="modal" onclick="resetFormLayout()" >×</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal form-label-left form-addNew-common" id="new_layout" action="javascript:void(0)" accept-charset="UTF-8" method="post">
				    <div class="row item form-group">
				    	<label class="control-label col-md-4 col-sm-12 col-xs-4 text-right" for="name">
				        	Testsuite<span class="required">*</span>
				        </label>
				        <div class="col-md-6 col-sm-6 col-xs-12">
					        <div class="input-icon right"><i class="fa layoutName"></i>
					        	<select id="lsTestsuite" class="form-control" name="tsList"></select>
					        </div>
				        	
					    </div>
					</div>
				    <div class="row item form-group bad newNamelayout">
				        <label class="control-label col-md-4 col-sm-12 col-xs-4 text-right" for="name">
				        	Layout Name<span class="required">*</span>
				        </label>
				        <div class="col-md-6 col-sm-6 col-xs-12">
				         	<div class="input-icon right"><i class="fa layoutName"></i>
				            	<input class="form-control" placeholder="" required="required" type="text" name="layoutName" id="layout_name" >
				        	</div>
				        </div>
				    </div>
				    <div class="row item form-group">
				        <label class="control-label col-md-4 col-sm-12 col-xs-4 text-right" for="name">
				        	Description<span class="required">*</span>
				        </label>
				        <div class="col-md-6 col-sm-6 col-xs-12">
				        	<div class="input-icon right"><i class="fa descriptionLayout" data-toggle="tooltip" ></i>
				            	<textarea class="form-control" placeholder="" required="required" rows="3" name="layoutDescription" id="layout_description"></textarea>
				        	</div>
				        </div>
				    </div>
				    <div class="row form-group justify-content-center">
				        <button type="button" class="btn bottest-button btn-success mr-2" onclick = "addNewlayout();" id="saveNewLayout"><i class="ti-check mr-2"></i> Save</button>
				        <button type="button" class="btn bottest-button btn-secondary" onclick="resetFormLayout()" data-dismiss="modal"><i class="ti-close mr-2"></i> Close</button>
				    </div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	$('#modalAddNewLayout').on('show.bs.modal', function() {
		$("#lsTestsuite").empty();
		getListTestsuite("false", function(testsuitels){
				
			var htmlOptions = "";
			$.each( testsuitels, function(index, valuesItemTestsuite) { 
				htmlOptions += '<option value="'+valuesItemTestsuite["testsuite"]+'">'+valuesItemTestsuite["testsuite"]+'</option>';
				
			});
			$("#lsTestsuite").append(htmlOptions);
			
		});
	});
</script>