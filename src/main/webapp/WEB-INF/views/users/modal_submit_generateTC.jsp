<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<div id="modalGenerateTestcase" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="preloader loading-important">
	    <div class="loader" style="top:45%">
	        <div class="loader__figure"></div>
	        <p class="loader__label text-center">Please waiting!<br> Bottest.io</p>
	    </div>
	</div>
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0">Generate TestCase</h4>
				<button type="button" class="close" data-dismiss="modal" onclick="resetFormGenerate()">×</button>
			</div>
			<div class="modal-body position-relative">
				
								
				<form class="form-horizontal form-label-left form-addNew-common" id="generate_Testcase" action="javascript:void(0)" accept-charset="UTF-8" method="post">
				    <!-- <div class="row item form-group bad newNameUTC">
				        <label class="control-label col-md-4 col-sm-12 col-xs-4" for="name">
				        	Testcase Prefix Name<span class="required">*</span>
				        </label>
				        <div class="col-md-6 col-sm-6 col-xs-12">
					        <div class="input-icon right"><i class="fa generateTCName"></i>
					            <input class="form-control" placeholder="" required="required" type="text" name="testcaseName" id="generateTC_name" title="" alt="">
					        </div>
					    </div>
				    </div> -->
				    <div class="row item form-group bad">
				        <label class="control-label col-md-4 col-sm-12 col-xs-4 text-right" for="name">
				        	URL Test<span class="required">*</span>
				        </label>
				        <div class="col-md-6 col-sm-6 col-xs-12">
					        <div class="input-icon right"><i class="fa generateTCUrlTest" data-toggle="tooltip" ></i>
					            <input class="form-control" placeholder="" required="required" type="text" name="urlTest" id="urlTest" title="" alt="">
					        </div>
					    </div>
				    </div>
                    <div class="row item form-group bad">
                        <label class="control-label col-md-4 col-sm-12 col-xs-4 text-right">
                            Check Data Control<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        	<div class="input-icon right"><i class="fa" data-toggle="tooltip"></i>
	                            <select name="type" id="typeSelect" class="form-control typeSelect" required = "required">
	                                <option value="">Please select</option>
	                            </select>
	                        </div>
                        </div>
                    </div>
                    <div class="row item form-group bad">
                        <label class="control-label col-md-4 col-sm-12 col-xs-4 text-right">
                            Milestone<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        	<div class="input-icon right"><i class="fa" data-toggle="tooltip"></i>
	                        	<select name="milestoneGenerate" id="milestoneGen" class="form-control" required = "required">
	                            </select>
                        	</div>
                            
                        </div>
                    </div>
                    <p style="color: red; text-align: center;" id="errorNotification"></p>
				    <div class="row form-group justify-content-center">
				        <button type="submit" class="btn bottest-button btn-success mr-2" id="btnGenerate" onclick="validateGenerateUTC(this);"><i class="ti-check"></i> Generate</button>
				        <button type="button" class="btn bottest-button btn-secondary" onclick="resetFormGenerate()" data-dismiss="modal"><i class="ti-close"></i> Close</button>
				    </div>
				</form>
			</div>
		</div>
	</div>
</div>

<script src="${context}/assets/js/management/layout.js"></script>
