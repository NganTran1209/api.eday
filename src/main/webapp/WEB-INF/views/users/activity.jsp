
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<div class="preloader loading-important">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting"></spring:message><br> Bottest.io</p>
    </div>
</div>
<jsp:include page="/WEB-INF/views/users/projectHeader.jsp"></jsp:include>
<div class="clearfix"></div>

<div class="row ">
	<div class="col-md-12 mb-2">
         <div class="card card-box card-topline-green">
         	<div class="card-body">
         		<div class="changeAddIssue" id="ModalActivity">
	         		<div class="card-head card-head-icon">
		             	<header class="headerIssue"><spring:message code="label.Activity"></spring:message></header>
		                 <div class="legend headerLe w-100 text-right mr-3" id="legendActivity"></div>
		            </div>
		            <div class="col-md-6  col-ms-12 row mt-3 ml-auto">	    
		             	<spring:message code="label.DateFrom" var = "dateFrom"></spring:message>    
		             	<spring:message code="label.DateTo" var = "dateTo"></spring:message>
		             	<spring:message code="label.Member" var = "member"></spring:message>
		            	<div class="col-md-4 col-ms-12 mb-3 " data-toggle="tooltip" data-placement="top" data-original-title="${dateFrom}"><input type="text" value="" class="form-control colorPlace" id="activityDateFrom" placeholder="${dateFrom}"></div>
		                <div class="col-md-4 col-ms-12 mb-3" data-toggle="tooltip" data-placement="top" data-original-title="${dateTo}"><input type="text"  value="" class="form-control colorPlace" id="activityDateTo" placeholder="${dateTo}" ></div>
			         	<div class="col-md-4 col-ms-12 mb-3" data-toggle="tooltip" data-placement="top" data-original-title="${member}"><select class="form-control" multiple="multiple" id="activityMember"></select></div>
			         </div>    
		             <div class="card-body no-padding height-9 mx-5" id="listActivity">
		             	
		            </div>
	           </div>
	           <jsp:include page="/WEB-INF/views/users/modal_add_new_issue.jsp"></jsp:include>
         	</div>
        </div>
    </div>
</div>

<script src="${context}/assets/js/management/tasks.js" ></script>
<script src="${context}/assets/js/management/activity.js"></script>
<script>
	var classTab = $(".text-uppercase.border-success");
	$.each(classTab, function(index, values){
	    if($(values).hasClass("active show") == true){
	        $(values).removeClass("active show");
	    }
	});
	$("#callActivityTab").addClass("active show");
	$(function(){
		var hash = window.location.hash;
		hash && $('ul.nav a[href="' + hash + '"]').tab('show');
		$('.nav-tabs a').click(function (e) {
			$(this).tab('show');
			var scrollmem = $('body').scrollTop();
			window.location.hash = this.hash;
			$('html,body').scrollTop(scrollmem);
		});
	});
	var projectId = '${projectId}';
 	var taskList = ${taskList};
 	var listBug = ${listBug};
  	var checkSession = true;
	var dateFromSS = '${dateFromSS}';
  	var dateToSS = '${dateToSS}';
  	var memberSS = '${memberSS}';
  	var checkSessionExist = ${checkSessionExist};
  	var memberParse = "";
 	if(memberSS != ""){
 		memberParse = JSON.parse(memberSS);
  	}
 	
 	if(checkSessionExist != true){
 		memberParse = [];
 	}
 	drawActivityFilter(dateFromSS, dateToSS, memberParse);
 	function duplicateUser(arr){
		var duplicateIds = [];
		var valueDupl = arr
	    .map(function(e){e['lastname'] + e['firstname']})
	    .map(function(e, i, final){ final.indexOf(e) !== i && i})
	    .filter(function(obj){ arr[obj]})
	    .map(function(e){ arr[e]['lastname'] + arr[e]['firstname']});
		 $.each(valueDupl, function(index, value){
			 $.each(arr, function(indexArr, valueArr){
				 var name = valueArr["lastname"] + valueArr["firstname"]; 
				 if(name == value){
					 duplicateIds.push(valueArr);
				 }
			 })
		 })
		 
		 return duplicateIds ;
	}
 	 
	 getUserProject(function(userProjectList){
		var duplicateUserTask = duplicateUser(userProjectList);
		var memberValue = [];
		for(var i = 0 ; i < userProjectList.length ; i++){
			if(userProjectList[i]["lastname"] != null && userProjectList[i]["firstname"] != null){
				var count = 0 ;
				if(duplicateUserTask.length > 0){
					$.each(duplicateUserTask, function(index,value){
						if(value.username == userProjectList[i]["username"]){
							memberValue.push({"id":userProjectList[i]["username"],"text":userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"] +"("+userProjectList[i]["username"]+")","title":userProjectList[i]["username"]});
							count ++;
						}					
					})
				} 
				
				if(count == 0 ){
					memberValue.push({"id":userProjectList[i]["username"],"text":userProjectList[i]["lastname"]+" "+userProjectList[i]["firstname"],"title":userProjectList[i]["username"]});
				}
			}
		}
		
		$("#activityMember").select2({
			placeholder: getLang(currentLocale,"Select Member"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			data: memberValue,
			tags: true,
			dropdownParent: "",
			theme: 'bootstrap'
		}); 
		$("#activityMember").val(memberParse);
		$("#activityMember").trigger('change');
	});
	$('#activityDateFrom').bootstrapMaterialDatePicker
	({
		format: 'YYYY-MM-DD',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});
	
	$('#activityDateTo').bootstrapMaterialDatePicker
	({
		format: 'YYYY-MM-DD',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
	});
	$("#activityDateFrom").val(dateFromSS);
	$("#activityDateTo").val(dateToSS);
</script>


