<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
var projectId="${projectId}";
</script>
<div class="modal fade bs-example-modal-sm" id="modalRun" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static">
 	<div class="modal-dialog modal-lg" style="min-width:64%">
 	<form name="runTestCases" id="runTestCases" >
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0">Run Test</h4>
				<button type="button" class="close" data-dismiss="modal" onclick="resetFormRunTestsuite()">×</button>
			</div>
			<div class="modal-body" id="modalBody">
			    <div class="row">
					<div class="col-md-2 col-xs-12">
						<h5>Project Name</h5>
					</div>
					<div class="col-md-4 col-xs-12 form-group">
						<p id="projectIdInModal">${currentProject.getName()}</p>
					</div>
					<div class="col-md-2 col-xs-12 form-group">
						<h5>Milestone Name<span class="text-danger">*</span></h5>
					</div>
					<div class="col-md-4 col-xs-12">
						<div class="input-icon right"><i class="fa excuteName"></i>
							<select id="executeName" class="form-control" name="executeName">
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2 col-xs-12">
						<h5>OS</h5>
					</div>
					<div class="col-md-4 col-xs-12 form-group">
						<select id="slOs" class="form-control" name="runningOS">
							<c:forEach items="${lsOs}" var="item">
							    <c:choose>
									<c:when test="${item.defaultC eq  true }">
								    	<option value="${item.itemValue}" selected>${item.itemName}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.itemValue}">${item.itemName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
					<div class="col-md-2 col-xs-12 form-group">
						<h5>Run Location</h5>
					</div>
					<div class="col-md-4 col-xs-12">
						<select id="sladdress" class="form-control" name="runningLocation">
							<c:forEach items="${lsLocation}" var="item">
							    <c:choose>
									<c:when test="${item.defaultC eq  true }">
								    	<option value="${item.itemValue}" selected>${item.itemName}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.itemValue}">${item.itemName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2 col-xs-12 form-group">
						<h5>Browser</h5>
					</div>
					<div class="col-md-4 col-xs-12 form-group">
						<select id="slbrowser" class="form-control" name = "browser">
							<c:forEach items="${lsBrowser}" var="item">
							    <c:choose>
									<c:when test="${item.defaultC eq  true }">
								    	<option value="${item.itemValue}" selected>${item.itemName}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.itemValue}">${item.itemName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
					<div class="col-md-2 col-xs-12 form-group text-nowrap">
						<h5>Screen Resolution</h5>
					</div>
					<div class="col-md-4 col-xs-12 form-group">
						<select id="slscreen" class="form-control" name="screenRes">
							<c:forEach items="${lsResolution}" var="item">
							    <c:choose>
									<c:when test="${item.defaultC eq  true }">
								    	<option value="${item.itemValue}" selected>${item.itemName}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.itemValue}">${item.itemName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2 col-xs-12 form-group">
						<h5>Timeout(ms)</h5>
					</div>
					<div class="col-md-4 col-xs-12">
						<input class="form-control allownumericwithoutdecimal" type="text" id="txtTimeout" name="timeout" value="300">
					</div>
					<div class="col-md-2 col-xs-12">
						<h5>Select Devices</h5>
					</div>
					<div class="col-md-4 col-xs-12 ">
						<select id="devicesSelection" name ="deviceName"
							class="form-control" tabindex="1">
							<c:forEach items="${lsDevice}" var="item">
							    <c:choose>
									<c:when test="${item.defaultC eq  true }">
								    	<option value="${item.itemValue}" selected>${item.itemName}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.itemValue}">${item.itemName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2 col-xs-12 form-group">
						<h5>Mobile Mode</h5>
					</div>
					<div class="col-md-4 col-xs-12 form-group">
						<div class="checkbox checkbox-green form-check form-check-inline mr-2 pl-2">
							<input type="checkbox" name="mobileMode" id="mobileMode">
							<label for="mobileMode"></label>
						</div>
					</div>
					<div class="col-md-2 col-xs-12 form-group">
						<h5>Record Video</h5>
					</div>
					<div class="col-md-4 col-xs-12 form-group">
						<div class="checkbox checkbox-green form-check form-check-inline mr-2 pl-2">
							<input type="checkbox" name="isRecordVideo" id="videoMode">
							<label for="videoMode"></label>
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-2 col-xs-12">
						Testsuite Name
					</div>
					<div class="col-md-4 col-xs-12 ">
						<p id="testsuiteNameInModal"></p>
					</div>
					<div class="col-md-2 col-xs-12">
						<label class="title-lg-component m-0">Test Concurrent </label>
					</div>
					<div class="col-md-4 col-xs-12 ">
						<select id="testConcurrent" class="form-control" name="">
						</select>
					</div>			
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="row form-group" id="testcases">
							<div class="col-md-4 col-xs-12 mt-1">
								Select Testcases
							</div>
							<div class="col-md-8 col-xs-8 fix-top-checkbox-forminline">
								<div class="checkbox checkbox-green form-check form-check-inline w-100 mr-0 border border-bottom-0 py-2">
									<input type="checkbox" name="runAsTestcase" id="runAsTestcase">
									<label for="runAsTestcase" class="mb-0">Select all</label>
								</div>	
								<div id="lsTestcases" class="selectTestcaseCheckbox">
															
								</div>
								<div class="btn-group" style="width: 100%;display:none">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row item form-group bad">
							<div class="col-md-4 col-xs-4">
								<h5>Run Description</h5> 
							</div>
							<div class="col-md-8 col-xs-8">
								<div class="input-icon right"><i class="fa excuteName"></i>
									<textarea class="form-control" rows="7" id="runDescription" name="runDescription" placeholder="Description of software version or more information"></textarea>
								</div>
							</div>
						</div>		
					</div> 
				</div>
				<div class="row">
					<div class="col-md-6">
					    <div class="row form-group" id="runOnLocalRoot">
							<div class="col-md-4 col-xs-12">
								Run on local
							</div>
							<div class="col-md-1 col-xs-12 form-group">
								<div class="checkbox checkbox-green form-check form-check-inline mr-2 pl-2">
									<input type="checkbox" name="runOnLocal" id="runOnLocal">
									<label for="runOnLocal"></label>
									
								</div>
							</div>
							<div class="col-md-7 col-xs-12 form-group">
								<p style="color: red;padding-top: 3px;" id="messageCheck">Require connect on local</p>
							</div>
							
						</div>
					</div>
					<div class="col-md-6">
						<div class="row form-group">
							<div class="col-md-4 col-xs-4">
								<a href="https://bottest.io/category/doc#14-instructions-run-bottest-on-your-local-pc"><spring:message code="label.GuideWin"></spring:message></a>
							</div>
							<div class="col-md-8 col-xs-8">
								<a href="https://bottest.io/category/doc#15-instructions-run-bottest-on-your-mac-pc"><spring:message code="label.GuideMac"></spring:message></a>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<div class="modal-footer" id="modalFooter">
				<button type="submit" id="btnRun" onclick="validateRunTestCases();" class="btn bottest-button btn-success mr-2">
					<i class="ti-check"> </i>Execute
				</button>
				<button type="button" class="btn bottest-button btn-secondary"
					data-dismiss="modal" id="btnExit" onclick="resetFormRunTestsuite()">
					<i class="ti-close"> </i> Close
				</button>
			</div>
		</div>
		</form>
	</div>
</div>
<script src="${context}/assets/js/management/modal_run_testcases.js"></script>
<script>
/* 	$(".selectTestcaseCheckbox input[name=runAsSchedulerAll]").click(function(){
		$(".selectTestcaseCheckbox input[type=checkbox]").prop("checked",true); 
	}); */
	
	$( document ).ready(function() {
		hideModalRun();
	});
	
	$("#runAsScheduler").click(function(){
		if($("#runAsScheduler").prop("checked") == true){
			$("#selectTimeRun").show();
		}else{
			$("#selectTimeRun").hide();
		}
		
	});
	
	$(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
	    $(this).val($(this).val().replace(/[^\d].+/, ""));
	     if ((event.which < 48 || event.which > 57)) {
	         event.preventDefault();
	     }
	 });
	
</script>

