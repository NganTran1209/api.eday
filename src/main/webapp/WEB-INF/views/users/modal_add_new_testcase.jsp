<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" /> 

<div id="modalAddNewTestcase" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="max-width:900px">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0">New Testcase</h4>
				<button type="button" class="close" data-dismiss="modal" onclick="resetFormTestCase()" >×</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal form-label-left form-addNew-common" id="new_testcase" action="javascript:void(0)" accept-charset="UTF-8" method="post">
			        <div class="col-md-12">
				        <div class="row mb-3">
				            <div class="col-md-6">
				                <div class="row form-group mb-0">
				                    <div class="col-md-4"><label>Testsuite <span class="required">*</span></label></div>
				                    <div class="col-md-8">
				                    	<div class="input-icon right">
					                    	<i class="fa" data-toggle="tooltip" ></i>
					                    	<select id="lsTestsuite" class="form-control" name="tsList"></select>
				                    	</div>
				                    </div>
				                </div>
				            </div>
				            <div class="col-md-6">
				                <div class="row form-group mb-0">
				                    <div class="col-md-4">
				                    	<label>
				                    		<spring:message code="label.Milestone"></spring:message>
				                    		 <span class="required">*</span>
			                    		</label>
		                    		</div>
				                    <div class="col-md-8">
				                    	<div class="input-icon right">
					                    	<i class="fa" data-toggle="tooltip" ></i>
					                    	<select id="milestoneTC" class="form-control" name="milestoneTC"></select>
				                    	</div>
				                    </div>
				                </div>
				            </div>
				        </div>
				        <div class="row mb-3">
				           <div class="col-md-6">
				                <div class="row">
				                    <div class="col-md-4"><label><spring:message code="label.Assignto"></spring:message><span class="required">*</span></label></div>
				                    <div class="col-md-8">
				                    	<select id="assignToTc" class="form-control" name="assignUser"></select>
				                    </div>
				                </div>
				            </div>
				             <div class="col-md-6">
				                <div class="row">
				                    <div class="col-md-4"><label><spring:message code="label.Status"></spring:message></label></div>
				                    <div class="col-md-8">
				                    	<select name="statustaskTc" id="statustaskTc" class="form-control"></select>
				                    </div>
				                </div>
				            </div>
				        </div>
				        <div class="row mb-3">
				            <div class="col-md-6">
				                <div class="row">
				                    <div class="col-md-4"><label><spring:message code="label.PlanStartDate"></spring:message></label></div>
				                    <div class="col-md-8">
				                    	<input type="text" id="planStDa" value="" class="form-control date" placeholder="mm/dd/yyyy"/>
				                    </div>
				                </div>
				            </div>
				            <div class="col-md-6">
				                <div class="row form-group">
				                    <div class="col-md-4"><label><spring:message code="label.PlanEndDate"></spring:message></label></div>
				                    <div class="col-md-8">
				                    	<div class="input-icon right">
					                    	<i class="fa" data-toggle="tooltip" ></i>
					                    	<input type="text" id="planEnDa" name="endDate" value="" class="form-control date" placeholder="mm/dd/yyyy"/>
					                    </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				        <div class="row mb-3">
				            <div class="col-md-6">
				                <div class="row">
				                    <div class="col-md-4"><label>Time</label></div>
				                    <div class="col-md-8">
				                    	<div class="row">
				                    		<div class="col-md-6">
				                    			<spring:message code="label.Enterestimatedtimes" var="enterestimatedtimes"></spring:message>
				                    			<spring:message code="label.EstimatedTimes" var="estimatedtimes"></spring:message>
				                    			<spring:message code="label.Enteractualtimes" var="enteractualtimes"></spring:message>
				                    			<spring:message code="label.ActualTimes" var="actualtimes"></spring:message>
				                    			<input id="estimatedTc" value="" class="form-control" placeholder="${enterestimatedtimes}" data-toggle="tooltip" data-placement="top" data-original-title="${estimatedtimes}"/>
				                    		</div>
				                    		<div class="col-md-6">
				                    			<input id="actualTimeTc" value="" class="form-control" placeholder="${enteractualtimes}" data-toggle="tooltip" data-placement="top" data-original-title="${actualtimes}"/>
				                    		</div>
				                    	</div>
				                    	
				                    </div>
				                </div>
				            </div>
				            <div class="col-md-6">
				                <div class="row">
				                    <div class="col-md-4"><label>Category</label></div>
				                    <div class="col-md-8">
				                    	<select name="categorytaskTc" id="categorytaskTc" class="form-control"></select>
				                    </div>
				                </div>
				            </div>
				        </div>
				         <div class="row mb-3">
				            <div class="col-md-12">
				                <div class="row mb-0 form-group">
				                    <div class="col-md-2"><label>Description<span class="required">*</span></label></div>
				                    <div class="col-md-10">
				                    	<div class="input-icon right">
					                    	<i class="fa" data-toggle="tooltip" ></i>
					                    	<textarea class="form-control" placeholder="" required="required" rows="4" name="testcaseDescription" id="testcase_description"></textarea>
				                    	</div>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>   
				    <input id="variableTestsuite" value="" type="hidden" name="variableTestsuite">
				    <input id="variableProject" value="${projectId}" type="hidden" name="variableTestsuite">
				    <input id="orderIdAuto" type="hidden">
				    <div class="row form-group justify-content-center">
				        <button type="submit" class="btn bottest-button btn-success mr-2" onclick="validateAddTestCase();" id="saveTestCase"><i class="ti-check mr-2"></i> Save</button>
				        <button type="button" class="btn bottest-button btn-secondary" onclick="resetFormTestCase()" data-dismiss="modal"><i class="ti-close mr-2"></i> Close</button>
				    </div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	$('#planStDa').bootstrapMaterialDatePicker
	({
		format: 'MM/DD/YYYY',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});
	$('#planEnDa').bootstrapMaterialDatePicker
	({
		format: 'MM/DD/YYYY',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});
</script>