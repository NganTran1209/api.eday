<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="modalCopyLink" class="modal fade" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			
			<div class="modal-body">
				<form id="copyLink" class="form-horizontal form-label-left form-addNew-common" action="javascript:void(0)" accept-charset="UTF-8" method="post">
					 <label id="titleFormCopy" style="font-size: large;"></label>
				     <div class="row item form-group py-4">
				    	
				        <div class="col-md-12 mx-auto">
				        	<input id="copyItem" class="form-control" style="border-radius: 7px">
					    </div>
					</div>
				    <div class="row justify-content-center">
				        <button type="submit" class="btn bottest-button btn-success mr-2 button-sm" onclick="copyLink();"><i class="icon-docs mr-2"></i> &nbsp;Copy</button>
				        <button type="button" class="btn bottest-button btn-secondary button-sm" onclick="resetFormCopy()" data-dismiss="modal"><i class="ti-close mr-2"></i> &nbsp;Cancel</button>
				    </div>
				</form>
			</div>
		</div>
	</div>
</div>
