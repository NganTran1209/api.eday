<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:include page="/WEB-INF/views/users/projectHeader.jsp"></jsp:include>
<div class="card card-box card-topline-green">
	<div class="card-body">
		<div class="w-100 changeAddIssue" id="detailTestcaseAuto">
			<div class="w-100 form-group">
				<div class="col-md-12 form-group">
					<div class="col-md-12 mb-5">
						<button class="btn bottest-button btn-secondary" onclick="window.location.href='${context}/project/${projectId}/testcases'">
							<i class="icon-arrow-left mr-2"></i> <spring:message code="label.Back"></spring:message>
						</button>
					</div>
					<div class="row">
						<div class="col-md-6 form-group">
							<div class="mb-3">
								<i class="fa fa-circle my-auto mr-2"></i>
							 	<span><spring:message code="label.TestcaseName"></spring:message>:</span> 
							 	<span id="titleTestcase" class="text-success font-weight-bold"></span>
							</div>
							<div>
								<span class="mb-3 d-flex" id="discription"> <i class="fa fa-circle my-auto mr-3"></i><spring:message code="label.Description"></spring:message>:</span>
				            	<textarea class="form-control" rows="8" id="testcaseDescription"></textarea>
							</div>
						</div>
						<div class="col-md-6 form-group">
							<div class="d-flex mb-2">
								<label class="text-nowrap label-control mr-2 my-auto col-md-4"><spring:message code="label.Status"></spring:message>: </label>
								<div class="col-md-8">
									<select class="form-control assignTo " id="statusIssueDetail">
										<option value=""></option>
									</select>
								</div>
							</div>
							<div class="d-flex mb-2">
								<label class="text-nowrap label-control mr-2 my-auto col-md-4"><spring:message code="label.AssignTo"></spring:message>: </label>
								<div class="col-md-8">
									<select class="form-control assignTo" id="filterAssignTos">
										<option value=""></option>
									</select>
								</div>
							</div>
							<div class="d-flex mb-2">
								<label class="text-nowrap label-control mr-2 my-auto col-md-4"><spring:message code="label.PlanStartDate"></spring:message>: </label>
								<div class="col-md-8">
									<input id="planStarDa" class="form-control date" value="" type="text" placeholder="mm/dd/yyyy"/>
								</div>
								
							</div>
							<div class="d-flex mb-2">
								<label class="text-nowrap label-control mr-2 my-auto col-md-4"><spring:message code="label.PlanEndDate"></spring:message>: </label>
								<div class="col-md-8">
									<input id="planEndDa" class="form-control date" value="" type="text" placeholder="mm/dd/yyyy"/>
									<p id="errorEndDate" style="color:red;" hidden></p>
								</div>
								
							</div>
							<div class="d-flex mb-2">
								<label class="text-nowrap label-control mr-2 my-auto col-md-4"><spring:message code="label.Time"></spring:message>: </label>
								<div class="col-md-8">
									<div class="row">
										<div class="col-md-6">
											<spring:message code="label.EstimatedTimes" var="estimatedTimes"></spring:message>
											<spring:message code="label.Enterestimatedtimes" var="enterestimatedtimes"></spring:message>
											<spring:message code="label.Enteractualtimes" var="enteractualtimes"></spring:message>
											<input id="estimatedTimeDetail" data-toggle="tooltip" data-placement="top" data-original-title="${estimatedTimes}" placeholder="${enterestimatedtimes}" class="form-control tooltip-primary" value=""/>
										</div>
										<spring:message code="label.ActualTimes" var="actualTimes"></spring:message>
										<div class="col-md-6">
											<input id="actualTimeDetail" data-toggle="tooltip" data-placement="top" data-original-title="${actualTimes}" placeholder="${enteractualtimes}" class="form-control tooltip-primary" value=""/>
										</div>
										
									</div>
								</div>
							</div>
							<div class="d-flex">
								<label class="text-nowrap label-control mr-2 my-auto col-md-4"><spring:message code="label.Category"></spring:message>: </label>
								<div class="col-md-8 mb-2" >
									<select class="form-control" id="categoryIssueDetail">
										<option value=""></option>
									</select>
								</div>
							</div>
							<div class="d-flex ">
								<label class="text-nowrap label-control mr-2 my-auto col-md-4"><spring:message code="label.Milestone"></spring:message>: </label>
								<div class="col-md-8">
									<select class="form-control" id="milestoneTCDetail">
										<option value=""></option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
			</div>
			<div class="position-relative w-100">
				
				<div class="preloader progress-loadMainContent" id="loadDetailTestCase">
				    <div class="loader">
				        <div class="loader__figure"></div>
				        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting"></spring:message><br> Bottest.io</p>
				    </div>
				</div>
				
				<div class="col-md-12 form-group pb-3">
					<ol id="sortable"></ol>
				
					<div class="col-md-12 col-sm-12 col-xs-12 form-group text-center">
						<button class="btn bottest-button btn-secondary" onclick="window.location.href='${context}/project/${projectId}/testcases'"><i class="icon-arrow-left mr-2"></i> <spring:message code="label.Back"></spring:message></button>
					    <button class="btn bottest-button btn-success" id="addStep"><i class="ti-plus mr-2"></i><spring:message code="label.AddStep"></spring:message></button>
					    <button class="btn bottest-button btn-success" id="saveStep" onclick="saveStepAuto()" data-loading-text="
					    	<i class='fa fa-spinner fa-spin '></i> Processing save ..."> <i class='ti-check mr-2'></i> <spring:message code="label.Save"></spring:message>
					    </button>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="/WEB-INF/views/users/modal_add_new_issue.jsp"></jsp:include>
	</div>
</div>
<script src="${context}/assets/js/management/testcase_detail.js"></script>
<script src="${context}/assets/js/management/tasks.js" ></script>
<script>
	$(function(){
		var hash = window.location.hash;
		hash && $('ul.nav a[href="' + hash + '"]').tab('show');
		$('.nav-tabs a').click(function (e) {
			$(this).tab('show');
			var scrollmem = $('body').scrollTop();
			window.location.hash = this.hash;
			$('html,body').scrollTop(scrollmem);
		});
	});
</script>

<script>
	var classTab = $(".text-uppercase.border-success");
	$.each(classTab, function(index, values){
	    if($(values).hasClass("active show") == true){
	        $(values).removeClass("active show");
	    }
	});
	
	function duplicateUser(arr){
		var duplicateIds = [];
		var valueDupl = arr
	    .map(e => e['lastname'] + e['firstname'])
	    .map((e, i, final) => final.indexOf(e) !== i && i)
	    .filter(obj => arr[obj])
	    .map(e => arr[e]['lastname'] + arr[e]['firstname']);
		 $.each(valueDupl, function(index, value){
			 $.each(arr, function(indexArr, valueArr){
				 var name = valueArr["lastname"] + valueArr["firstname"]; 
				 if(name == value){
					 duplicateIds.push(valueArr);
				 }
			 })
		 })
		 
		 return duplicateIds ;
	}
	
	$("#callAutomationTestcaseTab").addClass("active show");
	var context = '${context}';
	var testsuiteName = '${testsuiteName}';
	var testcaseName = '${testcaseName}';
	var projectId = ${projectId};
	var listActions = ${listActions};
	var actionHelp = ${actionHelp};
	var listRole = ${listPermisson};
 	var listPermisson =JSON.parse(listRole[0].role_detail);
	var lsAssignTo;
	var listBug="";
	var taskList = ${tasks};
	
	$('#planStarDa').bootstrapMaterialDatePicker
	({
		format: 'MM/DD/YYYY',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});
	$('#planEndDa').bootstrapMaterialDatePicker
	({
		format: 'MM/DD/YYYY',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});
	
	$('#planStarDa').bootstrapMaterialDatePicker({ weekStart : 0 }).on('change', function(e, date)
	{
		$('#planEndDa').bootstrapMaterialDatePicker('setMinDate', date);
	});
	
	getUserAssignTo("false", function(lsAssignTo){
		var duplicateUserAssign = duplicateUser(lsAssignTo); 
		var htmlOptions = "";
		$("#filterAssignTos").empty();
		$.each(lsAssignTo, function(ind,vals){
			var count = 0;
			if(duplicateUserAssign.length > 0){
				$.each(duplicateUserAssign, function(index,value){
					if(value.username == vals["username"]){
					 	htmlOptions += '<option value="'+vals["username"]+'">'+vals["lastname"]+" "+vals["firstname"]+'('+vals["username"]+')'+'</option>'; 
						count ++;
					}					
				})
			} 
			
			if(count == 0 ){
				htmlOptions += '<option value="'+vals["username"]+'">'+vals["lastname"]+" "+vals["firstname"]+'</option>';
			}
		});
		$("#filterAssignTos").append(htmlOptions);
	});
	
	var projectCo;
	infProjectCom(function(projectCo){
		$("#statusIssueDetail").empty();
		$("#categoryIssueDetail").empty(); 
		var html = "";
		var htmlCategory = "";
		for(var i = 0 ; i < projectCo.length; i++){
			if(projectCo[i]["groupCode"] == "Status"){
				html += '<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>';
			} else if(projectCo[i]["groupCode"] == "Category"){
				htmlCategory += '<option value = "'+ projectCo[i]["itemName"] +'">'+projectCo[i]["itemName"]+'</option>';
			}
		}
		$("#statusIssueDetail").append(html);
		$("#categoryIssueDetail").append(htmlCategory); 
	});
	
	getAllMilestoneInIssue(function(milestones){
		$("#milestoneTCDetail").empty();
		var htmlOptions = "";
		$.each(milestones,function(index, value){
			htmlOptions += '<option value="'+value.projectMilestoneIdentity["mileStoneName"]+'" >'+value.projectMilestoneIdentity["mileStoneName"]+'</option>';
		})
		$("#milestoneTCDetail").append(htmlOptions);
		getDetailTestcase(testsuiteName, testcaseName);
	})
	
	
</script>
<jsp:include page="/WEB-INF/views/users/modal_add_new_layout.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/users/modal_Detail_Layout.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/users/modal_action_API.jsp"></jsp:include>
<script src="${context}/assets/js/management/modal_action_api.js"></script>
