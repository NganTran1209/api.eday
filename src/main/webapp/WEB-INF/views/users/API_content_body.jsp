<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="w-100 bg-white py-3 selectTypeRequestBody d-flex">
    <div class="radio radio-green form-check-inline" id="bodyRequestNone">
        <input id="bodyRqNone" name="bodyRequest" type="radio" value="none" checked="checked">
        <label for="bodyRqNone" class="mb-0">none</label>
    </div>
    <div class="radio radio-green form-check-inline" id="bodyRequestFormData">
        <input id="bodyRqFormData" name="bodyRequest" value="form-data" type="radio">
        <label for="bodyRqFormData" class="mb-0">form-data</label>
    </div>
    <div class="radio radio-green form-check-inline" id="bodyRequestRaw">
        <input id="bodyRqRaw" name="bodyRequest" value="raw" type="radio">
        <label for="bodyRqRaw" class="mb-0">raw</label>
    </div>
    <div class="radio radio-green form-check-inline" id="bodyRequestBinary" style="display: none">
        <input id="bodyRqBinary" name="bodyRequest" value="binary" type="radio">
        <label for="bodyRqBinary" class="mb-0">binary</label>
    </div>

</div>
<div class="w-100 bodyTypeRequestBody">
	<div class="col-md-12 text-center text-muted mb-0 bodyOfTypeRequest py-3" id="requestBodyNone">
		This request does not have a body
	</div>
	<div class="w-100 bodyOfTypeRequest" id="requestFormData" style="display:none">
		<div class="title-content-action bg-white w-100 justify-content-end">
			<div class="box-add-row-action mx-3" style="text-align:right">
				<button class="btn bottest-button btn-success button-sm button-sm-custom mb-3" id="addRowMultiLine"><i class="ti-plus"></i></button>
			</div>
		</div>	
<!-- 	    <div class="box-add-row-action ml-auto mr-2">
		<button type="button" class="btn bottest-button btn-success button-sm" id="addRowMultiLine" style="display: none;"><i class="ti-plus mr-1"></i></button>
	</div> -->
		<table class="table table-bordered table-hover table-params m-0 mx-auto">
			<thead>
				<tr>
					<th width="6%" class="border-bottom-0 border-top-0"></th>
					<th width="25%" class="border-bottom-0 border-top-0">Key</th>
					<th width="25%" class="border-bottom-0 border-top-0">Value</th>
					<th width="19%" class="border-bottom-0 border-top-0"></th>
					<th width="25%" class="border-bottom-0 border-top-0">Description</th>
				</tr>
			</thead>
			<tbody>
<!-- 				<tr>
					<td class="text-center" width="6%">
						<button class="btn bottest-button btn-secondary button-sm button-sm-custom btn-deleteRowTable"><i class="ti-close"></i></button>
					</td>
					<td width="25%">
						<input type="text" id="Key" class="form-control" placeholder="Key">
					</td>
					<td width="25%">
						<textarea id="Value" class="form-control"  placeholder="Value"></textarea>
					</td>
					<td width="19%">
						<select name="Type" class=" form-control">
								<option value="file">File path</option>
								<option value="text">Text value</option> 
						</select>
					</td>
					<td width="25%">
						<input type="text" name="Description" class="form-control"  placeholder="Description">
					</td>
				</tr> -->
			</tbody>
		</table>
<!-- 		<div class="col-md-12 text-center my-3">
			<button type="button" class="btn bottest-button btn-success button-sm" id="addRowText"><i class="ti-plus mr-1"></i>Add new text</button>
			<button type="button" class="btn bottest-button btn-success button-sm" id="addRowFile"><i class="ti-plus mr-1"></i>Add new file</button>
		</div> 
-->
	</div>
	<div class="w-100 bodyOfTypeRequest" id="api_body_requestRaw" style="display:none">
		<textarea rows="10" class="form-control border-top-0"></textarea>
	</div>
	<div class="w-100 upload-btn-wrapper bodyOfTypeRequest " id="requestBodyBinary" style="display:none">
		<div class="col-md-3 position-relative py-3 binaryClass">
		  	<!--  <button class="btn bottest-button btn-success" type="button" id="btnUploadRequestBody"><i class="icon-cloud-upload mr-2"></i>Select File</button>
		  	<input id="fileUploadRequestBody" type="file"  name="" class="w-100 h-100" required="required" style="display:block">
		  	<div class="d-flex">
		  		<span class="nameFileUpload mr-3"></span>
		  		<a href="javascript:void(0)" id="resetUploadFileRsBody" style="display:none">
		  			<i class="icon-close text-danger"></i>
		  		</a>
		  	</div>
		  	 -->
	  	</div>
	</div>
</div>