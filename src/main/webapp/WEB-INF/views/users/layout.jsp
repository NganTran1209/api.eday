<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="/WEB-INF/views/users/projectHeader.jsp"></jsp:include>

<div class="card card-box card-topline-green changeAddIssue" id="drawScreenDesign">
	<div class="card-body">
		
		<div class="w-100" id="bodyListLayout">
			<div class="row">
				<div class="col-md-2 form-group">
					<button type="button" class="btn bottest-button btn-success ml-1" onclick="functionAddNewLayout();">
				 		<i class="fa fa-plus mr-2"></i>  <spring:message code="label.AddNew"></spring:message>
					 </button>
				</div>
				 <div class="col-md-8">
					<div class="row">
						<div class="col-md-3 form-group">
						<spring:message code="label.Enterfilename" var = "enterfilename"></spring:message>
							<input class="form-control colorPlace" placeholder="${enterfilename}" id="filterFileName" value=""/>
						</div>
						<div class="col-md-3 form-group">
							<select class="form-control testsuiteFilter"  multiple="multiple" id="filterTestsuite">
							</select>
						</div>
						<div class="col-md-3 form-group">
							<select class="form-control executeBy" multiple="multiple" id="filterCreateBy">
							</select>
						</div>
						<div class="col-md-3 form-group">
							<input class="form-control toDay colorPlace" id="filterCreateDate" placeholder="<spring:message code="label.CreateDate"></spring:message>" />
						</div>
					</div>
				</div>
				<div class="col-md-2 form-group">
					<button id="filterScrDesign" class="btnPlay btn bottest-button btn-success mr-2" onclick="filterScreenDesign();"><i class="ti-search text-white"> </i> <spring:message code="label.Search"></spring:message></button>
				</div>
			 </div>
			 <div class="col-12">
				<div class="row" id="bodyLayoutRecord"></div>
				<div class="row position-relative">
					 <div class="preloader progress-loadMainContent" id="loadingScreenDesign" style="display: none;">
					    <div class="loader" style="top:30%">
					        <div class="loader__figure"></div>
					        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting"></spring:message><br> Bottest.io</p>
					    </div>
					</div>
				</div>
			</div>
		</div>
		
			
		<div class="w-100" id="detailsLayout">
			<div class="w-100 form-group">
				<button class="btn bottest-button btn-secondary" onclick="closeDetailLayout()">
					<i class="icon-arrow-left mr-2"></i> Back
				</button>
			</div>
			<div class="w-100">
				<div class="page-title d-flex"><i class="ti-info-alt my-auto mr-2"></i> <span>Layout Name:</span> <span id="titleLayout" class="text-success font-weight-bold"></span></div>
			</div>
			<div class="box-btn-actionLayout"> 
		         <button class="btn bottest-button btn-success btnGenerateItems button-sm" onclick="functionGenerateTc(this);" id="btnGenerateItemDetails" type="button">Generate UTCs</button>
		         <div class="upload-btn-wrapper">
		         	<button class="btn bottest-button btn-primary button-sm" type="button">Import Screen Item</button>
		         	<input id="fileImportID" accept=".csv" type="file" name="myfile"/>
		         </div>
		         <button class="btn bottest-button btn-warning btnExportCSV button-sm" id="btnExportCSVFile" type="button" onclick="exportCSVFile(this);">Export Screen Item</button>
		         <div class="upload-btn-wrapper">
				  	<button class="btn bottest-button btn-success button-sm" type="button" id="uploadImages"><spring:message code="label.UploadImage"></spring:message></button>
				  	<input id="fileID" type="file" name="myfile" multiple="true"/>
				</div>
			   	<div class="upload-btn-wrapper">
					<button class="btn bottest-button btn-success button-sm" id="btnUpdateViewIcon">Save Layout</button>
				</div>
		   	</div>
		   	<p style="color: red" id="errorImportScreen"></p>
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-8 borderBox shadow-none border-0 light card-box m-0 tabsBorderCustom">
			           <div class="borderBox-title tabbable-line form-group">
			               <ul class="nav nav-tabs-border float-left" id="tabsViewLayout">
			                   <li class="nav-item" role="presentation">
			                       <a href="#borderBox_tab1" data-toggle="tab" role="tab" class="text-uppercase border-success active" aria-controls="borderBox_tab1" id="viewgirdLayoutDt" aria-selected="true"> View Icon</a>
			                   </li>
			                   <li class="nav-item"  role="presentation">
			                       <a href="#borderBox_tab2" data-toggle="tab" role="tab" class="text-uppercase border-success" aria-controls="borderBox_tab2" id="viewlistLayoutDt" onclick="generateGridLayout()" aria-selected="false"> View Grid </a>
			                   </li>
			               </ul>
			           </div>
			           <div class="borderBox-body" id="tab-content-viewLayout">
			               <div class="tab-content">
			                   <div class="tab-pane active show" id="borderBox_tab1" role="tabpanel">
			                   		<div class="w-100 form-group text-left">
								    	<button class="btn bottest-button btn-success btnAddControls button-sm" onClick="controlItem()"><i class="ti-plus mr-2"></i> Add New Control</button>
			 						</div>
			                        <div class="w-100 form-group"> 
						         		<div class="w-100 panel-group d-flow-root" id="accordion"></div>
							         </div>
				                </div>
			                <div class="tab-pane" id="borderBox_tab2" role="tabpanel">
			                	<div class="w-100 form-group text-left">
								    <button class="btn bottest-button btn-success btnAddControls button-sm" onClick="controlItem()"><i class="ti-plus mr-2"></i> Add New Control</button>
								</div>
			                    <div class="table-scrollable">
			                       <table class="table table-hover table-bordered table-common viewlistDetail-TB">
			                           <thead>
			                               <tr class="tr-bottest-hd bg-success">
			                               	   <th></th>
			                                   <th>Control name</th>
			                                   <th>Control type</th>
			                                   <th>ID</th>
			                                   <th>Name</th>
				                               <th>Class</th>
				                               <th>Xpath</th>
				                               <th>Href</th>
				                               <th>Text value</th>
				                               <th>Init value</th>
				                               <th>Data format</th>
				                               <th>Readonly</th>
				                               <th>Disabled</th>
				                               <th>Max length</th>
				                               <th>Min length</th>
				                                <th>Required</th>
				                               <th>Background Color</th>
				                               <th>Font color</th>
				                               <th>Text Align</th>
				                               <th>Error Attribute Name</th>
				                               <th>Error Attribute Value</th>
				                               <th>Note</th>
			                               </tr>
			                           </thead>
			                           <tbody id="bodyDetailLayoutList">
			                               
			                           </tbody>
			                       </table>
			                   </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    <input id="testsuiteName" hidden name="nameTs"/>
			    <input id="indexScreenLayout" hidden name="index"/>
			    <div class="col-md-4">
			       	<ul class="list-unstyled list-imgTestcase form-group gallery" id ="imageLayoutDetail"></ul>
			    </div>
		    </div>
		</div>
	</div>
</div>
<jsp:include page="/WEB-INF/views/users/modal_add_new_issue.jsp"></jsp:include>

<script src="${context}/assets/js/management/layouts.js"></script>
<script src="${context}/assets/js/management/layout.js"></script>
<script src="${context}/assets/js/management/tasks.js" ></script>
<script>
	var classTab = $(".text-uppercase.border-success");
	$.each(classTab, function(index, values){
	    if($(values).hasClass("active") == true){
	        $(values).removeClass("active");
	    }
	});
	$("#callScreenDesignTab").addClass("active");
	$("#viewgirdLayoutDt").addClass("active");
	
	var projectId=${projectId};
	var layoutls = ${listScreenDesign};
	
	var testsuiteNameSS = '${tsNameSession}';
	var createBySS = '${createBySession}';
	var createDateSS = "${createDateSession}";
	var fileNameSS = "${fileNameSession}";
	var checkSession = true;
	var listBug=${listBug};
 	var taskList = ${taskList};
 	var checkSessionExist = ${checkSessionExist};
	var testsuiteNameParse = "";
	var listActions = ${listActions};
	var actionHelp = ${actionHelp};
	if(testsuiteNameSS != ""){
		testsuiteNameParse = JSON.parse(testsuiteNameSS);
	}
	
	var createByParse = "";
	if(createBySS != ""){
		createByParse = JSON.parse(createBySS);
	}
	
	getListTestsuite("true", function(tsByProjectId){
		$("#filterTestsuite").select2({
			placeholder: getLang(currentLocale,"Select Testsuite"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			data: tsByProjectId,
			tags: true,
			dropdownParent: ""
		});
		$("#filterTestsuite").select2({placeholder: getLang(currentLocale,"Select Testsuite"), theme: 'bootstrap'});
		$("#filterTestsuite").val(testsuiteNameParse);
		$("#filterTestsuite").trigger('change');
		
	});
	getCreateBy(function(listExecuteBy){
		$("#filterCreateBy").select2({
			placeholder: getLang(currentLocale,"Select Create By"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			data: listExecuteBy,
			tags: true,
			dropdownParent: ""
		});
		$("#filterCreateBy").select2({placeholder: getLang(currentLocale,"Select Create By"), theme: 'bootstrap'});
		$("#filterCreateBy").val(createByParse);
		$("#filterCreateBy").trigger('change');
	});
	
	$('#filterCreateDate').bootstrapMaterialDatePicker
	({
		format: 'YYYY-MM-DD',
		lang: 'en',
		weekStart: 1,
		nowButton : true,
		time: false,
		clearButton: true
		
	});
	$("#filterCreateDate").val(createDateSS);
	$("#filterFileName").val(fileNameSS);
	
	
	if((testsuiteNameSS != "" && testsuiteNameSS != "[]") || (createBySS != "" && createBySS != "[]") || createDateSS != "" || fileNameSS != ""){	
		
	} else if(!checkSessionExist){
		testsuiteNameParse = [];
		createDateSS = "";
		createByParse = [];
		fileNameSS = "";
		$(".preloader").fadeOut("slow");
	}
	drawLayoutByFilter(testsuiteNameParse, createByParse, createDateSS, fileNameSS);
	
</script>

<jsp:include page="/WEB-INF/views/users/modal_add_new_layout.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/users/modal_Detail_Layout.jsp"></jsp:include>
<script src="${context}/assets/js/management/pictureEyes.js"></script>
<jsp:include page="/WEB-INF/views/users/modal_submit_generateTC.jsp"></jsp:include>
<script src="${context}/assets/js/management/generateUTC.js"></script>
<jsp:include page="/WEB-INF/views/users/modal_copy_layout.jsp"></jsp:include>

<script>
	$(function(){
		var hash = window.location.hash;
		hash && $('ul.nav a[href="' + hash + '"]').tab('show');
		$('.nav-tabs a').click(function (e) {
			$(this).tab('show');
			var scrollmem = $('body').scrollTop();
			window.location.hash = this.hash;
			$('html,body').scrollTop(scrollmem);
		});
	});
</script>