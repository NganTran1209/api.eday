<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<jsp:include page="/WEB-INF/views/users/projectHeader.jsp"></jsp:include>

<div class="clearfix"></div>
<div class="row changeAddIssue" id="drawTestsuites">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="card card-box card-topline-green">
           <div class="card-body " style="">
           		<div class="form-group">
           			 <button type="button" class="btn bottest-button btn-success" onclick="showModalAddNewTestsuite('false');">
           			 	<i class="fa fa-plus mr-1"></i> <spring:message code="label.AddNew"></spring:message> 
         			 </button>
         			 <button type="button" class="btn bottest-button btn-success" id="exportAllTestcase" onclick="getAllTestcaseToCSV()">
           			 	<i class="ti-export text-white mr-1"> </i> <spring:message code="label.ExportAll"> </spring:message> 
         			 </button> 
           		</div>
               <div class="table-scrollable">
                   <table class="table table-common">
                       <thead>
                           <tr class="tr-bottest-hd bg-success">
								<th class="border-bottom-0" width="25%"><spring:message code="label.Testsuite"></spring:message> </th>
								<th class="border-bottom-0" width="60%"><spring:message code="label.Teststatus"></spring:message> </th>
								<th class="border-bottom-0"></th>
								<th class="border-bottom-0" width="5%"></th>
							</tr>
                       </thead>
                       <tbody id="bodyTestsuiteRecord">
                           
                       </tbody>
                   </table>
               </div>
               <div class="position-relative row">
               		<div class="preloader loading-important position-absolute progress-loadMainContent" id="loader-testsuite">
					    <div class="loader" style="top:20%">
					        <div class="loader__figure"></div>
					        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting"></spring:message><br> Bottest.io</p>
					    </div>
					</div>
               </div>
           </div>
       </div>
	</div>
</div>
<jsp:include page="/WEB-INF/views/users/modal_add_new_issue.jsp"></jsp:include>

<input type="hidden" id="hiddenProjectId" value="${projectId}">
<script src="${context}/assets/js/management/testsuites.js"></script>

<script src="${context}/assets/js/management/tasks.js" ></script>

<script src="${context}/assets/js/management/testsuite.js"></script>
<script>
	var classTab = $(".text-uppercase.border-success");
	$.each(classTab, function(index, values){
	    if($(values).hasClass("active show") == true){
	        $(values).removeClass("active show");
	    }
	});
	$("#callTestsuiteTabs").addClass("active show");
	var projectId = ${projectId};
	var listBug=${listBug};
 	var taskList = ${taskList};
 	var testsuiteParse = JSON.parse(`${testsuiteList}`);
 	var listActions = ${listActions};
	var actionHelp = ${actionHelp};
	var listRole = ${listPermisson};
 	var listPermisson =JSON.parse(listRole[0].role_detail);
	loadListRs(testsuiteParse);
	
</script>

<jsp:include page="/WEB-INF/views/users/modal_add_new_testsuite.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/users/modal_edit_testsuite.jsp"></jsp:include>

<c:choose>
	<c:when test="${ sessionScope.lang == 'ja'}">
	<script src="${context}/assets/actions_ja.js" ></script> 
	</c:when>
	<c:otherwise>
	<script src="${context}/assets/actions.js" ></script>
	</c:otherwise>
</c:choose>
<script>
	$(function(){
		var hash = window.location.hash;
		hash && $('ul.nav a[href="' + hash + '"]').tab('show');
		$('.nav-tabs a').click(function (e) {
			$(this).tab('show');
			var scrollmem = $('body').scrollTop();
			window.location.hash = this.hash;
			$('html,body').scrollTop(scrollmem);
		});
	});
</script>

<script src="${context}/assets/js/management/modal_run_testcases.js"></script>
<jsp:include page="/WEB-INF/views/users/modal_run_testcases.jsp"></jsp:include>