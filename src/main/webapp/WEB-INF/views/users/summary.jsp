<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />



<div class="row">
	 <div class="col-md-12 mb-2">
         <div class="card card-box card-topline-green">
             <div class="card-head card-head-icon">
                 <header><spring:message code="label.Issue"></spring:message></header>
             </div>
             <div class="card-body no-padding height-9">
             	<div class="text-danger" style="text-align: center;" id="issueCircleEmpty"></div>
                 <div class="col-12" id="progressIssueAdd">
                 	
                </div>
            </div>
        </div>
    </div>
     <div class="col-md-12 mb-2">
         <div class="card card-box card-topline-green">
             <div class="card-head card-head-icon">
                 <header><spring:message code="label.CreatingProgress"></spring:message></header>
             </div>
             <div class="card-body no-padding height-9">
             	 <div  class="text-danger" style="text-align: center;" id="creatingProgressCircleEmpty"></div>
                 <div class="row" id="progressCreateAdd">
	      			
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mb-2">
         <div class="card card-box card-topline-green">
             <div class="card-head card-head-icon">
                 <header><spring:message code="label.TestProgress"></spring:message></header>
             </div>
             <div class="card-body no-padding height-9">
             	<div class="text-danger" style="text-align: center;" id="testProgressCircleEmpty"></div>
                 <div class="row" id="progressTestAdd">
                 	
                </div>
            </div>
        </div>
    </div>
</div>

<script src="${context}/assets/js/management/summary-chart.js"></script>