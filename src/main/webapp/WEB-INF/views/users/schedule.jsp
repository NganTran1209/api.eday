<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:include page="/WEB-INF/views/users/projectHeader.jsp"></jsp:include>

<div class="card card-box card-topline-green">
	<div class="card-body changeAddIssue" id="drawSchedule">
		<div class="col-md-12 form-group">
			<div class="row">
				<button type="button" class="btn bottest-button btn-success mb-3" onclick="creatScheduler();">
			      	<i class="fa fa-plus mr-1"></i><spring:message code="label.Createnewschedule"></spring:message>
				</button>
				<div class="col-md-2 form-group ml-auto mb-3">
					<select class="form-control testsuiteFilter" multiple="multiple" id="filterTestsuite">
					</select>
				</div>
				<button id="filSchedule" class="btnPlay btn bottest-button btn-success mb-3" onclick="filterSchedule();">
					<i class="ti-search text-white"> </i> Search
				</button>
			</div>
		</div>
		<div class="w-100" id="box-schedule-init">
			<div class="position-relative row">
				<div class="preloader progress-loadMainContent" id="loadlistScheduler" style="display: none;">
				    <div class="loader" style="top:30%">
				        <div class="loader__figure"></div>
				        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting"></spring:message><br> Bottest.io</p>
				    </div>
				</div>
			</div>
			<div id="listScheduler"></div>
			<div class="w-100 text-center text-danger" id="alert-empty-schedule"></div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/users/modal_add_new_issue.jsp"></jsp:include>
</div>


<script src="${context}/assets/js/management/scheduler.js"></script>
<jsp:include page="/WEB-INF/views/users/modal_run_scheduler.jsp"></jsp:include>
<script src="${context}/assets/js/management/tasks.js" ></script>
<script>
	var classTab = $(".text-uppercase.border-success");
	$.each(classTab, function(index, values){
	    if($(values).hasClass("active show") == true){
	        $(values).removeClass("active show");
	    }
	});
	$("#calSchedulerTabs").addClass("active show");
	
	var glTestsuiteId = null;
	var projectId = ${projectId};
	$("#listScheduler").empty();
	$(".progress-loadMainContent").show();
	var listScheduler = ${listExcute};
	var checkSession = true;
	var tsNameSession = '${tsNameSession}';
	var listBug=${listBug};
 	var taskList = ${taskList};
 	var checkSessionExist = ${checkSessionExist};
	
	var testsuiteNameParse = "";
	if(tsNameSession != ""){
		testsuiteNameParse = JSON.parse(tsNameSession);
	}
	
	if(tsNameSession != "" && tsNameSession != "[]"){
		drawScheduleByFilter(testsuiteNameParse);
	} else if(!checkSessionExist){
		drawListScheduler(listScheduler, "#listScheduler");	
	}
	
	getListTestsuite("true", function(testsuiteLs){
				
		$("#filterTestsuite").select2({
			placeholder: getLang(currentLocale,"Select Testsuite"),
			width:"100%",
			minimumResultsForSearch: Infinity,
			data: testsuiteLs,
			tags: true,
			dropdownParent: "",
			theme: 'bootstrap'
		});
		
		$("#filterTestsuite").val(testsuiteNameParse);
		$("#filterTestsuite").trigger('change');
	});
	
	getListTestsuite("false", function(testsuiteLs){
		testsuitels = testsuiteLs;
		$("#testsuiteRunScheduler").empty();
		$.each( testsuitels, function(index, valuesItemTestsuite) { 
			var optionSelectTestsuite = '<option value="'+valuesItemTestsuite["testsuite"]+'">'+valuesItemTestsuite["testsuite"]+'</option>';
			$("#testsuiteRunScheduler").append(optionSelectTestsuite);
			$("#testsuiteRunScheduler").select2({
				dropdownParent: $("#modalRunScheduler"),
				placeholder: getLang(currentLocale,"Select an option"),
				//allowClear: true,
				width: '100%',
				theme: 'bootstrap'
			});
		});
	});
	
	
	//getListTestsuites();
</script>

<script src="${context}/assets/js/management/modal_run_testcases.js"></script>
<jsp:include page="/WEB-INF/views/users/modal_run_testcases.jsp"></jsp:include>
