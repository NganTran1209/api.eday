<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div id="tableListresuft" class="position-relative">
	<div class="position-relative row">
		<div class="preloader progress-loadMainContent" id="loadlistScheduler" style="display: none;">
		    <div class="loader" style="top:30%">
		        <div class="loader__figure"></div>
		        <p class="loader__label text-center"><spring:message code="label.Pleasewaiting"></spring:message><br> Bottest.io</p>
		    </div>
		</div>
	</div>
	<div class="w-100 table-responsive">
	<div  id="listRunningJobSchedule"></div>
	</div>
</div>
<script src="${context}/assets/js/management/modal_run_testcases.js"></script>
<jsp:include page="/WEB-INF/views/users/modal_run_testcases.jsp"></jsp:include>
