<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="modal fade bs-example-modal-sm in" id="xMd" tabindex="1"
	role="dialog" aria-hidden="true" data-backdrop="static"
	style="display: block; padding-left: 0px;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close" id="btnClose">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel2">Run Test</h4>
				<div id="error"></div>
			</div>
			<div class="modal-body" id="modalBody">
				<div class="row">
					<div class="col-md-5 col-xs-12">
						<h5>OS</h5>
					</div>
					<div class="col-md-7 col-xs-12">
						<select id="slOs" class="form-control">
							<c:forEach items="${lsOs}" var="item">
							    <c:choose>
									<c:when test="${item.defaultC eq  true }">
								    	<option value="${item.itemValue}" selected>${item.itemName}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.itemValue}">${item.itemName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-5 col-xs-12">
						<h5>Run Location</h5>
					</div>
					<div class="col-md-7 col-xs-12">
						<select id="sladdress" class="form-control">
							<c:forEach items="${lsLocation}" var="item">
							    <c:choose>
									<c:when test="${item.defaultC eq  true }">
								    	<option value="${item.itemValue}" selected>${item.itemName}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.itemValue}">${item.itemName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-5 col-xs-12">
						<h5>Browser</h5>
					</div>
					<div class="col-md-7 col-xs-12">
						<select id="slbrowser" class="form-control">
							<c:forEach items="${lsBrowser}" var="item">
							    <c:choose>
									<c:when test="${item.defaultC eq  true }">
								    	<option value="${item.itemValue}" selected>${item.itemName}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.itemValue}">${item.itemName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-md-5 col-xs-12">
						<h5>Screen Resolution</h5>
					</div>
					<div class="col-md-7 col-xs-12">
						<select id="slscreen" class="form-control">
							<c:forEach items="${lsResolution}" var="item">
							    <c:choose>
									<c:when test="${item.defaultC eq  true }">
								    	<option value="${item.itemValue}" selected>${item.itemName}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.itemValue}">${item.itemName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-5 col-xs-12">
						<h5>Timeout(ms)</h5>
					</div>
					<div class="col-md-7 col-xs-12">
						<input class="form-control" type="number" id="txtTimeout"
							value="300">
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-5 col-xs-12">
						<h5>Record Video</h5>
					</div>
					<div class="col-md-7 col-xs-12">
						<div class="icheckbox_flat-green" style="position: relative;">
							<input type="checkbox" name="videoMode" id="videoMode"
								class="flat" style="position: absolute; opacity: 0;">
						</div>
					</div>
				</div>
				<br>
				<div class="row form-group">
					<div class="col-md-5 col-xs-12">
						<h5>Mobile Mode</h5>
					</div>
					<div class="col-md-7 col-xs-12">
						<div class="icheckbox_flat-green checked"
							style="position: relative;">
							<input type="checkbox" name="mobileMode" id="mobileMode"
								class="flat" style="position: absolute; opacity: 0;">
						</div>
					</div>
				</div>
				<div class="boxMoileAppend form-group">
					<div class="row " id="devices">
						<div class="col-md-5 col-xs-12">
							<h5>Select Devices</h5>
						</div>
						<div class="col-md-7 col-xs-12 ">
							<select id="devicesSelection"
								class="form-control select2-hidden-accessible" tabindex="1"
								aria-hidden="true">
								<c:forEach items="${lsDevice}" var="item">
								    <c:choose>
										<c:when test="${item.defaultC eq  true }">
									    	<option value="${item.itemValue}" selected>${item.itemName}</option>
										</c:when>
										<c:otherwise>
											<option value="${item.itemValue}">${item.itemName}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
					</div>
					<br>
				</div>
				<div class="row" id="testcases">
					<div class="col-md-5 col-xs-12">
						<h5>Select Testcases</h5>
					</div>
					<div class="col-md-7 col-xs-12 ">
						<select id="tcSelection" multiple="multiple" class="form-control"
							style="display: none;">
							

						</select>
						<div class="btn-group" style="width: 100%;">
						</div>
					</div>
				</div>
				<div class="row" id="hours">
					<div class="col-md-5 col-xs-12">
						<h5><input type=checkbox name="minutely">Minute of Hour</h5>
					</div>
					<div class="col-md-7 col-xs-12 ">
							<c:forEach var="i" begin="0" end="59" step="5" varStatus ="status">
								<input type="checkbox" name="minutes" value="${i}">
							    <c:out value="${i}" /> 
							</c:forEach>
						</div>
					</div>
				</div>

				<div class="row" id="hours">
					<div class="col-md-5 col-xs-12">
						<h5><input type=checkbox name="hourly">Hours of Day</h5>
					</div>
					<div class="col-md-7 col-xs-12 ">
							<c:forEach var="i" begin="0" end="23" step="1" varStatus ="status">
								<input type="checkbox" name="hours" value="${i}">
							    <c:out value="${i}" /> 
							</c:forEach>
						</div>
					</div>
				</div>
				<jsp:useBean id="dateFormatSymbols" class="java.text.DateFormatSymbols"></jsp:useBean>
						
				<div class="row" id="weekly">
					<div class="col-md-5 col-xs-12">
						<h5><input type=checkbox name="weekly">Day of week</h5>
					</div>
					<div class="col-md-7 col-xs-12 ">
							<c:forEach var="i" begin="0" end="6" step="1" varStatus ="status">
								<input type="checkbox" name="dayOfWeeks" value="${i}">
							    <c:out value="${dateFormatSymbols.getShortWeekdays()[i+1]}" /> 
							</c:forEach>
						</div>
					</div>
				</div>
				
				<div class="row" id="Date of month">
					<div class="col-md-5 col-xs-12">
						<h5><input type=checkbox name="dom">Day of Month</h5>
					</div>
					<div class="col-md-7 col-xs-12 ">
							<c:forEach var="i" begin="0" end="30" step="1" varStatus ="status">
								<input type="checkbox" name="dayOfMonths" value="${i}">
							    <c:out value="${i+1}" />
							</c:forEach>
						</div>
					</div>
				</div>
				
				<div class="row" id="monthly">
					<div class="col-md-5 col-xs-12">
						<h5><input type=checkbox name="monthly">Monthly</h5>
					</div>
					<div class="col-md-7 col-xs-12 ">
						<c:forEach var="i" begin="0" end="11" step="1" varStatus ="status">
							<input type="checkbox" name="months" value="${i}">
						    <c:out value="${dateFormatSymbols.getShortMonths()[i]}" /> 
						</c:forEach>
					</div>
				</div>			
			<div class="modal-footer" id="modalFooter">
				<button type="button" id="btnRun" class="btn btn-submit btn-icon-df">
					<i class="fa fa-check"> </i>Execute
				</button>
				<button type="button" class="btn btn-cancel btn-icon-df"
					data-dismiss="modal" id="btnExit">
					<i class="fa fa-times"> </i> Close
				</button>
			</div>