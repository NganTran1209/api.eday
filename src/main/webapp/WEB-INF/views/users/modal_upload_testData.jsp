<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="modal fade bs-example-modal-sm" id="uploadTestData" tabindex="1" role="dialog" aria-hidden="true">
 	<div class="modal-dialog">
 	<form name="formUploadTestData" id="formUploadTestData" enctype="multipart/form-data" action="javascript:void(0)" accept-charset="UTF-8" method="post">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title mt-0"><spring:message code="label.UploadFile"></spring:message></h4>
				<button type="button" class="close" data-dismiss="modal" onclick="resetFormTestData()">×</button>
			</div>
			<div class="modal-body">
				<div class="w-100 item form-group bad">
					<div class="input-icon right">
						<i class="fa" data-toggle="tooltip"></i>
						<select id="listTestsuiteUp" class="form-control" name="selectTestsuiteUp">
						</select>
					</div>
				</div>
			    <div class="upload-btn-wrapper form-group d-block">
			    	<label class="w-100" for="name">
				        	Description<span style="color:red;">*</span>
				    </label>
			        <div class="w-100 item form-group bad">
        	   			<div class="input-icon right">
        	   				<i class="fa" data-toggle="tooltip"></i>
        	   				<spring:message code="label.Enterdescription" var="enterDescription"></spring:message>
			        		<textarea id="descriptionTestData" name="desFormUploadTestData" class="form-control" required="required" rows="3" placeholder="${enterDescription}" required="required" /></textarea>
			        	</div>
				    </div>
				</div> 
				<div class="upload-btn-wrapper">
			    	<div class="w-100 position-relative form-group mb-0 item bad">
			    		<div class="input-icon right">
				    		<i class="fa" data-toggle="tooltip"></i>
						  	<button class="btn bottest-button btn-success w-100" type="button" ><i class="icon-cloud-upload mr-2"></i><spring:message code="label.UploadFile"></spring:message></button>
						  	<input id="fileUploadTestData" type="file" accept=".csv,.xlsx,.xls,.png,.PNG,.jpg" name="filesUploadTestData" multiple="true" class="w-100 h-100 d-block" required="required" title="Choose file">
						  	<span class="nameFileUpload"></span>
					  	</div>
				  	</div> 
				</div> 
				<p id="messageFileError" style="color:red;"></p>
			</div>
			
			<div class="modal-footer" >
				<button type="submit" onclick="validateUploadFieTestData()" class="btn bottest-button btn-success mr-2" id="btnUploadFileTestData">
					<i class="ti-check mr-2"></i>Save
				</button>
				<button type="button" class="btn bottest-button btn-secondary"
					data-dismiss="modal" onclick="resetFormTestData()">
					<i class="ti-close mr-2"></i> Close
				</button>
			</div>
		</div>
		</form>
	</div>
</div>
<script>
</script>