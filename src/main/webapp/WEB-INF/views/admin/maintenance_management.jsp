<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<div class="clearfix"></div>
<div class="w-100">
	<div class="card card-box card-topline-green" id="listSoftware"> 
		<div class="card-head card-head-icon">
           	<header><i class="ti-desktop"></i> List User</header>
           	<div class="tools">
				<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;" onclick="collsapseCardBody(this)"></a>
           	</div> 
          </div>
          <div class="card-body " style="">
          	<div class="form-group ml-3">
         		<button type="button" class="btn bottest-button btn-success" data-toggle="modal" data-target="#modalSettingTimeBuild" >
         			 <i class="fa fa-paper-plane-o">&nbsp;&nbsp;</i>  Send Maintenance Mail
       			 </button>
         	</div> 
          		<div class="table-scrollable">
                  <table class="display nowrap w-100" id="maintenance">
	                    <thead></thead>
						<tbody></tbody>
            	   </table>
               </div>
          </div>
      </div>
</div>
<div class="modal fade " tabindex="1" role="dialog" id="modalSettingTimeBuild">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success">
      	<h3 class="modal-title mt-0">Maintenance Mail</h3>  
      	<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetFormSettingTime()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form name="formSettingTime" id="formSettingTime" action="javascript:void(0)" accept-charset="UTF-8" method="post" class="form-horizontal">
        	<p id="idMaintenance" hidden></p>
        	<div class="row my-3"> 
				<div class="item form-group bad mb-2 d-flex mx-auto col-md-12">
			 		<label for="startTime" class="control-label col-md-2">Subject<span class="required">*</span></label>
			 		<div class="input-icon right col-md-10"><i class="fa startTime mr-3"></i>
				   	 	<input maxlength="255" type="text" value="" name="subjectMail" id="subjectMail" class="form-control"  placeholder="Subject">
			   		</div>
				 </div>
			 </div>
			 <div class="row mb-3"> 
				<div class="item form-group bad mb-2 d-flex mx-auto col-md-12">
			 		<label for="startTime" class="control-label col-md-2">Content</label>
			 		<div class="input-icon right col-md-10">
				   	 	<textarea class="form-control" rows="10" placeholder="Please enter" name="contentMail" id="contentMail"></textarea>
			   		</div>
				 </div>
			 </div>
			<div class="row mb-4"> 
				<div class="item form-group bad d-flex mx-auto col">
			 		<label for="startTime" class="control-label col-md-4">Start Time<span class="required">*</span></label>
			 		<div class="input-icon right col-md-8"><i class="fa startTime mr-3"></i>
				   	 	<input class="form-control" name="startTime" id="startTime" placeholder="Start Time">
			   		</div>
				 </div>
				 <div class="item form-group bad  d-flex mx-auto col">
			 		<label for="endTime" class="control-label col-md-4">End Time<span class="required">*</span></label>
			 		<div class="input-icon right col-md-8"><i class="fa endTime mr-3"></i>
				   	 	<input class="form-control"  name="endTime" id="endTime" placeholder="End Time">
			   		</div>
				 </div>
			 </div>
		    <div class=" text-center form-group">
		        <button type="submit" class="btn bottest-button btn-success mr-2" id="sendMail" onclick="sendMaitenanceMail()"><i class="fa fa-paper-plane-o">&nbsp;&nbsp;</i>Send Mail</button>
		        <button data-dismiss="modal" class="btn bottest-button btn-secondary" onclick="resetFormSettingTime()"><i class="ti-close mr-2"></i>Cancel</button>
      		</div>
	    </form>
      </div>
    </div>
  </div>
</div>
<script>
	var context = '${context}';
</script>
<script src="${context}/assets/js/management/admin/maintenance-management.js"></script>