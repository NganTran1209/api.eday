<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />


<div class="modal fade formUser" tabindex="1" role="dialog" id="modalEditUser">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success">
      	<h4 class="modal-title mt-0">Edit User</h4>  
      	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form name="formEditUser" id="formEditUser" action="javascript:void(0)" accept-charset="UTF-8" method="post" class="form-horizontal">
	       	<input type="hidden" name="token" id="token" value="${dto.getToken()}" disabled />
	       	<input type="hidden" name="username" id="userNameHidden" />
			 <div class="row item form-group bad mb-5">
			 	<div class="col-md-6 d-flex">
				    <label for="passconfirmation" class="col-md-5 control-label text-left">User Role</label>
				    <div class="col-md-7">
				    	<select id="editUserRoles" required="required" name="role" class="form-control">
		                       <option value="ROLE_ADMIN">Admin</option>
		                       <option value="ROLE_USER">User</option>
                  		</select>
				    </div>
			    </div>
			    <div class="col-md-6 d-flex">
				    <label for="passconfirmation" class="col-md-5 control-label text-left">Active User</label>
				    <div class="col-md-7">
				    	<select id="editUserEnable" required="required" name="enable" class="form-control">
	                       <option value="true">Yes</option>
	                       <option value="false">No</option>
	                   </select>
				    </div>
			    </div>
			 </div>
			 <div class="row item form-group bad mb-5 d-flex">
			 	 <div class="col-md-6 d-flex">
			    	 <label for="accountType" class="col-md-5 control-label text-left">Account Type</label>
				     <div class="col-md-7">
				    	<select id="accountTypes" required="required" name="accountTypes" class="form-control">
	    
	                   </select>
				    </div>
			    </div>
			    <div class="col-md-6 d-flex lab-limitedProject">
			    	 <label for="limitedProject" class="col-md-5 control-label text-left" > project Number</label>
				     <div class="col-md-7">
				    	<input id="limitedProjectNumb" type="number" required="required" name="limitedProjectNumb" min = "0" class="form-control">
				     </div>
				</div>
			 </div>
		    <div class="clearfix"></div>
		    <div class=" text-center form-group">
		        <button type="submit" class="btn bottest-button btn-success mr-2" id="updateRoleUser" onclick="saveUser();"><i class="ti-check mr-2"></i>Save</button>
		        <button data-dismiss="modal" class="btn bottest-button btn-secondary"><i class="ti-close mr-2"></i>Cancel</button>
      		</div>
	    </form>
      </div>
    </div>
  </div>
</div>