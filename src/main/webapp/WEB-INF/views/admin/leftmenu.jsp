<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="context" value="${pageContext.request.contextPath}" />


<!-- start sidebar menu -->
	<div class="sidebar-container">
		<div class="sidemenu-container navbar-collapse collapse fixed-menu">
              <div id="remove-scroll">
                  <ul class="sidemenu page-header-fixed p-t-20 sidemenu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                      <li class="sidebar-toggler-wrapper hide">
                          <div class="sidebar-toggler">
                              <span></span>
                          </div>
                      </li>
                      <li class="sidebar-user-panel">
                          <div class="user-panel">
                              <div class="row">
                                         <div class="sidebar-userpic">
                                             <img src="${context}/assets/img/dp.jpg" class="img-responsive" alt=""> </div>
                                     </div>
                                     <div class="profile-usertitle">
                                         <div class="sidebar-userpic-name" id="nameUser"></div>
                                         <div class="profile-usertitle-job" id=role></div>
                                     </div>
                                     <div class="sidebar-userpic-btn">
						        <a class="tooltips" href="#" data-placement="top" data-original-title="Profile" data-toggle="modal" data-target="#modalEditMember">
						        	<i class="material-icons">person_outline</i>
						        </a>
						        <!-- <a class="tooltips" href="email_inbox.html" data-placement="top" data-original-title="Mail">
						        	<i class="material-icons">mail_outline</i>
						        </a>
						        <a class="tooltips" href="chat.html" data-placement="top" data-original-title="Chat">
						        	<i class="material-icons">chat</i>
						        </a> -->
						        <a class="tooltips" href="login.html" data-placement="top" data-original-title="Logout">
						        	<i class="material-icons">input</i>
						        </a>
						    </div>
                          </div>
                      </li>
                      <li class="menu-heading">
	                	<span>-- Management</span>
	                </li>
                  </ul>
                  <ul class="sidemenu  page-header-fixed sidemenu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                       <li class="nav-item start">
                       		<span class="nav-link nav-toggle">
                       			<i class="material-icons">dashboard</i>
                       			<a class="title" href="${context}/admin">Dashboard</a>
                       			<span class="arrow" style="display:none" id="arrow_version test"></span>
                     		</span>
						</li>
						<li class="nav-item start">
                       		<span class="nav-link nav-toggle">
                       			<i class="material-icons">group</i>
                       			<a class="title" href="${context}/admin/user">User Management</a>
                       			<span class="arrow" style="display:none" id="arrow_version test"></span>
                     		</span>
						</li>
						<li class="nav-item start">
                       		<span class="nav-link nav-toggle">
                       			<i class="material-icons">desktop_mac</i>
                       			<a class="title" href="${context}/admin/component">Component Management</a>
                       			<span class="arrow" style="display:none" id="arrow_version test"></span>
                     		</span>
						</li>
						<li class="nav-item start">
                       		<span class="nav-link nav-toggle">
                       			<i class="material-icons">dvr</i> 
                       			<a class="title" href="${context}/admin/interface">Interface Management</a>
                       			<span class="arrow" style="display:none" id="arrow_version test"></span>
                     		</span>
						</li>
						<li class="nav-item start">
                       		<span class="nav-link nav-toggle">
                       			<i class="material-icons">coins</i> 
                       			<a class="title" href="${context}/admin/configAccountPrice">Config Account Price</a>
                       			<span class="arrow" style="display:none" id="arrow_version test"></span>
                     		</span>
						</li>
						<li class="nav-item start">
                       		<span class="nav-link nav-toggle">
                       			<i class="material-icons">computer</i> 
                       			<a class="title" href="${context}/admin/software">Software Management</a>
                       			<span class="arrow" style="display:none" id="arrow_version test"></span>
                     		</span>
						</li>
						<li class="menu-heading">
		                	<span>-- Project</span>
		                </li> 
		                <li class="nav-item start">
                       		<span class="nav-link nav-toggle">
                       			<i class=" ti-layout-grid2-alt"></i> 
                       			<a class="title" href="${context}/project">Project Management</a>
                       			<span class="arrow" style="display:none" id="arrow_version test"></span>
                     		</span>
						</li>
                  </ul>
                  <ul class="sidemenu sidemenuListProject  page-header-fixed sidemenu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                  
                  </ul>
                   <ul class="sidemenu sidemenuListProject  page-header-fixed sidemenu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                 		 <li class="nav-item start">
	                     	<span class="nav-link nav-toggle">
	                     		<i class="icon-cloud-download"></i> 
	                     		<a class="title" href="${context}/downloadSoftware"> <spring:message code="label.DownloadSoftware"></spring:message></a>
	                     		<span class="arrow" style="display:none" id="arrow_version test"></span>
	                   		</span>
						</li>
                  </ul>
              	</div>
             </div>
         </div>

         <!-- end sidebar menu --> 