<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />


<div class="clearfix"></div>
<div class="w-100">
	<div class="card card-box card-topline-green" id="listInterfProject"> 
		<div class="card-head card-head-icon">
           	<header><i class="ti-desktop"></i> Interface List</header>
           	<div class="tools">
				<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;" onclick="collsapseCardBody(this)"></a>
           	</div> 
          </div>
          <div class="card-body " style="">
          	<div class="form-group">
         		<button type="button" class="btn bottest-button btn-success" data-toggle="modal" data-target="#modalEditInterface" onclick="callModalAddInterface();">
         			 <i class="fa fa-plus"></i> Creat new interface
       			 </button>
         	</div> 
          		<div class="table-scrollable">
                    <table class="table table-hover table-checkable order-column full-width table-common" id="listInterfaceProject">
                        <thead>
                            <tr>
                                <th class="center"> #</th>
                                <th class="center"> Domain </th> 
                                <th class="center"> User Name</th>
                                <th class="center"> Job Name </th>
                                <th class="center"> OS </th>
                                <th class="center"> Running OS </th>
                                <th class="center"> Browser </th>
                                <th class="center"> Scm Type </th>
                                <th class="center"> Node Label </th>
                                <th class="center"> Action </th>
                            </tr>
                        </thead>
                        <tbody id="tableInterfaceManagement">
						</tbody>
                  </table>
               </div>
          </div>
      </div>
</div>

<jsp:include page="/WEB-INF/views/admin/modal-register.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/admin/modal-edit-interface.jsp"></jsp:include>
<script src="${context}/assets/js/management/admin/interface-management.js"></script>