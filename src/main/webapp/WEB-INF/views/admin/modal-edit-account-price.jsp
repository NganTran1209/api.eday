<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />


<div class="modal fade formAccountPrice" tabindex="1" role="dialog" id="modalEditAccountPrice">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success">
      	<h4 class="modal-title mt-0">Edit Account Price</h4>  
      	<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetFormAccountPrice()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form name="formEditAccountPrice" id="formEditAccountPrice" action="javascript:void(0)" accept-charset="UTF-8" method="post" class="form-horizontal">
			<p class="accountTypeCof" hidden></p>
			 <div class="item form-group bad mb-4 d-flex mx-auto col-md-11 accountTypeVal">
		 		<label for="accountTypeConfit" class="control-label col-md-4">Account Type<span class="required">*</span></label>
		 		<div class="input-icon right col-md-8"><i class="fa accounTypeConf mr-3"></i>
			   	 	<input class="form-control" type="text" name="accountTypeConfit" required="required" id="accountTypeConfit" placeholder="Account Type">
		   		</div>
			 </div>
			 <div class="item form-group bad mb-4 d-flex mx-auto col-md-11">
		 		<label for="paymentByMonth" class="control-label col-md-4">Payment By 3 Months</label>
		 		<div class="col-md-8">
			   	 	<input class="form-control" type="number" name="paymentByMonth" min="0" id="paymentByMonth" placeholder="Payment by 3 months">
		   		</div>
			 </div>
			 <div class="item form-group bad mb-4 d-flex mx-auto col-md-11">
		 		<label for="paymentByYear" class="control-label col-md-4">Payment By Year</label>
		 		<div class="col-md-8">
			   	 	<input class="form-control" type="number" name="paymentByYear"  min="0" id="paymentByYear" placeholder="Payment by year">
		   		</div>
			 </div>
			 <div class="item form-group bad mb-4 d-flex mx-auto col-md-11">
		 		<label for="projectlimittedConfit" class="control-label col-md-4">Project Limited</label>
		 		<div class="col-md-8">
			   	 	<input class="form-control" type="number" name="projectlimittedConfit" min="0" id="projectlimittedConfit" placeholder="Project limited">
		   		</div>
			 </div>
		    <div class="clearfix"></div>
		    <div class=" text-center form-group">
		        <button type="submit" class="btn bottest-button btn-success mr-2" id="updateAccountPrice" onclick="validateNewAccountPrice();"><i class="ti-check mr-2"></i>Save</button>
		        <button data-dismiss="modal" class="btn bottest-button btn-secondary" onclick="resetFormAccountPrice()"><i class="ti-close mr-2"></i>Cancel</button>
      		</div>
	    </form>
      </div>
    </div>
  </div>
</div>
