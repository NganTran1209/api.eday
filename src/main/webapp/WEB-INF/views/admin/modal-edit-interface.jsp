<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="modal fade formUser" tabindex="1" role="dialog" id="modalEditInterface">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success">
      	<h4 class="modal-title mt-0">Edit Interface</h4>  
      	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form name="formEditInterface" id="formEditInterface" action="javascript:void(0)" accept-charset="UTF-8" method="post" class="form-horizontal">
        	<input type="hidden" id="idInterface" value="">
        	<div class="row item form-group bad" id="itemIdInterfaceLock">
			    <label for="editDomian" class="col-md-4 control-label text-left">ID</label>
		    	<div class="col-md-8">
		    		<div class="input-icon right"><i class="fa firstName" data-toggle="tooltip"></i>
			      		<input type="text" class="form-control" id="idInterfaceLock" required="required" name="idInterfaceLock" disabled>
			      	 </div>
			    </div>
		  	</div>
	       	<div class="row item form-group bad">
			    <label for="editDomian" class="col-md-4 control-label text-left">Domain<span class="required">*</span></label>
		    	<div class="col-md-8">
		    		<div class="input-icon right"><i class="fa firstName" data-toggle="tooltip"></i>
			      		<input type="text" class="form-control" id="editDomian" required="required" name="editDomian" placeholder="Enter domain">
			      	 </div>
			    </div>
		  	</div>
	  		<div class="row item form-group bad">
			    <label for="editInterfUsename" class="col-md-4 control-label text-left">User name<span class="required">*</span></label>
		    	<div class="col-md-8">
		    		<div class="input-icon right"><i class="fa lastName" data-toggle="tooltip"></i>
			      		<input type="text" class="form-control" id="editInterfUsename" required="required" name="editInterfUsename" placeholder="Enter user name">
			      	 </div>
			    </div>
		  	</div>
		 	 <div class="row item form-group bad " >
			    <label for="editInterfJobname" class="col-md-4 control-label text-left">Job Name<span class="required">*</span></label>
			    <div class="col-md-8">
			    	<div class="input-icon right"><i class="fa checkCompany"></i>
			      		<input class="form-control" type="text" name="editInterfJobname" required="required" id="editInterfJobname" placeholder="Your component name">
			      	</div>
			    </div>
		 	 </div>
		 	 <div class="row item form-group bad">
			    <label for="passconfirmation" class="col-md-4 control-label text-left">OS<span class="required">*</span></label>
			    <div class="col-md-8">
			    	<select id="slOs" class="form-control" name="slOs">
					   
					</select>
			    </div>
			 </div>
			 <div class="row item form-group bad">
			    <label for="runningOS" class="col-md-4 control-label text-left">Running OS<span class="required">*</span></label>
			    <div class="col-md-8">
			    	<select id="runningOS" class="form-control" name="runningOS">
						    
					</select>
			    </div>
			 </div>
			 <div class="row item form-group bad">
			    <label for="eidtBrowser" class="col-md-4 control-label text-left">Browser<span class="required">*</span></label>
			    <div class="col-md-8">
			    	<select id="eidtBrowser" class="form-control" name="eidtBrowser">
						    
					</select>
			    </div>
			 </div>
			 <div class="row item form-group bad">
			    <label for="editInterfLocation" class="col-md-4 control-label text-left">Location<span class="required">*</span></label>
		    	<div class="col-md-8">
			      	 <select id="editInterfLocation" class="form-control" name="eidtBrowser">
						    
					</select>
			    </div>
		  	</div>
		  	<div class="row item form-group bad">
			    <label for="editInterfToken" class="col-md-4 control-label text-left">Token<span class="required">*</span></label>
		    	<div class="col-md-8">
		    		<div class="input-icon right"><i class="fa lastName" data-toggle="tooltip"></i>
			      		<input type="text" class="form-control" id="editInterfToken" required="required" name="editInterfToken" placeholder="Enter token">
			      	 </div>
			    </div>
		  	</div>
		  	<div class="row item form-group bad">
			    <label for="editScmType" class="col-md-4 control-label text-left">Scm Type<span class="required">*</span></label>
		    	<div class="col-md-8">
		    		<select class="form-control" name="editScmType" id="editScmType"> 
			      	    <option value="svn">SVN</option>
			      	    <option value="git">GIT</option>
			      	</select>
			    </div>
		  	</div>
		  	<div class="row item form-group bad">
			    <label for="editScmType" class="col-md-4 control-label text-left">Node Label<span class="required">*</span></label>
		    	<div class="col-md-8">
		    		<div class="input-icon right"><i class="fa lastName" data-toggle="tooltip"></i>
			      		<input type="text" class="form-control" id="editNodeLabel" required="required" name="editNodeLabel" placeholder="Enter Node Label">
			      	 </div>
			    </div>
		  	</div>
		    <div class="clearfix"></div>
		    <div class=" text-center form-group">
		        <button type="submit" class="btn bottest-button btn-success mr-2" id="saveInterfaceToDB" onclick="saveInterface();"><i class="ti-check mr-2"></i>Save</button>
		        <button data-dismiss="modal" class="btn bottest-button btn-secondary"><i class="ti-close mr-2"></i>Cancel</button>
      		</div>
	    </form>
      </div>
    </div>
  </div>
</div>