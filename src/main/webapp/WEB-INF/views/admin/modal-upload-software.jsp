<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<div class="modal fade formUser" tabindex="1" role="dialog" id="modalUploadSoftware">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success">
      	<h4 class="modal-title mt-0">Upload software</h4>  
      	<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetFormSoftware()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form name="formEditSoftware" id="formEditSoftware" action="javascript:void(0)" accept-charset="UTF-8" method="post" class="form-horizontal">
	       	<div class="row item form-group bad">
			    <label for="type" class="col-md-4 control-label text-left">Type</label>
		    	<div class="col-md-8">
			      	 <select id="typeSoftware" class="form-control" name="typeSoftware">
							<c:forEach items="${lsBottestTool}" var="item">
							    <c:choose>
									<c:when test="${item.itemValue eq 'Chrome extension' }">
								    	<option value="${item.itemValue}" selected>${item.itemName}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.itemValue}">${item.itemName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
			    </div>
		  	</div>
	  		<div class="row item form-group bad">
			    <label for="versionSoftware" class="col-md-4 control-label text-left">Version</label>
		    	<div class="col-md-8">
		    		<div class="input-icon right"><i class="fa versionSoftwaress"></i>
			      		<input type="text" class="form-control" id="versionSoftware" required="required" name="versionSoftwares" placeholder="Enter version software">
			      	 </div>
			    </div>
		  	</div>
		 	 <div class="row item form-group bad companyCheck fileUpload" >
			    <label for="editItemName" class="col-md-4 control-label text-left">File name</label>
			    <div class="col-md-8">
			    	<input type="file" id="fileName" name="fileName" title="choose file">
			    	<p class="err_file_software" style="color:red"></p>
			    </div>
		 	 </div>
		 	 <div class="row item form-group bad companyCheck" >
			    <label for="editItemName" class="col-md-4 control-label text-left">Upload Date</label>
			    <div class="col-md-8">
			    	<div class="input-icon right"><i class="fa mr-4 upDate"></i>
			      		<input class="form-control" type="Date" name="upDate" required="required" id="upDate" >
			      	</div>
			    </div>
		 	 </div>
		 	 <div class="row item form-group bad companyCheck" >
			    <label for="editItemName" class="col-md-4 control-label text-left">Requirement</label>
			    <div class="col-md-8">
			    	<div class="input-icon right">
			      		<textarea class="form-control" rows="3" placeholder="Please enter" name="requirement" id="requirement"></textarea>
			      	</div>
			    </div>
		 	 </div>
		 	 <div class="row item form-group bad companyCheck" >
			    <label for="editItemName" class="col-md-4 control-label text-left">Release Notes</label>
			    <div class="col-md-8">
			    	<div class="input-icon right">
			      		<textarea class="form-control" rows="3" placeholder="Please enter" name="releaseNote" id="releaseNote"></textarea>
			      	</div>
			    </div>
		 	 </div>
		    <input id="idSoftware" name="idSoftware" hidden>		    
		    <div class=" text-center form-group">
		        <button type="submit" class="btn bottest-button btn-success mr-2" id="UploadSoftware" onclick="validateAddSoftware();"><i class="ti-check mr-2"></i>Save</button>
		        <button data-dismiss="modal" class="btn bottest-button btn-secondary" onclick="resetFormSoftware();"><i class="ti-close mr-2"></i>Cancel</button>
      		</div>
	    </form>
      </div>
    </div>
  </div>
</div>


