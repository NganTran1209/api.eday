<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />


<div class="clearfix"></div>
<div class="w-100">
	<div class="card card-box card-topline-green" id="listAccountPrice"> 
		<div class="card-head card-head-icon">
           	<header><i class="icon-screen-desktop"></i>Account Price</header>
           	<div class="tools">
				<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;" onclick="collsapseCardBody(this)"></a>
           	</div> 
          </div>
          <div class="card-body " style="">
          	<div class="form-group">
         		<button type="button" class="btn bottest-button btn-success" data-toggle="modal" data-target="#modalEditAccountPrice" onclick="callModalAddAccountPrice();">
         			 <i class="fa fa-plus"></i> Creat New Account Price
       			 </button>
         	</div> 
          		<div class="table-scrollable">
                    <table class="table table-hover table-checkable order-column full-width table-common" id="listAccountPrices">
                        <thead>
                            <tr>
                                <th class="center"> Account Type </th> 
                                <th class="center"> Payment By 3 Months</th>
                                <th class="center"> Payment By Year </th>
                                <th class="center"> Project Limited </th>
                                <th class="center"> Action </th>
                            </tr>
                        </thead>
                        <tbody id="tableAccountPrice">
						</tbody>
                  </table>
               </div>
          </div>
      </div>
</div>
<script>
	var listAccountPrice = ${listAccountPrice}; 
</script>
<jsp:include page="/WEB-INF/views/admin/modal-edit-account-price.jsp"></jsp:include>
<script src="${context}/assets/js/management/admin/config-account-price.js"></script>