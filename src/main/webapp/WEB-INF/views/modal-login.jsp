<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div  class="modal fade formUser" role="dialog" id="modalLogin">
	<div class="modal-dialog" role="document">
		<div class="modal-content"> 
		<form class="form-horizontal" name="login">
			<div class="modal-header">
		      	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title">Sign In</h4>     
	      	</div>
			<div class="modal-body"> 
				<div class="form-group row">
				    <label for="username" class="col-md-2 control-label text-left">Username</label>
			    	<div class="col-md-10">
				      	<input type="text" name="username" id="username" placeholder="Enter username" class="form-control">
				    </div>
			  	</div> 
			  	<div class="form-group row">
				    <label for="password" class="col-md-2 control-label text-left">Password</label>
			    	<div class="col-md-10">
				      	<input type="password" name="password" id="password" placeholder="Enter password" class="form-control">
				    </div>
			  	</div>
			  	<div class="form-group row">
				    <div class="col-md-6">
				    	<input class="input-checkbox100" type="checkbox" name="rememberMe" id="rememberMe">
                       <label for="rememberMe" class="label-checkbox100">
                       		Remember me
                       </label>
				    </div>
			    	<div class="col-md-6 text-right">
				      	<a href="#" class="text-muted">Forgot Password?</a>
				    </div>
			  	</div>
			</div>
			<div class="modal-footer text-center">
		        <button type="button" class="btn btn-primary" onclick="signin();">Login</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      	</div>
	      	</form>
		</div>
	</div>
</div>
<script>
function signin(){
	var loginData = {
			username:$("input[name='username']").val(),
			password:$("input[name='password']").val()
	};
	$.ajax({
		url : "${context}/auth/signin",
		type : "POST",
		data : JSON.stringify( loginData),
		contentType : "application/json",
		success : function(result) {
			if (result.errormessage == null) {
				$("#modalLogin").modal("hide");
				$.ajax({
					url:"${context}/auth/me",
					type:"GET",
					headers:{
						"Authorization":"Bearer " + result.data.token
					},
					success: function(){
						window.location="${context}/project"
					}
				})
			} else {
			}
		},
		error : function(e) {
			console.log("ERROR: ", e);
		},
		done : function(e) {
			console.log("DONE");
		}
	});
	
	
	
}
</script>