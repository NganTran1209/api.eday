<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<style>
	#header, #outer-wrapper, #post-wrapper, #sidebar-wrapper, #content-wrapper, #footer-wrapper, #wrapper, .ignielToTop {display:none}
  body,html {overflow:hidden; margin:0; padding:0; width:100%; min-height:100vh}
  body {background:#fff; color:#1d2129}
  #igniel404 {background:#eceeee; text-align:center; font-weight:500; font-size:45px; font-family:'Ruda',sans-serif; position:fixed; width:100%; height:100%; line-height:1.25em; z-index:9999;}
  #igniel404 #error-text {position:relative; font-size:40px; color:#666; top:40%; right:50%; transform:translate(50%,-50%);}
  #igniel404 #error-text span {color:#1dcb8b;font-size:4rem;}
  #error-text .bottest-button{font-weight: 600; height: 40px; line-height: 40px; font-size: 17px;}
  @media only screen and (max-width:640px){
    #igniel404 #error-text {font-size:20px; display: grid;}
    #igniel404 #error-text span {font-size:60px;}
  }
  
  

</style>
<section class="main-form-fixed">

<div id='igniel404'>
  
  <div id='error-text'>
 	<div style="margin-bottom: 91px;color: #C94D4D;font-size: 7rem;">Can't Join Project !!!</div>
    <span style="">The number of projects is limited, please upgrade account.</span>  
   
  </div>
  <div id='error-text'><a  type="button" class="btn bottest-button btn-success" href="${context}/billing">upgrade account</a></div>
</div>
</section>