<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<style>
	#header, #outer-wrapper, #post-wrapper, #sidebar-wrapper, #content-wrapper, #footer-wrapper, #wrapper, .ignielToTop {display:none}
  body,html {overflow:hidden; margin:0; padding:0; width:100%; min-height:100vh}
  body {background:#fff; color:#1d2129}
  #igniel404 {background:#eceeee; text-align:center; font-weight:500; font-size:45px; font-family:'Ruda',sans-serif; position:fixed; width:100%; height:100%; line-height:1.25em; z-index:9999;}
  #igniel404 #error-text {position:relative; font-size:40px; color:#666; top:27%; right:50%; transform:translate(50%);}
  #igniel404 #error-text a {color:#888; text-decoration:none}
  #igniel404 #error-text p {margin:0!important; letter-spacing:.5px;}
  #igniel404 #error-text span {color:#1dcb8b;font-size:13.5rem;}
  #igniel404 #error-text a.back {background:#1dcb8b;color:#fff;padding:10px 20px;font-size:20px;border:double #fff;-webkit-transform:scale(1);-moz-transform:scale(1);transform:scale(1);transition:all 0.5s ease-out;}
  #igniel404 #error-text a.back:hover {background:#444;color:#fff;border:double #eceeee;}
  
  #igniel404 #error-text #copyright {font-size:16px}
  #igniel404 #error-text #copyright a {color:#1dcb8b}

  @media only screen and (max-width:640px){
    #igniel404 #error-text {font-size:20px;}
    #igniel404 #error-text span {font-size:60px;}
    #igniel404 #error-text a.back {padding:5px 10px;font-size:15px;}
    
  }

  h2 {
    color: #1d2129;
    margin-bottom: -2.6em;
  }

  p {
    color: #3e3636;
    margin-top: 0;
  }

  .main {
    text-align: center;
    width: 100%;
  }
</style>
<section class="main-form-fixed">

<div id='igniel404'>
  
  <div id='error-text'>
    <div style="color: #C94D4D;">ERROR</div>
    <div style="font-size: 13.5rem;font-weight: bold;display: block;line-height: normal;height: auto;color: #1dcb8b;">403</div>
    <div class="main">
    <a href="${context}/project" style="font-size: 15px;font-weight: lighter;line-height: normal;">Back Home</a>
    <h2 style="font-size: 24px;font-weight: 100;line-height: normal;">Sorry, You don't have this permission!</h2>
    
  </div>
  </div>
  
</div>
</section>