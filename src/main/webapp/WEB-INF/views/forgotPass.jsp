<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="body-container">
	<div class="body-screen">
		<div class="center-screen" style="min-height: auto; width: 520px"> 
			<h3 class="text-center title">Forgot password</h3>
			<form class="form-horizontal" id="resetPassword" name="login">
				<div class="modal-body">
				<input type = "hidden" id="token" value="${changePassDTO.getToken()}">
					<div class="row item form-group bad newPassword">
					    <label for="username" class="control-label col-md-4 col-sm-12 col-xs-4">Password <span class="required"> *</span></label>
				    	<div class="col-md-7 col-sm-6 col-xs-12">
					    	<div class="input-icon right"><i class="fa password" data-toggle="tooltip"></i>
						      	<input type="password" name="password" id="password" placeholder="Enter password" class="form-control">
						    </div> 
					    </div> 
				  	</div>  
				  	
				  	<div class="row item form-group bad passConfirm">
					    <label for="password" class="control-label col-md-4 col-sm-12 col-xs-4">Confirm Password <span class="required"> *</span></label>
				    	<div class="col-md-7 col-sm-6 col-xs-12">
					    	<div class="input-icon right"><i class="fa confirmPassWord"></i>
						      	<input type="password" name="confirmPass" id="confirmPass" placeholder="Enter password" class="form-control">
						    </div>
					    </div>
				  	</div>
				</div>
				<div class="modal-footer text-center">
			        <button type="submit" class="btn btn-primary" onclick="validateResetPass();">Change</button>
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      	</div>
		     </form>
	    </div>
	</div>
</div>
<script src="${context}/assets/js/management/forgotPassword.js"></script>