<script>
var CONTEXT = "${context }";
var currentLocale='${sessionScope.lang}';
if (currentLocale == '') {
	currentLocale = navigator.language || navigator.userLanguage;
	if (currentLocale == 'jp' || currentLocale == 'ja' || currentLocale.indexOf('jp-') != -1 || currentLocale.indexOf('ja-') != -1) {
		currentLocale = 'ja';
	} else {
		currentLocale = 'en';
	}
}
</script>

<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,900&display=swap&subset=latin-ext" rel="stylesheet">
<link href="${context}/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="${context}/assets/plugins/themify-icons/themify-icons.css" rel="stylesheet" type="text/css" />
<link href="${context}/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="${context}/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${context}/assets/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${context}/assets/plugins/sweet-alert/sweetalert.min.css">
<link href="${context}/assets/css/style.css" rel="stylesheet" type="text/css" /> 
<link href="${context}/assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="${context}/assets/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="${context}/assets/css/theme-color.css" rel="stylesheet" type="text/css" /> 
<link href="${context}/assets/css/custom.css" rel="stylesheet" type="text/css" />
<link href="${context}/assets/css/home-page/style_home.css" rel="stylesheet" type="text/css" /> 
<!-- start js include path -->
<script src="${context}/assets/plugins/jquery/jquery.min.js" ></script>
<script src="${context}/assets/plugins/popper/popper.min.js" ></script>
<script src="${context}/assets/plugins/jquery-ui/jquery-ui.min.js" ></script>
<script src="${context}/assets/plugins/bootstrap/js/bootstrap.min.js" ></script>
<script src="${context}/assets/plugins/sweet-alert/sweetalert.min.js" ></script>
<script src="${context}/assets/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
<script src="${context}/assets/js/management/signup.js"></script>
<script src="${context}/assets/js/management/login.js"></script>
<script src="${context}/assets/js/management/translate.js" charset="UTF-8" ></script>



