<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<tiles:importAttribute name="javascripts" />
<tiles:importAttribute name="stylesheets" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:getAsString name="title" /></title>
<link rel="icon" href="${context }/assets/img/favicon.ico" type="image/x-icon" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />

<%@ include file="../common/cmn-js-css.jsp"%>
<!--  <script type="text/javascript">
translateElement('body');
</script>-->
</head>
<script type="text/javascript">
var jscontext="${context}";
var username="${userDetails.getUsername()}";
var fullName="${userDetails.firstname}" + " " + "${userDetails.lastname }";
</script>
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark sidemenu-closed">
	<div class="page-wrapper">
		<tiles:insertAttribute name="header" />
	 	<div class="page-container">
			<tiles:insertAttribute name="leftmenu" /> 
			<div class="page-content-wrapper">
                <div class="page-content">
					<tiles:insertAttribute name="body" />
				</div>
			</div>
		</div>
		<tiles:insertAttribute name="footer" />
	</div>
</body>
<!-- <script>
	translateElement('body');	

</script> -->
</html>